#ifndef TUPLESRUNNER_HPP
#define TUPLESRUNNER_HPP

#include "ggraph_config.h"
#include "utility/runner.hpp"

#include <vector>
#include <cassert>



namespace ggraph {
	/**
	 * runner that goes through all n-tuples of k elements 
	 * (see the equivalent mathematica orderedTuples function)
	 * the output vector represents a map from k to the number of
	 * times the kth element appears in the current tuple
	 */
	class tuplesRunner : public runner<std::vector<unsigned int> > {
	public:
		//**************************************************
		// constructor
		//**************************************************
		tuplesRunner() : runner<std::vector<unsigned int> >(false), m_n(0), m_k(0) {}
		tuplesRunner(const std::size_t& n_, const std::size_t& k_)
			: runner<std::vector<unsigned int> >(n_ > 0 && k_ > 0), m_current(k_, 0) , m_n(n_),
			  m_k(k_) {
			m_current.front() = m_n;//first permutation
		}

		//**************************************************
		// methods
		//**************************************************
		bool next() override;

		const std::vector<unsigned int>& getCurrent() const override {
			return m_current;
		}

		void reset() {
			m_current = std::vector<unsigned int>(m_current.size(), 0);
			m_current.front() = m_n;
			runner<std::vector<unsigned int> >::m_isRunning = (m_k > 0) && (m_n > 0);
		}


	private:
		//**************************************************
		// members
		//**************************************************
		std::vector<unsigned int> m_current;
		const std::size_t m_n;
		const std::size_t m_k;
	};
}

#endif /*TUPLESRUNNER_HPP*/
