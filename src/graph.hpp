#ifndef GKA_GRAPH_HPP
#define GKA_GRAPH_HPP

#include "ggraph_config.h"

#include <iostream>
#include <utility>
#include <memory>
#include <string>
#include <map>
#include <boost/graph/adjacency_list.hpp>
#include <graph_canon/canonicalization.hpp>
#include <graph_canon/aut/implicit_size_2.hpp>
#include <graph_canon/aut/pruner_basic.hpp>
#include <graph_canon/invariant/cell_split.hpp>
#include <graph_canon/invariant/coordinator.hpp>
#include <graph_canon/invariant/partial_leaf.hpp>
#include <graph_canon/invariant/quotient.hpp>
#include <graph_canon/refine/WL_1.hpp>
#include <graph_canon/target_cell/flm.hpp>
#include <graph_canon/tree_traversal/bfs-exp.hpp>
#include <cereal/access.hpp>

#include "graphMap.hpp"
#include "util.hpp"

namespace ggraph {
	/**
	 * flavor
	 */
	struct flavor_t {
		int m_value;

		flavor_t (const int& value_ = -1) : m_value((value_ < 0) ? -1 : value_) {
		}

		operator int&() & {
			return m_value;
		}

		operator const int&() const & {
			return m_value;
		}

		operator int&&() && {
			return std::move(m_value);
		}

		//**************************************************
		// serialization
		//**************************************************
		template <typename Archive>
		void serialize(Archive& ar_) {
			ar_(cereal::make_nvp<std::size_t>("fl", m_value));
		}
	};

	/**
	 * all allowed line types
	 */
	enum class line_t : int {GLUON,QUARK,SCALAR};
	std::ostream& operator<<(std::ostream& stream_, const line_t& type_);
	line_t stringToLineT(const std::string& str_);
	inline bool isDirected(const line_t& type_) {
		return type_ == line_t::QUARK;
	}

	typedef std::pair<line_t, flavor_t> flLine_t;

	typedef std::map<std::size_t, std::size_t> permutation;

	void printLineFl(const line_t& lT_, const flavor_t& fl_, const bool& outgoing_,
					 std::ostream& ostream_ = std::cout);

	/**
	 * graph implements the following boost graph interfaces:
	 *   - AdjacencyGraph
	 *   - BidirectionalGraph (refines IncidenceGraph)
	 *   - VertexListGraph
	 *   - EdgeListGraph
	 * It is an undirected graph and we use "fake" vertices to produce directed edges
	 */
	class graph {
		friend class graphs;
		friend class labelledGraph;
		friend class dualGraphs;
		friend class cut;
		friend class orderedTree;
	public:
		//forward declarations
		struct vert_t;
		struct edge;
		typedef vert_t* vert;
		
		struct edge_t : public std::pair<vert, vert> {
			//**************************************************
			// constructors
			//**************************************************
			edge_t() {}
			edge_t(const flavor_t& flavor_, const line_t& type_)
				: m_flavor(flavor_), m_type(type_) {}
			edge_t(const flLine_t& type_)
				: m_flavor(type_.second), m_type(type_.first) {}
			//does not copy the verts
			edge_t(const edge_t& other_)
				: m_ID(other_.m_ID), m_flavor(other_.m_flavor), m_type(other_.m_type) {}

			//**************************************************
			// operators
			//**************************************************
			//compares properties (including m_ID and connected vert IDs)
			friend bool operator==(const edge_t& left_, const edge_t& right_);
			friend bool operator!=(const edge_t& left_, const edge_t& right_) {
				return !(left_ == right_);
			}
			friend bool operator<(const edge_t& left_, const edge_t& right);

			//**************************************************
			// methods
			//**************************************************
			vert& other(const vert v_) {
				if (first == v_) {
					return second;
				} else {
					assert(second == v_);
					return first;
				}
			}
			vert otherActual(const vert v_);

			//compare properties (excluding m_ID, direction and connected vert IDs)
			//-1: e1_<e2_, 0: e1_==e2_, 1: e1_>e2_
			static int compare(const edge_t& e1_, const edge_t& e2_);

			bool isActualEdge() const;
			edge getActualEdge();
			//edge getActualEdge() const;

			std::pair<vert, vert> getActualVerts();

			/**
			 * replaces the first occurence of vOld_ by vNew_
			 */
			void replaceVertex(const vert& vOld_, const vert& vNew_);

			/**
			 * follows the lines of the type (and flavor) of this line until it either
			 * ends at an external vertex or back it this line
			 * the behaviour is not well-defined if there is a vertex with
			 * more than two lines of the given type
			 * (i.e. only use for lines like quarks that always appear in pairs)
			 */
			bool isClosedLoop();

			/**
			 * checks if this leg is part of a loop
			 */
			bool isLoop();

			/**
			 * checks if the edge splits the graph into two-parts
		     * where one side has zero or one external leg
		     * (i.e. this edge has null momentum)
			 */
			bool isNull();

			void permuteFlavors(const std::map<flavor_t, flavor_t>& map_);
			

			//**************************************************
			//members
			//**************************************************
			std::size_t m_ID;
			flavor_t m_flavor;
			line_t m_type;

			//**************************************************
			// serialization
			//**************************************************
			template <typename Archive>
			void serialize(Archive& ar_) {
				ar_(cereal::make_nvp<std::size_t>("id", m_ID),
					cereal::make_nvp<flavor_t>("fl", m_flavor),
					cereal::make_nvp<line_t>("t", m_type));
			}
		};

		struct edge {
			edge(edge_t* edge_ = nullptr, const bool& dir_ = true)
				: m_edge(edge_), m_dir(dir_) {
			}

			explicit operator bool() {
				return m_edge;
			}

			edge_t& operator*() {
				return *m_edge;
			}

			const edge_t& operator*() const {
				return *m_edge;
			}

			edge_t* operator->() {
				return m_edge;
			}

			edge_t* operator->() const {
				return m_edge;
			}

			friend bool operator==(const edge& left_, const edge& right_) {
				return (left_.m_edge == right_.m_edge);
			}

			friend bool operator!=(const edge& left_, const edge& right_) {
				return !(left_.m_edge == right_.m_edge);
			}

			friend bool operator<(const edge& left_, const edge& right_) {
				return left_.m_edge < right_.m_edge;
			}

			//with correct m_dir
			edge getActualEdge() {
				auto e = m_edge->getActualEdge();
				e.m_dir = m_dir;
				return e;
			}

			edge_t* m_edge;
			bool m_dir;//true = outgoing
		};
		//typedef edge_t* edge;

		struct vert_t : public std::vector<edge> {
			//**************************************************
			// constructors
			//**************************************************
			vert_t(const int& number_ = 0) : m_number(number_) {}
			//does not copy the edges
			vert_t(const vert_t& other_) : m_ID(other_.m_ID), m_number(other_.m_number) {}

			//**************************************************
			// operators
			//**************************************************
			//compares properties (including m_ID) and IDs and m_dir of connected edges
			friend bool operator==(const vert_t& left_, const vert_t& right_);
			friend bool operator!=(const vert_t& left_, const vert_t& right_) {
				return !(left_ == right_);
			}
			friend bool operator<(const vert_t& left_, const vert_t& right_);

			//**************************************************
			// methods
			//**************************************************
			//compares properties (excluding m_ID)
			friend bool less(const vert_t& left_, const vert_t& right_) {
				//inverse ordering (external before internal vertices)
				return left_.m_number > right_.m_number;
			}
			//compares properties (excluding m_ID) and size
			friend bool lessSize(const vert_t& left_, const vert_t& right_);
			edge other(const edge e_);
			const edge other(const edge e_) const;

			/**
			 * tries to find and remove the given edge (a single time!)
			 * return true if succeed
			 * m_dir matters in first version!
			 */
			bool removeEdge(const edge& e_);
			bool removeEdge(const std::unique_ptr<edge_t>& e_);

			/**
			 * replaces the given edge by the new edge
			 * m_dir matters in first version!
			 * second version only replace first occurence
			 */
			void replaceEdge(const edge& eOld_, const edge& eNew_);
			void replaceEdge(const std::unique_ptr<edge_t>& eOld_, const edge& eNew_);


			/**
			 * replaces the edges simultaneously (important for tadpoles!!!)
			 */
			void replaceEdges(const edge& eOld1_, const edge& eNew1_,
							  const edge& eOld2_, const edge& eNew2_);

			/**
			 * changes the m_dir of the given edge inside this
			 * m_dir of given edge matters in first version
			 */
			void changeEdgeDir(const edge& e_);
			void changeEdgeDir(const std::unique_ptr<edge_t>& e_);

			/**
			 * tries to find the edge that comes cyclically after the given edge
			 * returns nullptr if the given edge is not found
			 */
			edge next(const edge& e_);
			const edge next(const edge& e_) const;

			/**
			 * tries to find the edges that come cyclically after the given edge
			 * returns pair of nullptr if the given edge is not found
			 */
			std::pair<edge, edge> nextTwo(const edge& e_);
			std::pair<const edge, const edge> nextTwo(const edge& e_) const;

			/**
			 * swaps the two given edges
			 */
			void swapEdges(const edge& e1_, const edge& e2_);

			//**************************************************
			// members
			//**************************************************
			std::size_t m_ID;
			/**
			 * -1 to -2: internal fake vertices
			 * 0: internal normal vertex
			 * positive: external number
			 */
			int m_number;

			//**************************************************
			// serialization
			//**************************************************
			template <typename Archive>
			void serialize(Archive& ar_) {
				ar_(cereal::make_nvp<std::size_t>("id",m_ID),
					cereal::make_nvp<std::size_t>("n",m_number));
			}
		};

		//vertex adjacency iterator
		struct vertAdjIt : public std::vector<edge>::const_iterator {
			//**************************************************
			// constructors
			//**************************************************
			vertAdjIt(const vert& v_, const std::vector<edge>::const_iterator& it_)
				: std::vector<edge>::const_iterator(it_), m_vert(v_) {
			}

			//**************************************************
			// operators
			//**************************************************
			vert& operator*() {
				return (std::vector<edge>::const_iterator::operator*())->other(m_vert);
			}

			vert* operator->() {
				return &(std::vector<edge>::const_iterator::operator*())->other(m_vert);
			}
			

			//**************************************************
			// members
			//**************************************************
			vert m_vert;
		};

		//vertex iterator
		struct vertIt : public std::vector<std::unique_ptr<vert_t> >::const_iterator {
			//**************************************************
			// constructors
			//**************************************************
			vertIt() {}
			vertIt(const std::vector<std::unique_ptr<vert_t> >::const_iterator& it_)
				: std::vector<std::unique_ptr<vert_t> >::const_iterator(it_) {
			}

			//**************************************************
			// operators
			//**************************************************
			vert& operator*() {
				m_vert = std::vector<std::unique_ptr<vert_t> >::const_iterator::operator*().get();
				return m_vert;
			}

			vert* operator->() {
				m_vert = std::vector<std::unique_ptr<vert_t> >::const_iterator::operator*().get();
				return &m_vert;
			}

			//stores the current vertex
			vert m_vert;
		};

		//edge iterator
		struct edgeIt : public std::vector<std::unique_ptr<edge_t> >::const_iterator {
			//**************************************************
			// constructors
			//**************************************************
			edgeIt() : m_edge(nullptr, true) {}
			edgeIt(const std::vector<std::unique_ptr<edge_t> >::const_iterator& it_)
				: std::vector<std::unique_ptr<edge_t> >::const_iterator(it_),
				m_edge(nullptr, true) {
			}

			//**************************************************
			// operators
			//**************************************************
			edge& operator*() {
				m_edge = edge(
					std::vector<std::unique_ptr<edge_t> >::const_iterator::operator*().get(),
					true);
				return m_edge;
			}

			edge* operator->() {
				m_edge = edge(
					std::vector<std::unique_ptr<edge_t> >::const_iterator::operator*().get(),
					true);
				return &m_edge;
			}

			//stores the current vertex
			edge m_edge;
		};

		//**************************************************
		// constructors
		//**************************************************
		graph() {};

		//from archive
		graph(std::istream& istream_);

		graph(const graph& other_);
		graph(graph&& other_) = default;

		graph& operator=(const graph& other_);
		graph& operator=(graph&& other_) = default;

		//**************************************************
		// operators
		//**************************************************
		vert operator[](const size_t& id_) const {
			return m_verts[id_].get();
		}

		vert operator[](const size_t& id_) {
			return m_verts[id_].get();
		}

		friend bool operator==(const graph& left_, const graph& right_);
		friend bool operator!=(const graph& left_, const graph& right_) {
			return !(left_ == right_);
		}

		friend bool operator<(const graph& left_, const graph& right_);

		/**
		 * returns negative number if left_ < right_
		 * 0 if left_ == right_
		 * 1 if left_ > right_
		 */
		friend int compare(const graph& left_, const graph& right_);

		//**************************************************
		// methods
		//**************************************************
		std::vector<std::unique_ptr<vert_t> >::iterator vBegin() {
			return m_verts.begin();
		}
		std::vector<std::unique_ptr<vert_t> >::iterator vEnd() {
			return m_verts.end();
		}
		std::vector<std::unique_ptr<vert_t> >::const_iterator vBegin() const {
			return m_verts.begin();
		}
		std::vector<std::unique_ptr<vert_t> >::const_iterator vEnd() const {
			return m_verts.end();
		}

		std::vector<std::unique_ptr<edge_t> >::iterator eBegin() {
			return m_edges.begin();
		}
		std::vector<std::unique_ptr<edge_t> >::iterator eEnd() {
			return m_edges.end();
		}
		std::vector<std::unique_ptr<edge_t> >::const_iterator eBegin() const {
			return m_edges.begin();
		}
		std::vector<std::unique_ptr<edge_t> >::const_iterator eEnd() const {
			return m_edges.end();
		}

		std::size_t nVerts() const {
			return m_verts.size();
		}
		std::size_t nEdges() const {
			return m_edges.size();
		}
		std::size_t nExts() const;
		/**
		 * gives iterator to after-last external vertex
		 */
		std::vector<std::unique_ptr<vert_t> >::iterator extEnd();
		std::vector<std::unique_ptr<vert_t> >::const_iterator extEnd() const;

		const std::vector<std::unique_ptr<vert_t> >& getVerts() const {
			return m_verts;
		}

		const std::vector<std::unique_ptr<edge_t> >& getEdges() const {
			return m_edges;
		}

		/**
		 * canonicalizes the graph and returns a vertex/map from inputIDs to canonical IDs
		 * First function might be marginally faster
		 */
		std::vector<std::size_t> canonicalize();
		graphMap canonMap();

		/**
		 * same as above, but doesn't change the (cyclic) edge ordering at each vertex
		 * i.e. canonicalized ordered graphs!
		 * (warning: experimental, not sure if true canonicalization)
		 * if a vertex is given, this one is assumed to be the starting vertex
		 * (i.e. it doesn't give a global canonicalization, but canonicalization
		 *  wrt to the given starting vertex)
		 */
		std::vector<std::size_t> canonicalizeOrdered(vert min_ = nullptr);
		graphMap canonMapOrdered(vert min_ = nullptr);

		/**
		 * finds a map of this graph to its canonical version
		 */
		graphMap findMap() const;

		/**
		 * prints in mathematica friendly way
		 */
		friend std::ostream& operator<<(std::ostream& ostream_, const graph& g_);

		/**
		 * integrates all vertices and edges from the given graph into *this
		 * and relabels them
		 */
		void join(graph&& other_);

		/**
		 * Integrates all vertices and edges from the given graph into *this
		 * and connects them at vertices with the same m_number
		 * (i.e. removes the external vertex and one of the edges)
		 * The edges connected to the externals need to be of the same type
		 * and go in the same direction for directed edges
		 * The deleter can be used to delete edges from a graphLabelling,
		 * i.e. the given functor takes a std::size_t argument and deletes
		 * the edge with this id from the graphLabelling
		 */
		struct defaultDeleter {
			void operator()(const std::size_t& ) {};
		};
		template <typename Deleter = defaultDeleter>
		void connect(graph&& other_, Deleter deleter = defaultDeleter());

		void printVerbose(std::ostream& ostream_ = std::cout) const;

		void saveToStream(std::ostream& ostream_) const;

		/**
		 * the graph needs to be canonicalized (otherwise the output is random!)
		 * constructs the symmetry group associated to this graph
		 * the symmetry group is given as a vector of generators of the group
		 *   each generator represents a map of the vertex ID's to themselves
		 *   multiline swaps and tadpoles are not included
		 * TODO: this is quite an expensive operation, since it copies and
		 *       canonicalizes the graph in the process
		 */
		std::vector<std::vector<std::size_t> > makeSymmetryGrp() const;

		/**
		 * prints the symmetry group in MMA readable way as constructed by makeSymmetryGrp
		 */
		std::ostream& printSymmetryGroup(std::ostream& ostream_ = std::cout) const;

		char* printSymmetryGroupMMA() const {
			return printCallable(
				std::bind(&graph::printSymmetryGroup,*this,std::placeholders::_1));
		}

		/**
		 * returns the symmetry factor from (undirected! TODO?) multi-edges and tadpoles
		 */
		std::size_t countMultiEdgeTadpoleSyms() const;

		/**
		 * Computes the full symmetry factor of the diagram (TODO: undirected multi-edges&tadpoles)
		 */
		std::size_t symmetryFactor() const;

		/**
		 * gives a map from edgeIDs to bool stating if the
		 * corresponding edge is a loop edge
		 */
		std::map<std::size_t, bool> loopEdges() const;

		/**
		 * returns a map from a graph with vertices permuted according to the
		 * generator (see makeSymmetryGrp) to canonical *this.
		 * (*this needs to be canonicalized already!)
		 * the edge map randomly assign mappings for multi-edges (but always 1-1)
		 */
		graphMap symGrpGenToMap(const std::vector<std::size_t>& gen_) const;

		/**
		 * sets all external m_numbers to one
		 */
		void setExtUnity();

		/**
		 * permutes the external numbers according to the permutation given
		 * (graph is not canonical anymore after this step)
		 */
		void permute(const permutation& perm_);

		/**
		 * checks if there's an edge that connects to the same vertex on both sides
		 * (also directed lines are properly checkd!)
		 */
		bool containsSimpleTadpole() const;

		/**
		 * checks if the diagram contains a bubble, i.e. if there exist two lines
		 * that connect the same two vertices
		 * (also directed lines are properly checked!)
		 */
		bool containsBubble() const;

		/**
		 * checks if there's an edge that splits the graph into two-parts
		 * where one side has zero or one external leg
		 * (i.e. this edge has null momentum)
		 */
		bool containsNull() const;

		/**
		 * checks if there is a loop consisting purely of gluon lines
		 */
		bool containsPureGluonLoop() const;

		/**
		 * returns if at least one leg has the given flavor or flavor+lineType 
		 */
		bool containsFlavor(const flavor_t& fl_) const;
		bool containsFlavor(const flLine_t& fl_) const;

		/**
		 * permutes flavors according to the map
		 * flavors not appearing in the map won't be permuted
		 */
		void permuteFlavors(const std::map<flavor_t, flavor_t>& map_);

		/**
		 * transforms the given graph into a pure topological graph
		 * (i.e. strips away fake edges, flavor, line types,...)
		 * everything goes to simple gluon lines
		 * the map is from *this to the newely produced graph and is not
		 * necessarily invertible since fake edges and vertices have been removed
		 */
		std::pair<graph, graphMap> toTopo() const;

		graph getTopo() const {
			return toTopo().first;
		}

		/**
		 * makes a vector of all quark lines
		 * for each quark line it collects all non-fake edges that belong to that line
		 * each vertex is only allowed to have two quark lines of the same flavor
		 * (and the arrows need to be consistent!)
		 */
		std::vector<std::vector<std::size_t> > getQuarkLines() const;

		/**
		 * count number of closed quark loops. each vertex is only allowed to have exactly zero
		 * or two quark lines of the same flavor (and arrows need to be consistent)
		 */
		std::size_t countQuarkLoops() const;

		/**
		 * makes a vector of all scalar lines
		 * for each quark line it collects all non-fake edges that belong to that line
		 * each vertex is only allowed to have two quark lines of the same flavor
		 */
		std::vector<std::vector<std::size_t> > getScalarLines() const;

		/**
		 * reverse quark lines in all possible combinations
		 * each vertex is only allowed to have two quark lines of the same flavor
		 * identity is skipped!
		 */
		std::vector<graph> reverseQuarkLines() const;
		/**
		 * this 2nd version keeps additionally track of a graphMap
		 * i.e. it assumes that the given map is from *this to another graph
		 * and it swaps the signs of the edges that it flips
		 */
		std::vector<std::pair<graph, graphMap> > reverseQuarkLines(
			const graphMap& map_) const;

		/**
		 * reverses every quark line at the same time
		 */
		graph reverseAllQuarks() const;

		/**
		 * reverse the given quark lines
		 * (only actual edges need to be given in the argument)
		 */
		void reverseQuarkLine(const std::vector<std::size_t>& edges_);
		/**
		 * flips additionally the edges signs of the given map, see above
		 */
		void reverseQuarkLine(const std::vector<std::size_t>& edges_,
							  graphMap& map_);

		/**
		 * swaps the given edges (specified by id and direction) at the given vertex
		 */
		void swapEdges(const std::size_t& vertID_, const std::size_t& edgeID1_, const bool& edgeDir1_,
					   const std::size_t& edgeID2_, const bool& edgeDir2_);

	private:
		//**************************************************
		// members
		//**************************************************
		std::vector<std::unique_ptr<vert_t> > m_verts;
		std::vector<std::unique_ptr<edge_t> > m_edges;

		//**************************************************
		// methods
		//**************************************************
		vert addVertex(const vert_t& vert_);
		/**
		 * adds the new edge and all required fake edges and vertices if directed
		 */
		edge addEdge(const edge_t& edge_, vert& from_, vert& to_);
		/**
		 * adds the new edge and all required fake edges and vertices if directed
		 * additionally allows to specify at which position the edge is inserted
		 * inside the vertices
		 * (the given iterators are updated to point to the newly inserted edge)
		 */
		edge addEdge(const edge_t& edge_, vert& from_, vert& to_,
					 vert_t::iterator& itFrom_, vert_t::iterator& itTo_);
		
		/**
		 * adds the new edge by replacing repl_
		 * it disconnects repl_ from the vertices
		 * if eRepl_ is a directed edge it also replaces (or if necessary removes)
		 * the fake edges and vertices associated to it
		 * the newly inserted actual edge has the same id as the old actual edge
		 * new fake edges will also have the same id as old fake edges
		 * if fake edges are removed, all other edge id's might have shifted
		 * accordingly (the order of the rest doesn't change)
		 * if new fake edges need to be created, they are always inserted at the end.
		 * the new edge(s) is always inserted in the end of the verts
		 */
		edge replaceEdge(const edge_t& edge_, vert& from_, vert& to_, edge repl_);
		/**
		 * same as before, but new edge(s) connects the same vertices.
		 * the edges are no longer inserted at the end of the verts, but
		 * remain at the same place (even if fakes are inserted or removed)
		 * if invert is given, the direction is inverted (even for undirected tadpoles)
		 */
		edge replaceEdge(const edge_t& edge_, edge repl_, const bool& invert_ = false);

		/**
		 * removes the vertex
		 * all other vertices ID's are reset accordingly
		 * (does NOT touch the connected edges in any way (e.g. to disconnect))
		 */
		void removeVertex(const vert& v_);
		void removeVertex(std::size_t id_);
		std::vector<std::unique_ptr<vert_t> >::iterator removeVertex(
			const std::vector<std::unique_ptr<vert_t> >::const_iterator& v_);
		
		/**
		 * removes the edge
		 * all other edge ID's are reset accordingly
		 * (does NOT touch the connected vertices in any way (e.g. to disconnect))
		 */
		void removeEdge(const edge& e_);
		void removeEdge(std::size_t id_);
		std::vector<std::unique_ptr<edge_t> >::iterator removeEdge(
			const std::vector<std::unique_ptr<edge_t> >::const_iterator& e_);

		/**
		 * adds a gluon leg from a scalar edge/vertex to another edge/vertex
		 * in every possible way
		 */
		std::set<graph> addEFTGraviton() const;
		graph addEFTGraviton(const edge& e1_, const edge& e2_) const;
		graph addEFTGraviton(const edge& e_, const vert& v_) const;
		//graph addEFTGraviton(const vert& v1_, const vert& v2_) const;

		/**
		 * adds a gluon leg from all edges/vertices to a new external BH with given external number
		 * connectToExt_: if we allow higher point WL couplings
		 */
		std::set<graph> addPMEFTBH(const std::size_t& number_, const bool& higherWLCoupling_ = false) const;

		/**
		 * adds a directed quark leg with flavor 0 to all edges/vertices starting from a new external graviton
		 * with given external number
		 * connectToExt_: if we allow higher point WL couplings
		 */
		std::set<graph> addPMEFTGrav(const std::size_t& number_, const bool& higherWLCoupling_ = false) const;

		/**
		 * adds a gluon edge starting from all possible purely gluonic vertices or gluon edges
		 * to both "BHs", i.e. externals with number 1 and ,
		 */
		std::set<graph> addGRWLGraviton() const;

		/**
		 * adds the combination of a WL and graviton by connecting the WL to all existing WL
		 * vertices. The added graviton line will always connect both "BHs", i.e. externals with
		 * number 1 and 2
		 */
		std::set<graph> addGRWLTree() const;

		/**
		 * adds the combination of a WL and graviton by connecting the WL to all existing WL
		 * vertices. The added graviton line will always connect both "BHs", i.e. externals with
		 * number 1 and 2
		 * the difference to the above function is that the WL is an undirected scalar
		 */
		std::set<graph> addGRWLTreeUndir() const;

		/**
		 * Helper for symmetryFactor.
		 * Permutes the given vector using the given permutation.
		 */
		static std::vector<std::size_t> permuteHelper(const std::vector<std::size_t>& vec_,
													  const std::vector<std::size_t>& perm_);

	public:
		//vertex index map
		struct indexMap {
			const std::size_t& get(const vert& v_) const {
				return v_->m_ID;
			}

			const std::size_t& get(const vert& v_) {
				return v_->m_ID;
			}
		};
		static indexMap m_indexMap;

		//vertex less
		struct vertexLess {
			bool operator()(const vert& left_, const vert& right_) {
				return less(*left_, *right_);
			}
		};
		static vertexLess m_vertexLess;

		//less externals used for ordered canonicalization
		struct extLessOrdered {
			bool operator()(const vert& left_, const vert& right_);
		};
		static extLessOrdered m_extLessOrdered;

		//edge less (comparing source and target vertex IDs as well as edge properties)
		struct edgeLess {
			bool operator()(const edge& left_, const edge& right_);
			bool operator()(const std::unique_ptr<edge_t>& left_,
							const std::unique_ptr<edge_t>& right_) {
				return (this->operator()(edge(left_.get(),true),
										 edge(right_.get(), true)));
			}
		};
		static edgeLess m_edgeLess;

	//private:
	public:
		template <typename SizeType>
		struct edgeHandler_t : graph_canon::edge_handler_all_equal_impl<SizeType> {
			template<typename State>
			int compare(const State& s_, const edge& e1_, const edge& e2_) {
				return edge_t::compare(*e1_, *e2_);
			}
		};

		struct edgeHandlerCreator_t {
			template<typename SizeType>
			using type = edgeHandler_t<SizeType>;
				
			template<typename SizeType>
			type<SizeType> make() {
				return {};
			}
		};
		static edgeHandlerCreator_t edgeHandlerCreator;

		static  graph_canon::compound_visitor<
			graph_canon::target_cell_flm, graph_canon::traversal_bfs_exp, graph_canon::refine_WL_1,
			graph_canon::aut_pruner_basic, graph_canon::invariant_cell_split,
			graph_canon::invariant_partial_leaf, graph_canon::invariant_quotient> defaultVisitor;

		//**************************************************
		// serialization
		//**************************************************
		friend class cereal::access;
		template <typename Archive>
		void save(Archive& ar_) const {
			//save vertex and edge data
			ar_(cereal::make_nvp<Archive, std::size_t>("nV", m_verts.size()),
				cereal::make_nvp<Archive, std::size_t>("nE", m_edges.size()));
			for (const auto& v : m_verts) {
				ar_(cereal::make_nvp<vert_t>("v", *v));
			}
			for (const auto& e : m_edges) {
				ar_(cereal::make_nvp<edge_t>("e", *e));
			}

			//save connections
			for (const auto& v : m_verts) {
				ar_(cereal::make_nvp<Archive, std::size_t>("nOE", v->size()));
				for (const auto& e : *v) {
					ar_(cereal::make_nvp<std::size_t>("eID", e->m_ID));
					ar_(cereal::make_nvp<bool>("eDir", e.m_dir));
				}
			}
			for (const auto& e : m_edges) {
				ar_(cereal::make_nvp<std::size_t>("vID", e->first->m_ID),
					cereal::make_nvp<std::size_t>("vID", e->second->m_ID));
			}
		}

		template <typename Archive>
		void load(Archive& ar_) {
			//load vertex and edge data
			std::size_t nVerts, nEdges;
			ar_(nVerts, nEdges);
			m_verts.reserve(nVerts);
			m_edges.reserve(nEdges);
			for (auto i = 0; i != nVerts; ++i) {
				vert_t v;
				ar_(v);
				m_verts.emplace_back(new vert_t(std::move(v)));
			}
			for (auto i = 0; i != nEdges; ++i) {
				edge_t e;
				ar_(e);
				m_edges.emplace_back(new edge_t(std::move(e)));
			}

			//load connections and connect
			for (auto i = 0; i != nVerts; ++i) {
				auto& v = m_verts[i];
				std::size_t n;
				ar_(n);
				for (auto j = 0; j != n; ++j) {
					std::size_t id;
					ar_(id);
					bool dir;
					ar_(dir);
					v->push_back(edge(m_edges[id].get(),dir));
				}
			}
			for (auto i = 0; i != nEdges; ++i) {
				auto& e = m_edges[i];
				std::size_t idFrom, idTo;
				ar_(idFrom, idTo);
				e->first = m_verts[idFrom].get();
				e->second = m_verts[idTo].get();
			}
		}
	};

	//generic implementations
	template <typename Deleter>
	void graph::connect(graph&& other_, Deleter deleter_) {
		std::set<std::size_t> ignore;
		//collect set of fake vertices that we don't need to copy
		for (auto& v : other_.m_verts) {
			if (v->m_number < 0) {
				auto vs = v->front()->getActualVerts();
				if (vs.first->m_number > 0) {
					const auto& n = vs.first->m_number;
					auto it = std::find_if(vBegin(), vEnd(),
										   [&](auto& v_) {
											   return n == v_->m_number;
										   });
					if (it != vEnd()) {
						ignore.insert(v->m_ID);
					}
				} else if (vs.second->m_number > 0) {
					const auto& n = vs.second->m_number;
					auto it = std::find_if(vBegin(), vEnd(),
										   [&](auto& v_) {
											   return n == v_->m_number;
										   });
					if (it != vEnd()) {
						ignore.insert(v->m_ID);
					}
				}
			}
		}
		//integrate and connect vertices
		for (auto& v : other_.m_verts) {
			//check if external, if yes, check if already present in *this
			if (v->m_number > 0) {
				const auto& n = v->m_number;
				auto it = std::find_if(vBegin(), vEnd(),
									   [&](auto& v_) {
										   return n == v_->m_number;
									   });
				if (it != vEnd()) {
					//already there, connect!
					if (isDirected(v->front()->m_type)) {
						//directed edge case
						auto e1 = v->front();
						auto v1 = e1->other(v.get());
						//check that they go into the same direction
						assert((*it)->front()->other(it->get())->m_number != v1->m_number);
						auto e2 = v1->other(e1);
						auto v2 = e2->other(v1);
						auto e3 = v2->other(e2);
						auto v3 = e3->other(v2);
						//reconnect
						e3.m_dir = !e3.m_dir;
						v3->replaceEdge(e3, (*it)->front());
						(*it)->front()->replaceVertex(it->get(), v3);
						//remove unneeded edges
						removeVertex(it->get());
						deleter_(e1->m_ID);
						other_.removeEdge(e1);
						deleter_(e2->m_ID);
						other_.removeEdge(e2);
						deleter_(e3->m_ID);
						other_.removeEdge(e3);
					} else {
						//undirected edge case
						auto otherE = v->front();
						auto otherV = otherE->other(v.get());
						//reconnect
						otherE.m_dir = !otherE.m_dir;
						otherV->replaceEdge(otherE, (*it)->front());
						(*it)->front()->replaceVertex(it->get(), otherV);
						//remove unneeded vertex and edge (we can ignore otherV)
						removeVertex(it->get());
						deleter_(otherE->m_ID);
						other_.removeEdge(otherE);
					}
				} else {
					//not there yet, simply copy (and reset ID)
					v->m_ID = m_verts.size();
					m_verts.push_back(std::move(v));
				}
			} else if (v->m_number == 0) {
				//internal, simply copy (and reset ID)
				v->m_ID = m_verts.size();
				m_verts.push_back(std::move(v));
			} else {
				//fake, might not have to copy it
				//check if we need to ignore it
				if (ignore.count(v->m_ID) != 1) {
					v->m_ID = m_verts.size();
					m_verts.push_back(std::move(v));
				}
			}
		}

		//integrate edges
		auto size = m_edges.size();
		for (auto& e : other_.m_edges) {
			e->m_ID += size;
		}
		m_edges.insert(m_edges.end(), std::make_move_iterator(other_.m_edges.begin()),
					   std::make_move_iterator(other_.m_edges.end()));
	}
}

namespace boost {
	struct traversalCategoryTag : adjacency_graph_tag, vertex_list_graph_tag,
								  edge_list_graph_tag, bidirectional_graph_tag {
	};
	
	template <>
	struct graph_traits<ggraph::graph> {
		typedef ggraph::graph::vert vertex_descriptor;
		typedef ggraph::graph::edge edge_descriptor;

		typedef std::vector<ggraph::graph::edge>::const_iterator out_edge_iterator;
		typedef std::vector<ggraph::graph::edge>::const_iterator in_edge_iterator;
		typedef ggraph::graph::vertIt vertex_iterator;
		typedef ggraph::graph::edgeIt edge_iterator;

		typedef undirected_tag directed_category;
		typedef allow_parallel_edge_tag edge_parallel_category;
		typedef traversalCategoryTag traversal_category;
		typedef std::size_t vertices_size_type;
		typedef std::size_t edges_size_type;
		typedef std::size_t degree_size_type;

		vertex_descriptor null_vertex() {
			return nullptr;
		}

		typedef ggraph::graph::vertAdjIt adjacency_iterator;
	};

	template <>
	struct property_traits<ggraph::graph::indexMap> {
		typedef std::size_t value_type;
		typedef const std::size_t& reference;
		typedef ggraph::graph::vert key_type;
		typedef readable_property_map_tag category;
	};
} //namespace boost

namespace ggraph {
	inline boost::graph_traits<graph>::vertex_descriptor source(
		boost::graph_traits<graph>::edge_descriptor e_, const graph& g_) {
		if (e_.m_dir) {
			return e_->first;
		}
		//else
		return e_->second;
	}

	inline boost::graph_traits<graph>::vertex_descriptor target(
		boost::graph_traits<graph>::edge_descriptor e_, const graph& g_) {
		if (e_.m_dir) {
			return e_->second;
		}
		//else
		return e_->first;
	}

	inline std::pair<boost::graph_traits<graph>::out_edge_iterator,
			  boost::graph_traits<graph>::out_edge_iterator> out_edges(
				  boost::graph_traits<graph>::vertex_descriptor u_,
				  const graph& g_) {
		return std::make_pair(u_->begin(), u_->end());
	}

	inline std::pair<boost::graph_traits<graph>::in_edge_iterator,
			  boost::graph_traits<graph>::in_edge_iterator> in_edges(
				  boost::graph_traits<graph>::vertex_descriptor u_,
				  const graph& g_) {
		return std::make_pair(u_->begin(), u_->end());
	}

	inline boost::graph_traits<graph>::degree_size_type out_degree(
		boost::graph_traits<graph>::vertex_descriptor u_,
		const graph& g_) {
		return u_->size();
	}

	inline boost::graph_traits<graph>::degree_size_type in_degree(
		boost::graph_traits<graph>::vertex_descriptor u_,
		const graph& g_) {
		return u_->size();
	}

	//TODO: this one should not be necessary
	inline boost::graph_traits<graph>::degree_size_type degree(
		boost::graph_traits<graph>::vertex_descriptor u_,
		const graph& g_) {
		return u_->size();
	}

	inline std::pair<boost::graph_traits<graph>::adjacency_iterator,
			  boost::graph_traits<graph>::adjacency_iterator> adjacent_vertices(
				  boost::graph_traits<graph>::vertex_descriptor v_,
				  const graph& g_) {
		return std::make_pair(graph::vertAdjIt(v_, v_->begin()),
							  graph::vertAdjIt(v_, v_->end()));
	}

	inline std::pair<boost::graph_traits<graph>::vertex_iterator,
			  boost::graph_traits<graph>::vertex_iterator> vertices(const graph& g_) {
		return std::make_pair(graph::vertIt(g_.vBegin()), graph::vertIt(g_.vEnd()));
	}

	inline boost::graph_traits<graph>::vertices_size_type num_vertices(const graph& g_) {
		return g_.nVerts();
	}

	inline std::pair<boost::graph_traits<graph>::edge_iterator,
			  boost::graph_traits<graph>::edge_iterator> edges(const graph& g_) {
		return std::make_pair(graph::edgeIt(g_.eBegin()), graph::edgeIt(g_.eEnd()));
	}

	inline boost::graph_traits<graph>::edges_size_type num_edges(const graph& g_) {
		return g_.nEdges();
	}

	inline boost::property_traits<graph::indexMap>::reference get(
		const graph::indexMap& map_,
		const boost::property_traits<graph::indexMap>::key_type& key_) {
		return map_.get(key_);
	}

	//TODO: this one should not be necessary
	inline boost::graph_traits<graph>::vertex_descriptor vertex(
		const boost::graph_traits<graph>::vertices_size_type& n_, const graph& g_) {
		return g_[n_];
	}
}

#endif /*GKA_GRAPH_HPP*/
