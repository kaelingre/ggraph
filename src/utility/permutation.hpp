#ifndef PERMUTATION_HPP
#define PERMUTATION_HPP

#include <map>
#include <set>
#include <vector>
#include <iostream>

namespace ggraph {
	template <typename T>
	class permutation : public std::map<T, T> {
	public:
		//**************************************************
		//constructor
		//**************************************************
		/**
		 * trivial permutation
		 */
		inline permutation();

		/**
		 *join two permutations
		 */
		inline permutation(const permutation<T>& left_, const permutation<T>& right_);

		inline permutation(const std::map<T, T>& map_);
		inline permutation(std::map<T, T>&& map_);

		//**************************************************
		//operators
		//**************************************************
		template <typename S>
		friend bool operator==(const permutation<S>& left_, const permutation<S>& right_);

		//**************************************************
		//methods
		//**************************************************
		inline T permute(const T& key_) const;

		inline void invert();

		/**
		 * applies the given permutation on this (i.e. concatenates them)
		 */
		inline void concatenate(const permutation<T>& perm_);

		inline void print(std::ostream& ostream_ = std::cout) const;
	};

	template <typename T>
	permutation<T>::permutation() {
	};

	template <typename T>
	permutation<T>::permutation(const std::map<T, T>& map_) : std::map<T, T>(map_) {
	}

	template <typename T>
	permutation<T>::permutation(std::map<T, T>&& map_) : std::map<T, T>(std::move(map_)) {
	}

	template <typename T>
	bool operator==(const permutation<T>& left_, const permutation<T>& right_) {
		return (std::map<T, T>) left_ == (std::map<T, T>) right_;
	}

	template <typename T>
	T permutation<T>::permute(const T& key_) const {
		auto it = std::map<T, T>::find(key_);
		if (it != std::map<T, T>::end()) {
			return it->second;
		}
		//else
		return key_;
	}

	template <typename T>
	void permutation<T>::invert() {
		std::map<T, T> newMap;
		for (auto it = std::map<T, T>::begin(); it != std::map<T, T>::end(); ++it) {
			newMap.insert(std::make_pair(std::move(it->second), std::move(it->first)));
		}

		std::map<T, T>::operator==(std::move(newMap));
	}

	template <typename T>
	void permutation<T>::concatenate(const permutation<T>& perm_) {
		std::set<T> applied;

		for (auto it = std::map<T, T>::begin(); it != std::map<T, T>::end(); ++it) {
			auto found = perm_.find(it->second);
			if (found != perm_.end()) {
				it->second = found->second;
				applied.insert(found->first);
			}
		}

		//insert not yet applied elements
		for (auto it = perm_.begin(); it != perm_.end(); ++it) {
			if (applied.find(it->first) == applied.end()) {
				std::map<T,T>::insert(*it);
			}
		}
	}

	template <typename T>
	void permutation<T>::print(std::ostream&  ostream_) const {
		for (auto it = std::map<T, T>::begin(); it != std::map<T, T>::end(); ++it) {
			ostream_ << it->first << " --> " << it->second << std::endl;
		}
	}
}

#endif /*PERMUTATION_HPP*/
