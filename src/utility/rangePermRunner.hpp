#ifndef RANGEPERMRUNNER_HPP
#define RANGEPERMRUNNER_HPP

#include "runner.hpp"

namespace ggraph {
	/**
	 * a runner over all vectors of length n_ with entries from within a range specified by
	 * from_ and to_ (to_ NOT included) (T is an iterator and has to have operator++/-- defined)
	 * UP to permutations of the T's in the sense:
	 * {0, 0, 1, 2} == {1, 1, 0, 2} where we permuted 0 and 1
	 * first entry is ALWAYS 'from_'
	 */
	template <typename T>
	class rangePermRunner : public runner<std::vector<T> > {
	public:
		//**************************************************
		//constructor
		//**************************************************
		rangePermRunner();
		rangePermRunner(const typename std::vector<T>::size_type n_, const T& from_, const T& to_);

		//**************************************************
		//methods
		//**************************************************
		bool next() override;

		inline const std::vector<T>& getCurrent() const override {
			return m_current;
		}

		void reset() {
			m_current = std::vector<T>(m_current.size(), m_from);

			runner<std::vector<T> >::m_isRunning = !m_current.empty();
		}
		
	private:
		//**************************************************
		//members
		//**************************************************
		std::vector<T> m_current;
		
		T m_from;
		T m_to;
	};

	template <typename T>
	rangePermRunner<T>::rangePermRunner()
		: runner<std::vector<T> >(false) {
	}

	template <typename T>
	rangePermRunner<T>::rangePermRunner(const typename std::vector<T>::size_type n_, const T& from_,
										const T& to_)
		: runner<std::vector<T> >(n_ > 0), m_current(std::vector<T>(n_, from_)), m_from(from_),
		m_to(to_) {
	}

	template <typename T>
	bool rangePermRunner<T>::next() {
		if (!runner<std::vector<T> >::isRunning()) {
			return false;
		}

		//special case
		if (m_from == m_to) {
			runner<std::vector<T> >::m_isRunning = false;
			return false;
		}

		auto it = m_current.rbegin();
		for (; it != m_current.rend(); ++it) {
			T next = (*it);
			bool isValid = false;
			for (auto it2 = it + 1; it2 != m_current.rend(); ++it2) {
				if (next == m_from) {
					//vector is in a valid state
					isValid = true;
					break;
				}
				if (*it2 == next) {
					--next;
				}
			}
			if (isValid) {
				++(*it);
				if (*it == m_to) {
					*it = m_from;
				} else {
					break;
				}
			} else {
				*it = m_from;
			}
		}

		if (it == m_current.rend()) {
			runner<std::vector<T> >::m_isRunning = false;
			return false;
		}
		
		return true;
	}
}

#endif /*RANGEPERMRUNNER_HPP*/
