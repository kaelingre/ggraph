#ifndef ORDEREDRANGERUNNER_HPP
#define ORDEREDRANGERUNNER_HPP

#include "runner.hpp"
#include <vector>

namespace ggraph {
	/**
	 * a runner over all vectors of length n_ with entries of type T in a range specified by
	 * from_ and to_ (to_ NOT included) (T has to have operator++/-- defined)
	 * where the entries are ordered! (i.e. first element is always <= second element, ...)
	 */
	template <typename T>
	class orderedRangeRunner : public runner<std::vector<T> > {
	public:
		//**************************************************
		//constructor
		//**************************************************
		orderedRangeRunner();
		orderedRangeRunner(const typename std::vector<T>::size_type n_, const T& from_, const T& to_);

		//**************************************************
		//methods
		//**************************************************
		bool next() override;

		void reset() {
			m_current = std::vector<T>(m_current.size(), m_from);

			runner<std::vector<T> >::m_isRunning = m_current.size() > 0;
		}

		const std::vector<T>& getCurrent() const override {
			return m_current;
		}

		const T& from() const {
			return m_from;
		}

		const T& to() const {
			return m_to;
		}

	private:
		//**************************************************
		//members
		//**************************************************
		std::vector<T> m_current;
		
		T m_from;
		T m_to;
	};

	template <typename T>
	orderedRangeRunner<T>::orderedRangeRunner() : runner<std::vector<T> >(false) {
	}
	
	template <typename T>
	orderedRangeRunner<T>::orderedRangeRunner(const typename std::vector<T>::size_type n_,
											  const T& from_, const T& to_)
		: runner<std::vector<T> >(n_ > 0), m_current(std::vector<T>(n_, from_)), m_from(from_),
		m_to(to_) {
	}

	template <typename T>
	bool orderedRangeRunner<T>::next() {
		if (!runner<std::vector<T>>::isRunning()) {
			return false;
		}

		//special case
		if (m_from == m_to) {
			runner<std::vector<T> >::m_isRunning = false;
			return false;
		}

		for (auto it = m_current.begin(); it != m_current.end() - 1; ++it) {
			if (*it != *(it + 1)) {
				++(*it);
				return true;
			} else {
				*it = m_from;
			}
		}

		++m_current.back();
		if (m_current.back() == m_to) {
			runner<std::vector<T>>::m_isRunning = false;
			return false;
		}
		//else
		return true;
	}
}

#endif /*ORDEREDRANGERUNNER_HPP*/
