#ifndef DYCKWORD_HPP
#define DYCKWORD_HPP

#include <vector>
#include <stack>
#include <cassert>

namespace ggraph {
	/**
	 * a dyckWord is represented as a vector that gives the positions of the closing brackets
	 * (zero indexed!)
	 */
	class dyckWord : public std::vector<std::size_t> {
	public:
		//**************************************************
		//constructors
		//**************************************************
		/**
		 * standard dyck word with length_ pairs of brackets (needed in dyckRunner, so don't change):
		 * ()()()()()...()
		 */
		inline dyckWord(const std::size_t& length_);

		//**************************************************
		//methods
		//**************************************************
		/**
		 * assigns the dyckWord actual objects in forms of pairs and orders them according
		 * to the dyckWord
		 */
		template <typename T>
		std::vector<T> assign(const std::vector<std::pair<T, T> >& pairs_) const;

		/**
		 * counts the number brackets on first level
		 */
		inline size_t numberOfFirstLevel() const;
	};

	dyckWord::dyckWord(const std::size_t& length_) : std::vector<std::size_t>(length_, 0) {
		for (auto it = begin(); it != end(); ++it) {
			*it = 2 * (it - begin()) + 1;
		}
	}

	//**************************************************
	//generic implementations
	//**************************************************
	template <typename T>
	std::vector<T> dyckWord::assign(const std::vector<std::pair<T, T> >& pairs_) const {
		std::vector<T> ret;
		ret.reserve(2 * pairs_.size());
		auto it = begin();
		auto itPairsOpen = pairs_.begin();
		std::stack<const T*> closingStack;
		for (std::size_t i = 0; i != 2 * pairs_.size(); ++i) {
			assert(it != end());
			if (i == *it) {
				//close the bracket
				assert(!closingStack.empty());
				ret.push_back(std::move(*closingStack.top()));
				closingStack.pop();
			} else {
				assert(*it > i);
				//open a new bracket
				ret.push_back(itPairsOpen->first);
				closingStack.push(&(itPairsOpen++)->second);
			}
		}

		assert(ret.size() == 2 * pairs_.size());
		return ret;
	}

	size_t dyckWord::numberOfFirstLevel() const {
		size_t counter = 0;
		size_t level = 0;
		auto it = begin();

		for (unsigned int i = 0; i != 2*size(); ++i) {
			assert(it != end());
			if (i == *it) {
				//close a bracket
				assert(level > 0);
				//see if we close a first level bracket
				if (--level == 0) {
					++counter;
				}
				++it;
			} else {
				//open a bracket
				++level;
			}
		}

		return counter;
	}
}


#endif /*DYCKWORD_HPP*/
