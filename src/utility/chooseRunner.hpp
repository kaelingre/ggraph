#ifndef CHOOSERUNNER_HPP
#define CHOOSERUNNER_HPP

#include "runner.hpp"

namespace ggraph {
	/**
	 * runner that does the operation "m choose n" on a vector.
	 * a selection is returned as a vector (of length n_) of iterators (that point to vector entries)
	 * TODO: implement changes for non-random-access-iterators
	 */
	template <typename T>
	class chooseRunner : public runner<std::vector<T> > {
	public:
		//**************************************************
		//constructor
		//**************************************************
		chooseRunner();
		chooseRunner(const T& begin_, const T& end_, const unsigned int& n_);
		
		//**************************************************
		//methods
		//**************************************************
		bool next() override;

		inline const std::vector<T>& getCurrent() const override {
			return m_current;
		}

		void reset();
		
	private:
		//**************************************************
		//members
		//**************************************************
		std::vector<T> m_current;

		T m_begin;
		T m_end;
	};

	template <typename T>
	chooseRunner<T> makeChooseRunner(T&& begin_, T&& end_, const unsigned int& n_) {
		return chooseRunner<T>(std::forward<T>(begin_), std::forward<T>(end_), n_);
	}

	template <typename T>
	chooseRunner<T>::chooseRunner() : runner<std::vector<T> >(false) {
	}
	
	template <typename T>
	chooseRunner<T>::chooseRunner(const T& begin_,
								  const T& end_,
								  const unsigned int& n_)
		: runner<std::vector<T> >(end_ - begin_ >= n_ && n_ > 0), m_begin(begin_), m_end(end_) {
		assert(m_end >= m_begin);
		if (m_end - m_begin < n_) {
			return;
		}
		auto it = m_begin;
		for (auto i = 0; i < n_; ++i) {
			m_current.push_back(it++);
		}
	}

	template <typename T>
	bool chooseRunner<T>::next() {
		if (!runner<std::vector<T> >::isRunning()) {
			return false;
		}

		for (auto it = m_current.rbegin(); it != m_current.rend(); ++it) {
			if (it == m_current.rbegin()) {
				//special case: last element
				if (++(*it) != m_end) {
					return true;
				} else {
					--(*it);
				}
			} else {
				if (++(*it) != *(it - 1)) {
					//reset all iterators to the right
					auto itCurrent = *it;
					for (--it; it != m_current.rbegin(); --it) {
						*it = ++itCurrent;
					}
					*it = ++itCurrent;
					return true;
				} else {
					--(*it);
				}
			}
		}

		runner<std::vector<T>>::m_isRunning = false;
		return false;
	}

	template <typename T>
	void chooseRunner<T>::reset() {
		auto n = m_current.size();
		m_current.clear();

		if (m_end - m_begin < n) {
			return;
		}
		auto it = m_begin;
		for (auto i = 0; i < n; ++i) {
			m_current.push_back(it++);
		}

		runner<std::vector<T>>::m_isRunning = true;
	}
}

#endif /*CHOOSERUNNER_HPP*/
