#ifndef RANGERUNNER_HPP
#define RANGERUNNER_HPP

#include "runner.hpp"
#include <vector>

namespace ggraph {
	/**
	 * a runner over all vectors of length n_ with entries within a range specified by
	 * from_ and to_ (to_ NOT included) (T is an iterator and needs to have operator++/-- defined)
	 */
	template <typename T>
	class rangeRunner : public runner<std::vector<T> > {
	public:
		//**************************************************
		//constructor
		//**************************************************
		rangeRunner();
		rangeRunner(const typename std::vector<T>::size_type n_, const T& from_, const T& to_);

		//**************************************************
		//methods
		//**************************************************
		bool next() override;

		const std::vector<T>& getCurrent() const override {
			return m_current;
		}

		void reset() {
			m_current = std::vector<T>(m_current.size(), m_from);

			runner<std::vector<T> >::m_isRunning = !m_current.empty();
		}

		const T& from() const {
			return m_from;
		}

		const T& to() const {
			return m_to;
		}

	private:
		//**************************************************
		//members
		//**************************************************
		std::vector<T> m_current;
		
		T m_from;
		T m_to;
	};

	template <typename T>
	rangeRunner<T> makeRangeRunner(const typename std::vector<T>::size_type n_, T&& begin_, T&& end_) {
		return rangeRunner<T>(n_, std::forward<T>(begin_), std::forward<T>(end_));
	}

	template <typename T>
	rangeRunner<T>::rangeRunner() : runner<std::vector<T> >(false) {
	}
	
	template <typename T>
	rangeRunner<T>::rangeRunner(const typename std::vector<T>::size_type n_, const T& from_,
									  const T& to_)
		: runner<std::vector<T> >(n_ > 0), m_current(std::vector<T>(n_, from_)), m_from(from_),
		m_to(to_) {
	}

	template <typename T>
	bool rangeRunner<T>::next() {
		if (!runner<std::vector<T> >::isRunning()) {
			return false;
		}

		//special case
		if (m_from == m_to) {
			runner<std::vector<T> >::m_isRunning = false;
			return false;
		}

		auto it = m_current.rbegin();
		for (; it != m_current.rend(); ++it) {
			if (++(*it) != m_to) {
				return true;
			} else {
				*it = m_from;
			}
		}

		runner<std::vector<T> >::m_isRunning = false;
		return false;
	}
}

#endif /*RANGERUNNER_HPP*/
