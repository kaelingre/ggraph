#ifndef THREADMANAGER_HPP
#define THREADMANAGER_HPP

#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>

namespace ggraph {
	/**
	 * static class!
	 */
	class threadManager {
	public:
		//**************************************************
		//constructor
		//**************************************************
		threadManager(const unsigned int& numberOfThreads_ = std::thread::hardware_concurrency())
			: m_numberOfThreads(numberOfThreads_) {
		}

		//**************************************************
		//methods
		//**************************************************
		/**
		 * runs the given function as soon as a thread is available
		 * returns as soon as the new thread is started
		 */
		inline void run(std::function<void(void)> function_);

		/**
		 * joins until all of the threads are finished
		 */
		inline void join();

		inline void removeThread(const std::thread::id& id_);

	private:
		//**************************************************
		//members
		//**************************************************
		std::mutex m_mutex;
		std::condition_variable m_isFull;
		std::condition_variable m_isRunning; //if at least one thread is running
		std::vector<std::thread::id> m_threadIDs;
		const unsigned int m_numberOfThreads;
	};

	void threadManager::run(std::function<void(void)> function_) {
		std::unique_lock<std::mutex> lock(m_mutex);

		m_isFull.wait(lock, [&]() { return m_threadIDs.size() < m_numberOfThreads; });

		//produce the lambda function with bind to be able to move function_
		auto lambda = std::bind([&](std::function<void(void)> fun_) {
				fun_();
				//lock mutex to be able to remove the finished thread
				std::unique_lock<std::mutex> lock(m_mutex);
				//remove this thread from the running thread vector
				removeThread(std::this_thread::get_id());
				//notify that there's a free thread
				m_isFull.notify_one();
			}, std::move(function_));

		//run the actual function
		std::thread newThread(std::move(lambda));

		m_threadIDs.push_back(newThread.get_id());

		newThread.detach();
	}

	void threadManager::join() {
		std::unique_lock<std::mutex> lock(m_mutex);

		m_isRunning.wait(lock, [&]() { return m_threadIDs.empty(); });
	}

	void threadManager::removeThread(const std::thread::id& id_) {
		for (auto it = m_threadIDs.begin(); it != m_threadIDs.end(); ++it) {
			if (id_ == *it) {
				m_threadIDs.erase(it);
				if (m_threadIDs.empty()) {
					m_isRunning.notify_all();
				}
				return;
			}
		}
		//not found
		assert(false);
	}
}

#endif /*THREADMANAGER_HPP*/
