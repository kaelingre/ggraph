#ifndef DYCKRUNNER_HPP
#define DYCKRUNNER_HPP

#include "runner.hpp"
#include "dyckWord.hpp"

namespace ggraph {
	/**
	 * a runner that runs over all Dyck words of length 2 * n_
	 * see arXiv:1002.2625
	 */
	class dyckRunner : public runner<dyckWord> {
	public:
		//**************************************************
		//constructor
		//**************************************************
		dyckRunner() : runner<dyckWord>(false), m_current(0) {
		}
		inline dyckRunner(const size_t& n_);

		//**************************************************
		//methods
		//**************************************************
		inline bool next() override;

		inline const dyckWord& getCurrent() const override {
			return m_current;
		}

		inline void reset();

	private:
		//**************************************************
		//members
		//**************************************************
		dyckWord m_current;
	};

	dyckRunner::dyckRunner(const size_t& n_) : runner<dyckWord>(n_ > 0), m_current(n_) {
	}

	bool dyckRunner::next() {
		if (!runner<dyckWord>::isRunning()) {
			return false;
		}

		const size_t n = m_current.size();

		if (n < 2) {
			runner<dyckWord>::m_isRunning = false;
			return false;
		}
		
		bool done = true;
		for (int i = n - 2; i >= 0; --i) {
			if (m_current[i] < n + i) {
				++m_current[i];
				for (int j = i + 1; j <= (int) n - 2; ++j) {
					m_current[j] = std::max(m_current[j-1] + 1, (size_t) 2 * j + 1);
				}
				done = false;
				break;
			}
		}

		if (done) {
			runner<dyckWord>::m_isRunning = false;
			return false;
		}

		return true;
	}

	void dyckRunner::reset() {
		m_current = dyckWord(m_current.size());
		runner<dyckWord>::m_isRunning = !m_current.empty();
	}
}

#endif /*DYCKRUNNER_HPP*/
