#include "couplingsRunner.hpp"

namespace ggraph {
	couplingsRunner::couplingsRunner(const std::vector<coupling>& couplings_,
									 const std::size_t& nLoops_, const std::size_t& nExts_,
									 const std::size_t& exactNV_)
		: m_sum(2*nLoops_ + nExts_), m_exactNV(exactNV_) {
		for (auto& c : couplings_) {
			auto it = m_couplings.find(c.size());
			if (it == m_couplings.end()) {
				m_couplings.insert(std::make_pair(c.size(), 1));
			} else {
				++(it->second);
			}
		}

		m_current = std::vector<std::size_t>(m_couplings.size(), 0);

		//prepare rangeRunner, reset tuplesRunner and construct current
		if (exactNV_ == 0) {
			assert(m_sum > 1);
			assert(couplings_.front().size() > 1);
			m_rR = rangeRunner(m_couplings.size(), (std::size_t) 0, m_sum-1);
		} else {
			m_rR = rangeRunner(m_couplings.size(), (std::size_t) 0, m_exactNV+1);
		}
		if (!nextRangeRunner()) {
			runner<std::vector<std::size_t> >::m_isRunning = false;
		}
	}

	bool couplingsRunner::next() {
		if (!isRunning()) {
			return false;
		}

		if (nextTuplesRunner()) {
			return true;
		}
		//else
		if (nextRangeRunner()) {
			return true;
		}
		//else: end
		runner<std::vector<std::size_t> >::m_isRunning = false;
		return false;
	}

	bool couplingsRunner::nextRangeRunner() {
		while (m_rR.next()) {
			auto& c = m_rR.getCurrent();
			std::size_t counter = 0;
			std::size_t countVs = 0;
			int res = 2;
			for (auto& k : m_couplings) {
				res += ((int)k.first - 2) * c[counter];
				countVs += c[counter];
				//step
				++counter;
			}
			if (res >= 0 && ((std::size_t)res) == m_sum && (m_exactNV == 0 ? true : countVs == m_exactNV)) {
				resetTuplesRunners();
				return true;
			}
		}
		return false;
	}

	bool couplingsRunner::nextTuplesRunner() {
		for (auto& tR : m_tuplesRunners) {
			if (tR.next()) {
				constructCurrent();
				return true;
			}
			//else
			tR.reset();
		}
		return false;
	}

	void couplingsRunner::resetTuplesRunners() {
		m_tuplesRunners.clear();
		auto it = m_couplings.begin();
		for (auto& n : m_rR.getCurrent()) {
			m_tuplesRunners.push_back(tuplesRunner(n, it->second));
			//step
			++it;
		}
		constructCurrent();
	}

	void couplingsRunner::constructCurrent() {
		m_current.clear();
		for (auto& tR : m_tuplesRunners) {
			auto& c = tR.getCurrent();
			m_current.insert(m_current.end(), c.begin(), c.end());
		}
	}
}
