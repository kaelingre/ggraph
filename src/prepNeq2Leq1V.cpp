#include "config.h"

#include <iostream>
#include <fstream>

#include "graphLabelling.hpp"
#include "eqns.hpp"

using namespace ggraph;


int main(int argc, char* argv[]) {
	std::vector<flExt> exts;
	auto gluon =std::make_pair(ext_t::GLUON,-1);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);
	exts.push_back(gluon);

	std::fstream fs("/tmp/test.m", std::fstream::out);

	labelledGraphs gs(exts, 1, {coupling::THREEGLUON, coupling::QUARKGLUON});
	labelledGraphs gsNeq4(exts, 1, {coupling::THREEGLUON});
	
	auto es = eqns::makeJacobiEqns(gs);
	es.add(eqns::makeRevSymEqns(gs));
	
	std::vector<const graph*> knowns;
	for (auto& g : gsNeq4) {
		es.push_back(eqns::makeNeq4DecompID(g, gsNeq4, gs));
		knowns.push_back(&g.getGraph());
	}
	
	std::vector<const graph*> masters;
	masters.push_back(&gs[11].getGraph());

	auto sol = es.reduce(masters, knowns);
	replaceRepeated(sol);

	fs << "solFunctional=" << sol << ';' << std::endl
	   << "consistencyEqns=" << es << ';' << std::endl;

	return 0;
}
