#include "tuplesRunner.hpp"

namespace ggraph {
	bool tuplesRunner::next() {
		if (!runner<std::vector<unsigned int> >::isRunning()) {
			return false;
		}

		//end of recursion
		if (m_current.back() == m_n) {
			runner<std::vector<unsigned int> >::m_isRunning = false;
			return false;
		}

		if (m_current.back() != 0) {
			//we need to move them towards the front of the vector
			auto nMoved = m_current.back();
			m_current.back() = 0;
			for (auto it = m_current.rbegin(); it != m_current.rend(); ++it) {
				if (*it != 0) {
					--(*it);
					*(--it) = nMoved + 1;
					return true;
				}
			}
			assert(false);
		}
		//else
		for (auto it = m_current.rbegin(); it != m_current.rend(); ++it) {
			if (*it != 0) {
				--(*it);
				*(--it) = 1;
				return true;
			}
		}
		assert(false);
		return false;
	}
}
