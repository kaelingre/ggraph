#ifndef GKA_UTIL_HPP
#define GKA_UTIL_HPP

#include <type_traits>
#include <typeinfo>
#include <algorithm>
#include <string>
#include <vector>
#include <functional>
#include <sstream>
#include <memory>
#include <cstring>
#include <cassert>

namespace ggraph {
	/**
	 * nicely print the type of a variables
	 * use as: type_name<decltype(a)>()
	 */
	template <typename T>
	std::string type_name() {
		typedef typename std::remove_reference<T>::type TR;
		std::unique_ptr<char, void(*)(void*)> own
			(
/*#ifndef _MSC_VER
                abi::__cxa_demangle(typeid(TR).name(), nullptr,
									nullptr, nullptr),
									#else*/
                nullptr,
//#endif
                std::free
				);
		std::string r = own != nullptr ? own.get() : typeid(TR).name();
		if (std::is_const<TR>::value)
			r += " const";
		if (std::is_volatile<TR>::value)
			r += " volatile";
		if (std::is_lvalue_reference<T>::value)
			r += "&";
		else if (std::is_rvalue_reference<T>::value)
			r += "&&";
		return r;
	}

	/**
	 * gives the inverse(!) permutation that would sort the given vector
	 * (like Mathematica's "Ordering" function)
	 */
	template <typename T, typename Compare = std::less<T> >
	std::vector<std::size_t> ordering(const std::vector<T>& vec_, Compare c = Compare()) {
		std::vector<std::size_t> ret(vec_.size(), 0);
		for (std::size_t i = 0, size = vec_.size(); i != size; ++i) {
			ret[i] = i;
		}
		std::sort(ret.begin(), ret.end(), [&](const std::size_t& a_, const std::size_t& b_) {
				return c(vec_[a_], vec_[b_]);
			});
		return ret;
	}

	/**
	 * computes the parity of the permutation vect_
	 * vect_ needs to be vector filled with numbers 0,...,vect_.size()
	 * true = 1 means odd permutation
	 * false = 0 means even permutation
	 */
	template <typename T>
	bool parity(const std::vector<T>& vect_) {
		//idea: find all the cycles
		//      then: parity is the $sum_k (k-1)%2$ where k is the length of a cycle
		bool res = false;
		std::vector<bool> visited(vect_.size(), false);
		while (true) {
			auto it = std::find(visited.begin(), visited.end(), false);
			if (it == visited.end()) {
				break;
			}

			unsigned int counter = 1;
			const T start = vect_[it - visited.begin()];
			visited[start] = true;
			T el = vect_[start];
			while (el != start) {
				assert(!visited[el]);
				visited[el] = true;
				el = vect_[el];
				++counter;
			}
			if ((counter-1)%2 == 1) {
				res = !res;
			}
		}		

		return res;
	}


	/**
	 * finds the minimal element in the vector by also comparing the following elements
	 * in case of a tie (with the same Less function)
	 */
	template <typename T, typename Less = std::less<T> >
	typename std::vector<T>::iterator cyclicMinElement(std::vector<T>& vec_, Less l = Less()) {
		if (vec_.size() <= 1) {
			return vec_.begin();
		}
		typename std::vector<T>::iterator min = vec_.begin();
		const auto end = vec_.end();
		for (auto it = min + 1; it != end; ++it) {
			if (l(*it, *min)) {
				min = it;
			} else if (!l(*min, *it)) {
				auto it1 = it + 1;
				auto it2 = min + 1;
				//check if we reached end
				if (it1 == end) {
					it1 = vec_.begin();
				}
				if (it2 == end) {
					it2 = vec_.begin();
				}
				while (it1 != it) {
					//compare
					if (l(*it1, *it2)) {
						min = it;
					} else if (l(*it2, *it1)) {
						break;
					}

					//step
					++it1;
					++it2;
					//check if we reached end
					if (it1 == end) {
						it1 = vec_.begin();
					}
					if (it2 == end) {
						it2 = vec_.begin();
					}
				}
			}
		}

		return min;
	}

	/**
	 * finds all minimal elements in the vector by also comparing the following elements
	 * in case of a tie (with the same Less function)
	 * (tie means !(a < b) && !(b < a), i.e. a==b)
	 */
	template <typename T, typename Less = std::less<T> >
	std::vector<typename std::vector<T>::iterator> cyclicMinElements(
		std::vector<T>& vec_, Less l = Less()) {
		std::vector<typename std::vector<T>::iterator> ret;
		ret.push_back(vec_.begin());
		if (vec_.size() <= 1) {
			return ret;
		}
		
		const auto end = vec_.end();
		for (auto it = vec_.begin() + 1; it != end; ++it) {
			if (l(*it, *ret.front())) {
				//new min
				ret.clear();
				ret.push_back(it);
			} else if (!l(*ret.front(), *it)) {
				auto it1 = it + 1;
				auto it2 = ret.front() + 1;
				//check if we reached end
				if (it1 == end) {
					it1 = vec_.begin();
				}
				if (it2 == end) {
					it2 = vec_.begin();
				}
				bool foundDifference = false;
				while (it1 != it) {
					//compare
					if (l(*it1, *it2)) {
						//new min
						ret.clear();
						ret.push_back(it);
						foundDifference = true;
						break;
					} else if (l(*it2, *it1)) {
						foundDifference = true;
						break;
					}

					//step
					++it1;
					++it2;
					//check if we reached end
					if (it1 == end) {
						it1 = vec_.begin();
					}
					if (it2 == end) {
						it2 = vec_.begin();
					}
				}
				if (!foundDifference) {
					ret.push_back(it);
				}
			}
		}

		return ret;
	}

	/**
	 * wrap stream printing to char* printing
	 */
	template <typename T>
	char* print(const T& obj_) {
		std::stringstream ss;
		ss << obj_;

		std::string str = std::move(ss.str());

		char* cstr = new char[str.length() + 1];
		std::strcpy(cstr, str.c_str());

		return cstr;
	}
	template <typename T>
	char* printCallable(T callable_) {
		std::stringstream ss;
		callable_(ss);

		std::string str = std::move(ss.str());

		char* cstr = new char[str.length() + 1];
		std::strcpy(cstr, str.c_str());

		return cstr;
	}

	/**
	 * default print for a vector into mathematica lists
	 */
	template <typename T>
	std::ostream& operator<<(std::ostream& ostream_, const std::vector<T>& vector_) {
		ostream_ << '{';
		bool first = true;
		for (auto& e : vector_) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_  << e;
		}
		ostream_ << '}';
		return ostream_;
	}
}


#endif /*GKA_UTIL_HPP*/
