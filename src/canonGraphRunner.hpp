#ifndef GKA_CANONGRAPHRUNNER_HPP
#define GKA_CANONGRAPHRUNNER_HPP

#include "ggraph_config.h"

#include <set>
#include <stack>

#include "utility/runner.hpp"
#include "graph.hpp"

namespace ggraph {
	struct defaultBackEdgeVisitor {
		void operator()(const graph::edge& e_) {
		}
	};
	/**
	 * depth-first-search runner over vertices canonically
	 * allows for a backEdgeVisitor
	 * TODO: improve how the backEdge visitor is captured (reference?)
	 */
	template <typename backEdgeVisitor = defaultBackEdgeVisitor>
	class canonGraphRunner : public runner<graph::vert> {
	public:
		//**************************************************
		// constructor
		//**************************************************
		//this runner searches in all directions starting from given vertex
		canonGraphRunner(const graph::vert& v_,
						 backEdgeVisitor vis_ = defaultBackEdgeVisitor());
		//this runner does NOT search in the direction of given edge starting
		//from given vertex (edge should be connected to given vertex)
		canonGraphRunner(const graph::vert& v_, const graph::edge& e_,
						 backEdgeVisitor vis_ = defaultBackEdgeVisitor());

		//**************************************************
		// methods
		//**************************************************
		graph::vert const& getCurrent() const override {
			return m_path.top().first;
		}

		graph::edge nextEdge() const {
			if (m_path.empty()) {
				return nullptr;
			}
			//else
			return (*m_path.top().first)[m_path.top().second.second];
		}

		graph::edge prevEdge() const {
			if (m_path.empty()) {
				return nullptr;
			}
			//else
			return (*m_path.top().first)[m_path.top().second.first];
		}
		
		bool next() override;
		
	private:
		//**************************************************
		// members
		//**************************************************
		graph::edge m_edge;
		backEdgeVisitor m_vis;

		std::set<graph::vert> m_visited;
		//pairs of vertex and a pair of starting edge and at which edge we turned
		std::stack<std::pair<graph::vert, std::pair<const std::size_t, std::size_t> > >
		m_path;
	};

	template <typename backEdgeVisitor>
	canonGraphRunner<backEdgeVisitor>::canonGraphRunner(const graph::vert& v_,
														backEdgeVisitor vis_)
		: m_edge(nullptr), m_vis(vis_) {
		if (!v_) {
			runner<graph::vert>::m_isRunning = false;
			return;
		}
		m_path.push(std::make_pair(v_, std::make_pair(0,0)));
		m_visited.insert(v_);
	}

	template <typename backEdgeVisitor>
	canonGraphRunner<backEdgeVisitor>::canonGraphRunner(const graph::vert& v_,
														const graph::edge& e_,
														backEdgeVisitor vis_)
		: m_edge(e_), m_vis(vis_) {
		if (!v_) {
			runner<graph::vert>::m_isRunning = false;
			return;
		}
		m_path.push(std::make_pair(v_, std::make_pair(0, v_->front() == e_ ? 1 : 0)));
		m_visited.insert(v_);
	}

	template <typename backEdgeVisitor>
	bool canonGraphRunner<backEdgeVisitor>::next() {
		if (!runner<graph::vert>::isRunning()) {
			return false;
		}

		auto& v = m_path.top().first;
		const auto vSize = v->size();
		auto& eStart = m_path.top().second.first;
		auto& ePos = m_path.top().second.second;

		if (ePos > vSize && (ePos % vSize) == eStart) {
			//we visited all edges starting from this vertex
			m_path.pop();
			if (m_path.empty()) {
				runner<graph::vert>::m_isRunning = false;
				return false;
			}
			if ((*v)[(++ePos) % vSize] == m_edge) {
				++ePos;
			}
			//call recursively
			return next();
		}
		//else
		auto newV = (*v)[ePos % vSize]->other(v);
		
		if (m_visited.find(newV) != m_visited.end()) {
			//alread visited
			//we found a back edge, call visitor
			m_vis((*v)[ePos % vSize]);

			//continue
			++ePos;
			//call recursively
			return next();
		}
		//else (not yet visited)
		const auto itE = std::find(newV->begin(), newV->end(), (*v)[ePos % vSize]);
		assert(itE != newV->end());
		const auto newEPos = itE - newV->begin();
		m_path.push(std::make_pair(newV, std::make_pair(newEPos, newEPos)));
		m_visited.insert(newV);

		return true;
	}

	
}
#endif /*GKA_CANONGRAPHRUNNER_HPP*/
