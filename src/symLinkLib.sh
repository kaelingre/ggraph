#!/bin/sh

mkdir -p "${MESON_SOURCE_ROOT}/gGraph/LibraryResources/$2/"
ln -f -s "${MESON_INSTALL_DESTDIR_PREFIX}/$3/libggraph.so" "${MESON_SOURCE_ROOT}/gGraph/LibraryResources/$2/$1"
