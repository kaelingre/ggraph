#ifndef GKA_EQNS_HPP
#define GKA_EQNS_HPP

#include "ggraph_config.h"
#include "graphLabelling.hpp"

#include <boost/rational.hpp>

namespace ggraph {
	struct rule;
	typedef std::map<const graph*, std::vector<rule> > intSyms_t;

	struct term {
		//**************************************************
		// constructor
		//**************************************************
		/**
		 * immediately canonicalizes the graph  and it's labelling and finds the graph
		 * inside gs to set the graph pointer (and the main labels) correctly
		 * (the label canonicalization is not perfect yet, it for example misses
		 *  undirected multi-edges and some tadpole symmetries)
		 * if the graph is not found, m_graph is set to nullptr
		 */
		term(labelledGraph g_, const labelledGraphs& gs_, bool& sign_,
			 const std::string& head_ = "n");

		//**************************************************
		// operators
		//**************************************************
		friend bool operator<(const term& left_, const term& right_);
		
		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const term& t_);

		/**
		 * prints according to main labels
		 */
		void printNum(std::ostream& ostream_ = std::cout) const;
		
		/**
		 * canonicalize the labelling
		 * (i.e. use the symmetries of the graph and momentum conservation
		 *  to rewrite it in a canonical form)
		 * returns if there is a minus sign introduced in the mapping
		 * (the label canonicalization is not perfect yet, it for example misses
		 *  undirected multi-edges and some tadpole symmetries)
		 */
		bool canonLabelling() {
			auto copy = *m_graph;
			return canonLabellingHelper(copy);
		}
		bool canonLabellingHelper(graph& g_);
		
		//**************************************************
		//members
		//**************************************************
		const graph* m_graph;//canonicalized graph with external numbers all = 1
		graphLabelling m_labelling;
		std::string m_head;//in order to print
	};

	typedef boost::rational<int> ratio;
	struct sum : public std::map<term, ratio> {
		//**************************************************
		// constructors
		//**************************************************
		sum() {};
		sum(const term& t_, const ratio& r_)
			: std::map<term, ratio>({{t_,r_}}) {}
		sum(term&& t_, const ratio& r_)
			: std::map<term, ratio>({{std::move(t_),r_}}) {}
		
		//**************************************************
		// operators
		//**************************************************
		friend sum operator-(const sum& sum_);
		friend sum operator-(sum&& sum_);
		sum& operator+=(const sum& right_);
		sum& operator-=(const sum& right_);
		friend sum operator+(sum left_, const sum& right_) {
			return left_ += right_;
		}
		friend sum operator-(sum left_, const sum& right_) {
			return left_ -= right_;
		}
		friend sum operator+(const sum& left_, sum&& right_) {
			return std::move(right_) + left_;
		}
		sum& operator*=(const ratio& ratio_);
		friend sum operator*(sum left_, const ratio& right_) {
			return left_ *= right_;
		}
		friend sum operator*(const ratio& left_, sum right_) {
			return right_ *= left_;
		}

		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const sum& s_);
		void negate();

		/**
		 * replaces the label with given ID_ by the sum of terms in repl_
		 * in all summands
		 */
		void replaceLabel(const std::size_t& ID_, const label& repl_);
		/**
		 * as before, but replaces all the ID's given simultaneously
		 */
		void replaceLabel(const std::map<std::size_t, label>& repl_);

		/**
		 * applies the replacement rule
		 * returns if there was a replacement happening
		 */
		bool replace(const rule& rule_);

		/**
		 * canonicalizes all the labellings
		 */
		void canonLabelling();
	};

	/**
	 * optimized replacement rule term->sum (with momentum label patterns!)
	 */
	struct rule {
		//**************************************************
		// constructor
		//**************************************************
		rule(const term& from_, const sum& to_) : m_from(from_), m_to(to_) {
			initialize();
		}
		rule(term&& from_, sum&& to_) : m_from(std::move(from_)), m_to(std::move(to_)) {
			initialize();
		}

		//**************************************************
		// operators
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const rule& rule_);

		//**************************************************
		// members
		//**************************************************
		term m_from;
		sum m_to;

		//**************************************************
		// methods
		//**************************************************
		void initialize();

		/**
		 * applies the relace rules to RHS, i.e. m_to
		 * returns if there was a replacement happening
		 */
		bool replace(const rule& r_);
	};

	struct eqn {
		//**************************************************
		// constructor
		//**************************************************
		eqn(const sum& LHS_, const sum& RHS_) : m_LHS(LHS_), m_RHS(RHS_) {}
		eqn(sum&& LHS_, sum&& RHS_) : m_LHS(std::move(LHS_)), m_RHS(std::move(RHS_)) {}

		//**************************************************
		//methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const eqn& eqn_);
		
		/**
		 * if LHS consists only of one term, we con solve for that term and make
		 * it into a replacement rule
		 */
		rule toRule() const;

		/**
		 * applies the replacement rule
		 * returns if there was a replacement happening
		 */
		bool replace(const rule& rule_);

		/**
		 * canonicalizes all the labellings
		 */
		void canonLabelling() {
			m_LHS.canonLabelling();
			m_RHS.canonLabelling();
		}

		/**
		 * tries to check if two terms on the LHS are equal up to a flavor symmetry and
		 * combines them if that's the case (i.e. it assumes that a general flavor sym holds)
		 */
		void combineByFlavorSym(const std::vector<flavor_t>& fls_);

		//**************************************************
		// members
		//**************************************************
		sum m_LHS;
		sum m_RHS;
	};

	class eqns : public std::vector<eqn> {
	public:
		//**************************************************
		// constructors
		//**************************************************
		eqns() {}
		eqns(const std::vector<eqn>& eqns_) : std::vector<eqn>(eqns_) {}
		eqns(std::vector<eqn>&& eqns_) : std::vector<eqn>(std::move(eqns_)) {}

		//**************************************************
		// factories
		//**************************************************
		/**
		 * this function right now just delivers 0==0 statements since
		 * symmetry equations are trivially solved by labelling canonicalizations
		 */
		static eqns makeSymEqns(const labelledGraphs& gs_,
								const std::string& head_ = "n");

		static eqns makeJacobiEqns(const labelledGraphs& gs_,
								   const std::string& head_ = "n");

		static eqns makeTwoTermEqns(const labelledGraphs& gs_,
									const std::string& head_ = "n");

		static eqns  makeTwoTermFlEqns(const labelledGraphs& gs_,
									   const std::array<flavor_t, 3>& fls_,
									   const std::string& head_ = "n");

		static eqns makeScalarTwoTermEqns(const labelledGraphs& gs_,
										  const std::string& head_ = "n");

		static eqns makeScalarTwoTermFlEqns(const labelledGraphs& gs_,
											const std::array<flavor_t, 3>& fls_,
											const std::string& head_ = "n");

		static eqns makeFlavorSymEqns(const labelledGraphs& gs_,
									  const std::array<flavor_t, 3>& fls_,
									  const std::string& head_ = "n",
									  const bool& onlyTruePerms_ = true);

		static eqn makeNeq4DecompID(const labelledGraph& g_,
									const labelledGraphs& gsNeq4_,
									const labelledGraphs& gsNeq2_,
									const std::string& headNeq4_ = "nNeq4",
									const std::string& headNeq2_ = "n");

		static eqn makeNeq4DecompIDS(const labelledGraph& g_,
									 const labelledGraphs& gsNeq4_,
									 const labelledGraphs& gsNeq2_,
									 const std::string& headNeq4_ = "nNeq4",
									 const std::string& headNeq2_ = "n");

		static eqn makeNeq2DecompID(const labelledGraph& g_,
									const labelledGraphs& gsNeq2_,
									const labelledGraphs& gsNeq1_,
									const std::string& headNeq2_ = "nNeq2",
									const std::string& headNeq1_ = "n");

		static eqn makeNeq2DecompIDS(const labelledGraph& g_,
									 const labelledGraphs& gsNeq2_,
									 const labelledGraphs& gsNeq1_,
									 const std::string& headNeq2_ = "nNeq2",
									 const std::string& headNeq1_ = "n");

		static eqns makeRevSymEqns(const labelledGraphs& gs_,
								   const std::string& head_ = "n");

		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const eqns& eqns_);

		void add(const std::vector<eqn>& eqns_);
		void add(std::vector<eqn>&& eqns_);

		/**
		 * reduces the system of eqns by solving every graph appearing in terms
		 * of masters and known graphs
		 * Not used masters are removed from the vector of masters_
		 * Returns replacement rules for each graph it finds in any eqn.
		 * if it's able to completely reduce the system, it will set *this
		 * to a system of remaining consistency equations
		 *
		 * if flavors are given, it will try combine terms that are equal up to flavor symmetries
		 * (i.e. it applies flavor symmetries for given flavor in order to simplify)
		 */
		std::vector<rule> reduce(std::vector<const graph*>& masters_,
								 const std::vector<const graph*>& knowns_,
								 const std::vector<flavor_t>& fls_ = {});

		/**
		 * applies the rule to all eqns and resorts them by size
		 * returns if there was a replacement happening
		 */
		bool replace(const rule& rule_);
		/**
		 * applies all the rules in given order (NOT simultaneously or repeated)
		 */
		bool replace(const std::vector<rule>& rules_);

		/**
		 * canonicalizes all the labellings
		 */
		void canonLabelling() {
			for (auto& e : *this) {
				e.canonLabelling();
			}
		}

		/**
		 * tries to check if two terms on the LHS are equal up to a flavor symmetry and
		 * combines them if that's the case (i.e. it assumes that a general flavor sym holds)
		 */
		void combineByFlavorSym(const std::vector<flavor_t>& fls_);

	private:
		//**************************************************
		// methods
		//**************************************************
		void remove0Eq0();

		void moveToTheRight(const graph* g_);

		/**
		 * static helper for reduce
		 * detects internal symmetries (from e.g. flavor symmetries), that are not captured by
		 * canonical labelling
		 * @param eqns_: two term identities consisting of twice the same graph (will select only
		 *               the ones that have internal syms)
		 * @return: map from a graph pointer to a list of replacement rules
		 */
		static intSyms_t prepareIntSyms(const std::vector<eqn>& eqns_);

		/**
		 * inverse sort by length(m_LHS), and then length(total) 
		 */
		struct sorter {
			bool operator()(const eqn& left_, const eqn& right_);
		};
		static sorter m_sorter;
	};

	//**************************************************
	// free functions
	//**************************************************
	/**
	 * applies the set of rules on itself until no further change happenes
	 */
	std::vector<rule>& replaceRepeated(std::vector<rule>& rules_);

	ratio power(const ratio& rat_, std::size_t exp_);
}


#endif /*GKA_EQNS_HPP*/
