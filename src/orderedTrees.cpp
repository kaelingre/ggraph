#include "orderedTrees.hpp"

#include <algorithm>

#include "utility/permItRunner.hpp"
#include "util.hpp"

namespace ggraph {
	extOrdering& canonRotate(extOrdering& ordering_) {
		std::rotate(ordering_.begin(), cyclicMinElement(ordering_),
					ordering_.end());
		return ordering_;
	}

	std::vector<std::pair<flExt, std::size_t> > orderedTree::getExtOrdering() const {
		vert vBegin = m_verts.front().get();
		assert(vBegin->m_number > 0);

		//follow the graph around
		std::vector<std::pair<flExt, std::size_t> > ret;
		auto v = vBegin;
		auto nextE = v->front();
		do {
			if (v->m_number > 0) {
				ret.push_back(std::make_pair(
								  std::make_pair(
									  lineDirToExt(nextE->m_type, !nextE.m_dir),
									  nextE->m_flavor),
								  v->m_ID));
			}
			//step
			v = nextE->other(v);
			nextE.m_dir = !nextE.m_dir;
			nextE = v->next(nextE);
		} while (v != vBegin);
		return ret;
	}

	void orderedTree::canonicalize(const vert& v_) {
		canonicalizeOrdered(v_);
	}

	void orderedTree::number(const std::vector<int>& numbers_) {
		vert vBegin = m_verts.front().get();
		assert(vBegin->m_number > 0);

		//follow the graph around
		auto v = vBegin;
		auto nextE = v->front();
		auto it = numbers_.begin();
		do {
			if (v->m_number > 0) {
				v->m_number = (*it++);
			}
			//step
			v = nextE->other(v);
			nextE.m_dir = !nextE.m_dir;
			nextE = v->next(nextE);
		} while (v != vBegin);
		assert(it == numbers_.end());
	}
	
	std::ostream& operator<<(std::ostream& ostream_, const orderedTrees& trees_) {
		for (auto& eO : trees_) {
			//print ordering
			bool isFirst = true;
			for (auto& ext : eO.first) {
				if (isFirst) {
					isFirst = false;
				} else {
					ostream_ << ',';
				}
				ostream_ << ext.first << ':' << ext.second;
			}
			ostream_ << std::endl;
			//print all the graphs
			isFirst = true;
			ostream_ << '{';
			for (auto& g : eO.second) {
				if (isFirst) {
					isFirst = false;
				} else {
					ostream_ << ',';
				}
				ostream_ << g;
			}
			ostream_ << '}' << std::endl;
		}
		
		return ostream_;
	}

	std::vector<labelledGraph> orderedTrees::get(const extOrdering& extOrdering_,
												 const std::vector<int>& numbers_) {
		//get or construct the orderedTrees for given ordering
		auto it = find(extOrdering_);
		const mapped_type* set;
		if (it == end()) {
			set = &add(extOrdering_);
		} else {
			set = &(it->second);
		}

		//apply the numbering
		std::vector<labelledGraph> ret;
		for (auto oT : *set) {
			oT.number(numbers_);
			ret.push_back(labelledGraph(std::move(oT), true));
		}
		return ret;
	}

	const orderedTrees::mapped_type& orderedTrees::add(
		extOrdering extOrdering_) {
		add(graphs(extOrdering_, 0, m_couplings));
		//note: this might add and return an empty set if the ordering doesn't lead
		//to any tree graphs with that ordering
		return operator[](canonRotate(extOrdering_));
	}

	void orderedTrees::add(const graphs& gs_) {
		//makes a copy
		for (auto g : gs_) {
			//go through all vertex orderings
			std::vector<permItRunner<std::vector<graph::edge>::iterator> > runners;
			runners.reserve(g.nVerts());

			for (auto& v : g.getVerts()) {
				if (v->size() > 1) {
					runners.push_back(permItRunner(std::make_pair(v->begin() + 1,
																  v->end())));
				}
			}

			do {
				//construct the orderedTree
				orderedTree oT(g);

				//get the external ordering
				auto eOrdering = oT.getExtOrdering();
				//get all possible minima
				auto mins = cyclicMinElements(
					eOrdering,
					[] (const std::pair<flExt, std::size_t>& left_,
						const std::pair<flExt, std::size_t>& right_) {
						return left_.first < right_.first;
					});

				assert(mins.size() > 0);
				//construct the canonicalized extOrdering once!
				extOrdering eO;
				for (auto it = mins.front(); it != eOrdering.end(); ++it) {
					eO.push_back(it->first);
				}
				for (auto it = eOrdering.begin(); it != mins.front(); ++it) {
					eO.push_back(it->first);
				}
				//canonicalize for all possible mins
				for (auto& min : mins) {
					//make a copy
					auto copy = oT;
					copy.canonicalize(oT[min->second]);
					operator[](eO).insert(std::move(copy));
				}
				
				//step
				bool found = false;
				for (auto it = runners.begin(), end = runners.end(); it != end; ++it) {
					if (it->next()) {
						found = true;
						break;
					} else {
						it->reset();
					}
				}				
				if (!found) {
					//end of recursion
					break;
				}
			} while (true);
		}
	}
}
