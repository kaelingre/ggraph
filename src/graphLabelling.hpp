#ifndef GKA_GRAPHLABELLING_HPP
#define GKA_GRAPHLABELLING_HPP

#include "ggraph_config.h"

#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <cassert>
#include <cereal/access.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/base_class.hpp>


#include "graph.hpp"
#include "graphs.hpp"
#include "util.hpp"
#ifdef GGRAPH_USE_MMA
	#include <WolframLibrary.h>
#endif

namespace ggraph {
	/**
	   decomposition rules for lines
	   maps a line+flavor pair to a sum over flavor+ext (for direction)
	 */
	typedef std::map<flLine_t, std::vector<flExt> > decompRules;
	/**
	 * maps label identifier to prefactor
	 * i.e. the momentum label is the sum over the elements of the form
	 * k[identifier]*prefactor
	 */
	class label : public std::map<std::size_t, int> {
	public:
		//**************************************************
		//constructor
		//**************************************************
		label(const bool& null_ = true) : m_null(null_) {};
		label(const std::size_t& id_, const int& coeff_);
		label(const std::map<std::size_t, int>& map_);
		label(std::map<std::size_t, int>&& map_);
		label(std::istream& istream_);

		//**************************************************
		//operators
		//**************************************************
		friend label operator+(const label& label_) {
			return label_;
		}
		friend label operator+(label&& label_) {
			return std::forward<label>(label_);
		}
		friend label operator-(const label& label_);
		friend label operator-(label&& label_);
		label& operator+=(const label& right_);
		label& operator-=(const label& right_);
		friend label operator+(label left_, const label& right_);
		friend label operator-(label left_, const label& right_);
		friend label operator+(const label& left_, label&& right_) {
			return std::move(right_) + left_;
		}
		//TODO: implement operator- with move semantics

		//**************************************************
		//methods
		//**************************************************
		void negate();
		bool isZero() const {
			return empty();
		}
		bool isNull() const {
			return m_null;
		}
		void setNull() {
			assert(empty());
			m_null = true;
		}
		void unsetNull() {
			m_null = false;
		}

		friend std::ostream& operator<<(std::ostream& ostream_, const label& label_);

		size_t highestLabelID() const;//return hightest label identifier appearing

		/**
		 * permute label identifiers according to the permutation given
		 */
		void permute(const permutation& perm_);

		/**
		 * changes the sign of all labels with the given id(s)
		 * does NOT anything in the graph, only labels
		 */
		void changeSign(const std::size_t& id_);
		void changeSign(const std::vector<size_t>& ids_);

		/**
		 * replace k[ID] by the given label (i.e. the sum of these labels)
		 * if not there, nothing happens
		 */
		void replace(const std::size_t& ID_, label repl_);
		/**
		 * like above, but replaces all IDs simultaneously
		 */
		void replace(const std::map<std::size_t, label>& repl_);

	private:
		//**************************************************
		//static
		//**************************************************
		bool m_null;
		
		//**************************************************
		//static const
		//**************************************************
		static const std::string STD_LABEL_STR;

		//**************************************************
		//serialization
		//**************************************************
		friend class cereal::access;
		template <typename Archive>
		void serialize(Archive& ar_) {
			ar_(cereal::base_class<std::map<std::size_t, int> >(this));
		}
	};
	
	class graphLabelling : public std::vector<label> {
		friend class labelledGraph;
	public:
		//**************************************************
		// constructors
		//**************************************************
		graphLabelling() {}
		/**
		 * automatically labels a graph
		 * (assumes that the graph is canonicalized<--not anymore I think)
		 * useExtNumbers_ specifies if the m_numbers of exts should be used
		 * or if the momentum labels should be given according to their
		 * m_ID + 1
		 * (e.g. use false if all exts have m_number == 1)
		 */
		graphLabelling(const graph& graph_, const bool& useExtNumbers_ = false);

		graphLabelling(std::vector<label>&& labels_,
					   std::vector<std::pair<std::size_t, bool> >&& mainLabels_)
			: std::vector<label>(std::move(labels_)),
			  m_mainLabels(std::move(mainLabels_)) {}

		//**************************************************
		// operators
		//**************************************************
		/**
		 * strict ordering of labellings (of the same graph).
		 * does only compare the labels, NOT the mainLabels
		 */
		friend bool operator<(const graphLabelling& left_, const graphLabelling& right_);

		friend std::ostream& operator<<(std::ostream& ostream_, const graphLabelling& gL_);

		//**************************************************
		// methods
		//**************************************************
		/**
		 * relabels the given edge according to the connected edges at given vertex
		 * the m_dir of e_ needs to be wrt. given v_
		 * returns reference to new label
		 */
		label& relabel(const graph::edge& e_, const graph::vert& v_);

		/**
		 * prints the mainLabels with commas in between
		 */
		void printArgs(std::ostream& ostream_ = std::cout) const;

		const std::vector<std::pair<std::size_t, bool> >& getMainLabels() const {
			return m_mainLabels;
		}

		std::vector<std::pair<std::size_t, bool> >& getMainLabels() {
			return m_mainLabels;
		}

		/**
		 * recomputes the labelling for the given graph (which should be a cut)
		 * i.e. puts individual labels for each internal leg
		 */
		void labelCut(const graph& graph_);

		/**
		 * permutes momentum label identifiers according to the permutation given
		 */
		void permute(const permutation& perm_);
		
		/**
		 * permute the edges according to graphMap (and also changes sign)
		 */
		void permuteEdges(const graphMap& map_);

		/**
		 * changes the sign of all labels with the given id(s)
		 */
		void changeSign(const std::size_t& id_);
		void changeSign(const std::vector<size_t>& ids_);

		/**
		 * replace k[ID] by the given label (i.e. the sum of these labels)
		 * if not there, nothing happens
		 */
		void replaceLabel(const std::size_t& ID_, const label& repl_);
		/**
		 * as above, but replaces all IDs simultaneously
		 */
		void replaceLabel(const std::map<std::size_t, label>& repl_);

	private:
		//**************************************************
		// members
		//**************************************************
		//a vector with edge IDs pointing to edges with k[i] labels and if
		//the direction is flipped (i.e. true means that the direction is flipped!!!)
		std::vector<std::pair<std::size_t, bool> > m_mainLabels;
		
		//**************************************************
		// methods
		//**************************************************
		/**
		 * construction helper: labels all internal lines automatically
		 */
		void labelInternal(const graph& graph_);

		/**
		 * construction helper: labels all internal lines automatically for a cut
		 */
		void labelInternalCut(const graph& graph_);
	};

	class labelledGraph {
		friend class labelledGraphs;
		friend class cut;
	public:
		//**************************************************
		// constructor
		//**************************************************
		/**
		 * useExtNumbers_ specifies if the m_numbers of exts should be used
		 * or if the momentum labels should be given according to their
		 * m_ID + 1
		 * (e.g. use false if all exts have m_number == 1)
		 */
		labelledGraph() {}
		labelledGraph(const graph& graph_, const bool& useExtNumbers_ = false)
			: m_graph(graph_), m_labelling(m_graph, useExtNumbers_) {}
		labelledGraph(graph&& graph_, const bool& useExtNumbers_ = false)
			: m_graph(std::move(graph_)), m_labelling(m_graph, useExtNumbers_) {}
		labelledGraph(graph&& graph_, graphLabelling&& labelling_)
			: m_graph(std::move(graph_)), m_labelling(std::move(labelling_)) {}

		//from mathematica
		labelledGraph(std::stringstream& stream_);

		//**************************************************
		// operator
		//**************************************************
		/**
		 * only compare the underlying graph, not the labelling!
		 */
		friend bool operator<(const labelledGraph& left_, const labelledGraph& right_) {
			return left_.m_graph < right_.m_graph;
		}
		friend bool operator<(const graph& left_, const labelledGraph& right_) {
			return left_ < right_.m_graph;
		}
		friend bool operator<(const labelledGraph& left_, const graph& right_) {
			return left_.m_graph < right_;
		}
		friend bool operator==(const labelledGraph& left_, const labelledGraph& right_) {
			return left_.m_graph == right_.m_graph;
		}
		friend bool operator==(const graph& left_, const labelledGraph& right_) {
			return left_ == right_.m_graph;
		}
		friend bool operator==(const labelledGraph& left_, const graph& right_) {
			return left_.m_graph == right_;
		}
		friend bool operator!=(const labelledGraph& left_, const labelledGraph& right_) {
			return left_.m_graph != right_.m_graph;
		}

		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const labelledGraph& g_);

		void printVerbose(std::ostream& ostream_ = std::cout) const;

		/**
		 * gets all graphs related to this one by a symmetry generator
		 * (but to print the symmetry equations it's easier, see printSyms)
		 * first element in pair is a possible sign
		 */
		std::vector<std::pair<bool, labelledGraph> > syms() const;

		/**
		 * gets all pairs of graphs this one is related to via jacobies,
		 * commutation relations, and three-flavor color algebra relations
		 * (ignores identities acting on the edge of a tadpole)
		 */
		std::vector<std::pair<labelledGraph, labelledGraph> > jacobies() const;

		/**
		 * gets all the graphs that are related to this one via a two-term identity
		 * (i.e. two quarks-antiquark-pairs with the same flavor)
		 * it also returns the sign (true=there's a minus sign)
l		 */
		std::vector<std::pair<bool, labelledGraph> > twoTerm() const;

		/**
		 * gets all the graphs that are related to this one via a three-flavor
		 * two-term identity
		 * (i.e. two quark-antiquark-pairs of different flavor)
		 * it also returns the sign (true=there's a minus sign)
		 */
		std::vector<std::pair<bool, labelledGraph> > twoTermFl(
			const std::array<flavor_t, 3>& fls_) const;

		/**
		 * gets all the graph pairs that are related to this via a
		 * scalar two-term identity (two scalar pairs of the same flavor!)
l		 */
		std::vector<std::pair<labelledGraph, labelledGraph> > scalarTwoTerm() const;

		/**
		 * gets all the graph pairs that are related to this via a three-flavor
		 * scalar two-term identity (two scalar pairs of different flavor!)
l		 */
		std::vector<std::pair<labelledGraph, labelledGraph> > scalarTwoTermFl(
			const std::array<flavor_t, 3>& fls_) const;

		/**
		 * prints the numerator according to mainLabels
		 */
		void printNum(const std::string& head_, std::ostream& ostream_ = std::cout) const;

		/**
		 * prints the numerator using the given mainLabels_ mapped with map onto *this
		 * (idea: map_ is a map from canonical version of *this to *this,
		 *        mainLabels_ are the labels of the canonical graph)
		 * also prints the sign (+/-) given in the map in front
		 */
		void printNum(const std::string& head_, const graphMap& map_,
					  const std::vector<std::pair<std::size_t, bool> >& mainLabels_,
					  std::ostream& ostream_ = std::cout) const;

		/**
		 * applies each generator of the automorphism group to the graph
		 * and prints it (printNum) with the corresponding exchanged labels
		 * (including multilines & tadpoles)
		 * graph needs to be canonicalized to work properly
		 */
		void printSyms(const std::string& head_, std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printSymsMMA(const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraph::printSyms,*this,head_,std::placeholders::_1));
		}
#endif

		/**
		 * returns the symmetry factor from (undirected! TODO?) multi-edges and tadpoles
		 */
		std::size_t countMultiEdgeTadpoleSyms() const {
			return m_graph.countMultiEdgeTadpoleSyms();
		}

		/**
		 * return sthe full symmetry factor from the underlying graph (TODO: undirected multi-edges&tadpoles)
		 */
		std::size_t symmetryFactor() const {
			return m_graph.symmetryFactor();
		}

		/**
		 * permutes the external numbers and momentum labels (also internal)
		 * according to the permutation given
		 * (graph is not canonical anymore after this step)
		 */
		void permute(const permutation& perm_);

		/**
		 * changes the sign of all labels with the given id(s)
		 * does NOT anything in the graph, only labels
		 */
		void changeSign(const std::size_t& id_) {
			m_labelling.changeSign(id_);
		}
		void changeSign(const std::vector<size_t>& ids_) {
			m_labelling.changeSign(ids_);
		}

		graph& getGraph() {
			return m_graph;
		}

		const graph& getGraph() const {
			return m_graph;
		}

		const graphLabelling& getLabelling() const {
			return m_labelling;
		}

		graphLabelling& getLabelling() {
			return m_labelling;
		}

		const std::vector<std::pair<std::size_t, bool> >& getMainLabels() const {
			return m_labelling.getMainLabels();
		}

		/**
		 * integrates all vertices and edges from the given graph into *this
		 * and connects them at vertices with the same m_number
		 * (i.e. removes the external vertex and one of the edges)
		 * the edges connected to the externals need to be of the same type
		 * and go in the same direction for directed edges
		 * In the given graph, the external vertices need to come first in the
		 * vertex list (or at least every external that will to be connected 
		 * needs to be belfore eventual fake vertices connected to it)
		 */
		void connect(labelledGraph&& other_);

		/**
		 * checks if there's an edge that connects to the same vertex on both sides
		 * (also directed lines are properly checked!)
		 */
		bool containsSimpleTadpole() const {
			return m_graph.containsSimpleTadpole();
		}

		/**
		 * checks if the diagram contains a bubble, i.e. if there exist two lines
		 * that connect the same two vertices
		 * (also directed lines are properly checked!)
		 */
		bool containsBubble() const {
			return m_graph.containsBubble();
		}

		/**
		 * checks if there's an edge that splits the graph into two-parts
		 * where one side has zero or one external leg
		 * (i.e. this edge has null momentum)
		 */
		bool containsNull() const {
			return m_graph.containsNull();
		}
		
		/**
		 * returns if at least one leg has the given flavor or flavor+lineType 
		 */
		template <typename T>
		bool containsFlavor(const T& fl_) const {
			return m_graph.containsFlavor(fl_);
		}

		/**
		 * permutes flavors according to the map
		 * flavors not appearing in the map won't be permuted
		 */
		void permuteFlavors(const std::map<flavor_t, flavor_t>& map_) {
			m_graph.permuteFlavors(map_);
		}

		/**
		 * transforms the given graph into a pure topological graph
		 * (i.e. strips away fake edges, flavor, line types,...)
		 * everything goes to simple gluon lines
		 * the map is from *this to the newely produced graph and is not
		 * trivially invertible since fake edges and vertices have been removed
		 */
		std::pair<labelledGraph, graphMap> toTopo() const;

		/**
		 * canonicalizes the graph AND the graphLabelling
		 * returns a map from inputIDs to canonical IDs
		 */
		graphMap canonMap();

		/**
		 * prints all the propgator momenta (unsquared!) as a list
		 */
		void printProps(std::ostream& ostream_) const;
#ifdef GGRAPH_USE_MMA		
		char* printPropsMMA() const {
			return printCallable(
				std::bind(&labelledGraph::printProps,*this,std::placeholders::_1));
		}
#endif

		/**
		 * replaces every internal(!) line of given flLine_t with all flExt_t given
		 * by the map. Only keeps graphs produce in this way that have couplings taken
		 * from the given set of couplings.
		 * We need to use flExt_t since we might want to use specific direction of
		 * quark lines.
		 */
		std::vector<labelledGraph> decompose(
			const decompRules map_,
			const std::vector<coupling>& couplings_) const;


		/**
		 * reverses all connected quark lines in all possible ways
		 * returns each graph together with a map from *this to the graph
		 */
		std::vector<std::pair<labelledGraph, graphMap> > reverseQuarkLines() const;

		/**
		 * reverses all quark directions at the same time
		 */
		labelledGraph reverseAllQuarks() const;

		/**
		 * count number of closed quark loops. each vertex is only allowed to have exactly zero
		 * or two quark lines of the same flavor (and arrows need to be consistent)
		 */
		std::size_t countQuarkLoops() const {
			return m_graph.countQuarkLoops();
	    }

		std::ostream& printColorFactor(std::ostream& ostream_ = std::cout) const;

#ifdef GGRAPH_USE_MMA		
		char* printColorFactorMMA() const {
			return printCallable(
				std::bind(&labelledGraph::printColorFactor,*this,std::placeholders::_1));
		}
#endif

		std::ostream& printSymmetryGroup(std::ostream& ostream_ = std::cout) const {
			return m_graph.printSymmetryGroup(ostream_);
		}

#ifdef GGRAPH_USE_MMA
		char* printSymmetryGroupMMA() const {
			return printCallable(
				std::bind(&labelledGraph::printSymmetryGroup,*this,std::placeholders::_1));
		}
#endif		

		//**************************************************
		// static members
		//**************************************************
		const static decompRules neq4to2DecomposeRules;
		const static decompRules neq4to2DecomposeRulesS;
		const static decompRules neq2to1DecomposeRules;
		const static decompRules neq2to1DecomposeRulesS;
		const static decompRules undirToDirDecomposeRules;

	private:
		//**************************************************
		// members
		//**************************************************
		graph m_graph;
		graphLabelling m_labelling;

		//**************************************************
		// methods
		//**************************************************
		std::pair<labelledGraph, labelledGraph> jacobies(const graph::edge& e_,
														 const graph::vert& v_) const;

		std::pair<labelledGraph, labelledGraph> jacobies2Dir(const graph::edge& e_,
														     const graph::vert& v_) const;

		std::pair<labelledGraph, labelledGraph> jacobies2Undir(const graph::edge& e_,
															   const graph::vert& v_) const;

		std::pair<bool, labelledGraph> twoTerm(const graph::edge& e_) const;

		std::pair<bool, labelledGraph> twoTermFl(const graph::edge& e_,
												 const flavor_t& fl_) const;

		std::pair<labelledGraph, labelledGraph> scalarTwoTerm(const graph::edge& e_) const;
		std::pair<labelledGraph, labelledGraph> scalarTwoTermFl(
			const graph::edge& e_, const flavor_t& fl_) const;

		//**************************************************
		// structs
		//**************************************************
		/**
		 * deleter functor used for the connect function,
		 * erases a given edge from the graphLabelling
		 */
		struct deleter {
			graphLabelling& m_labelling;
			deleter(graphLabelling& labelling_) : m_labelling(labelling_) {};
			void operator()(const std::size_t& id_) {
				m_labelling.erase(m_labelling.begin()+id_);
			}
		};
		deleter makeDeleter() {
			return deleter(m_labelling);
		}
	};

	class labelledGraphs : public std::set<labelledGraph> {
	public:
		//**************************************************
		// constructors
		//**************************************************
		labelledGraphs() {}
		/**
		 * useExtNumbers_ specifies if the m_numbers of exts should be used
		 * or if the momentum labels should be given according to their
		 * m_ID + 1
		 * (e.g. use false if all exts have m_number == 1)
		 *
		 * exactNV_ (!=0) specifies that only diagrams with this exact number of vertices
		 * should be constructed
		 */
		labelledGraphs(const graphs& graphs_, const bool& useExtNumbers_ = false);
		labelledGraphs(graphs&& graphs_, const bool& useExtNumbers_ = false);
		labelledGraphs(const std::vector<flExt>& exts_, const unsigned int& nLoops_,
					   const std::vector<coupling>& couplings_,
					   const bool& useExtNumbers_ = false, const std::size_t& exactNV_ = 0);

		//**************************************************
		// factories
		//**************************************************
		/**
		 * Makes all PM EFT graphs with undirected gravitons (GLUON lines) for the in-out formalism.
		 * @param order_ PM order
		 * @param nBH_ Number of black holes
		 * @param higherWLCoupling_ If higher order couplings of gravitons to WL should be taken into account
		 */
		static labelledGraphs makePMEFTGraphs(const std::size_t& order_, const std::size_t& nBH_ = 2,
											  const bool& higherWLCoupling_ = false);


		/**
		 * makes all PM EFT graphs with directed gravitons (QUARK lines) for the VARIATION of the action in
		 * the in-in formalism.
		 * all graphs will be directed such that all arrows point towards a single external vertex
		 * this "out" vertex is chosen to have the lowest possible external number
		 * higherWLCoupling_: if there should be higher point WL couplings
		 */
		static labelledGraphs makePMEFTGraphsInIn(const std::size_t& order_, const std::size_t& nBH_ = 2,
												  const bool& higherWLCoupling_ = false);

		/**
		 * makes all PM EFT graphs with directed gravitons and a single external radiating graviton.
		 * Gravitons are "quark" lines with flavor 0
		 * all graphs will be directed such that all arrows point towards a single external vertex=source
		 * this "out" vertex is chosen to have the lowest possible external number
		 * higherWLCoupling_: if there should be higher point WL couplings
		 * the graviton has external number nBH_+1
		 */
		static labelledGraphs makePMEFTRadGraphs(const std::size_t& order_, const std::size_t& nBH_ = 2,
												 const bool& higherWLCoupling_ = false);

		/**
		 * makes all tree graphs to compute the classical impulse using the GR+WL theory
		 */
		static labelledGraphs makeGRWLImpulseGraphs(const std::size_t& orderG_);

		/**
		 * makes all tree graphs to compute the classical action using the GR+WL theory
		 */
		static labelledGraphs makeGRWLSGraphs(const std::size_t& orderG_);

		//**************************************************
		// operators
		//**************************************************
		const labelledGraph& operator[](const std::size_t& n_) const;

		//**************************************************
		// methods
		//**************************************************
		friend std::ostream& operator<<(std::ostream& ostream_, const labelledGraphs& graphs_);

		void printVerbose(std::ostream& ostream_ = std::cout) const;

		/**
		 * canonicalizes the given graph (or a copy thereof) and tries to find it inside *this
		 * if found, returns iterator to its canonicalized version
		 * and a map from given graph to its canonicalized version
		 * if not found, iterator points to this->end()
		 */
		std::pair<std::set<labelledGraph>::const_iterator, graphMap> findMap(
			const labelledGraph& g_) const;
		std::pair<std::set<labelledGraph>::const_iterator, graphMap> findMap(
			labelledGraph&& g_) const;
		std::pair<std::set<labelledGraph>::iterator, graphMap> findMap(
			const labelledGraph& g_);
		std::pair<std::set<labelledGraph>::iterator, graphMap> findMap(
			labelledGraph&& g_);

		void printSyms(const std::string& head_,
					   std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printSymsMMA(const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printSyms,*this,head_,std::placeholders::_1));
		}
#endif	   

		void printJacobies(const std::string& head_,
						   std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printJacobiesMMA(const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printJacobies,*this,head_,
						  std::placeholders::_1));
		}
#endif		

		void printTwoTerm(const std::string& head_,
						  std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printTwoTermMMA(const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printTwoTerm,*this,head_,
						  std::placeholders::_1));
		}
#endif		

		void printTwoTermFl(const std::string& head_, const std::array<flavor_t, 3>& fls_,
						  std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printTwoTermFlMMA(const std::string& head_,
								const std::vector<mint>& fls_) const {
			assert(fls_.size() == 3);
			std::array<flavor_t, 3> arr;
			std::copy_n(fls_.begin(), 3, arr.begin());
			return printCallable(
				std::bind(&labelledGraphs::printTwoTermFl,*this,head_,arr,
						  std::placeholders::_1));
		}
#endif		

		void printScalarTwoTerm(const std::string& head_,
								std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printScalarTwoTermMMA(const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printScalarTwoTerm,*this,head_,
						  std::placeholders::_1));
		}
#endif		

		void printScalarTwoTermFl(const std::string& head_,
								  const std::array<flavor_t, 3>& fls_,
								  std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printScalarTwoTermFlMMA(const std::string& head_,
									  const std::vector<mint>& fls_) const {
			assert(fls_.size() == 3);
			std::array<flavor_t, 3> arr;
			std::copy_n(fls_.begin(), 3, arr.begin());
			return printCallable(
				std::bind(&labelledGraphs::printScalarTwoTermFl,*this,head_,arr,
						  std::placeholders::_1));
		}
#endif		

		/**
		 * permutes the given flavors and looks for mappings back to itself
		 * or other graphs
		 */
		void printFlavorSyms(const std::string& head_,
							 const std::vector<flavor_t>& fls_,
							 std::ostream& ostream_ = std::cout,
							 const bool& onlyTruePerms_ = true) const;
#ifdef GGRAPH_USE_MMA		
		char* printFlavorSymsMMA(const std::string& head_,
								 const std::vector<mint>& fls_, const bool& onlyTruePerms_ = true) const {
			std::vector<flavor_t> fls(fls_.begin(), fls_.end());
			return printCallable(
				std::bind(&labelledGraphs::printFlavorSyms,*this,head_,fls,
						  std::placeholders::_1, onlyTruePerms_));
		}
#endif		

		/**
		 * prints neq4 decomposition identity (i.e. sum of N=2 graphs)
		 * *this needs to contain all of these graphs!
		 * (i.e. *this needs to have been constructed with correct couplings)
		 */
		void printNeq4DecompID(const std::string& head_, const labelledGraph& g_,
							   std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printNeq4DecompIDMMA(const labelledGraph& g_, const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printNeq4DecompID,*this,head_,g_,
						  std::placeholders::_1));
		}
#endif		

		/**
		 * the same, but for undirected graphs
		 */
		void printNeq4DecompIDS(const std::string& head_, const labelledGraph& g_,
							   std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printNeq4DecompIDMMAS(const labelledGraph& g_, const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printNeq4DecompIDS,*this,head_,g_,
						  std::placeholders::_1));
		}
#endif		

		/**
		 * prints neq2 decomposition identity (i.e. sum of N=1 graphs)
		 * *this needs to contain all of these graphs!
		 * (i.e. *this needs to have been constructed with correct couplings)
		 */
		void printNeq2DecompID(const std::string& head_, const labelledGraph& g_,
							   std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printNeq2DecompIDMMA(const labelledGraph& g_, const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printNeq2DecompID,*this,head_,g_,
						  std::placeholders::_1));
		}
#endif		


		/**
		 * the same, but for undirected graphs
		 * TODO: this does not include the correct factor of 1/2, see eqns.cpp where it is included
		 */
		void printNeq2DecompIDS(const std::string& head_, const labelledGraph& g_,
								std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printNeq2DecompIDMMAS(const labelledGraph& g_, const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printNeq2DecompIDS,*this,head_,g_,
						  std::placeholders::_1));
		}
#endif		

		void printReversalSyms(const std::string& head_,
							   std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printReversalSymsMMA(const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printReversalSyms,*this,head_,
						  std::placeholders::_1));
		}
#endif		

		
		void printRevAllQuarks(const std::string& head_,
							   std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printRevAllQuarksMMA(const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printRevAllQuarks,*this,head_,
						  std::placeholders::_1));
		}
#endif		

		/**
		 * prints decomposition of the given undirected graphs in terms of directed graphs
		 * inside *this
		 * *this needs to contain all of these graphs!
		 * (i.e. *this needs to have been constructed with correct couplings)
		 */
		void printUndirToDir(const std::string& head_, const labelledGraphs& gs_,
							 const std::vector<coupling>& couplings_,
							 std::ostream& ostream_ = std::cout) const;
#ifdef GGRAPH_USE_MMA		
		char* printUndirToDirMMA(const labelledGraphs& gs_, const std::vector<coupling>& couplings_,
								 const std::string& head_) const {
			return printCallable(
				std::bind(&labelledGraphs::printUndirToDir,*this,head_,gs_,couplings_,
						  std::placeholders::_1));
		}
#endif		

		/**
		 * produces a map from canonicalized topological graphs to a vector which
		 * contains pairs of {iterator, map} pointing to each graph with this
		 * topology and a map from that graph to the canonicalized topological graph
		 * (internally we only canonicalize the underlying graph without labels
		 * and then make a labelledGraph out if it again)
		 */
		std::map<labelledGraph, std::vector<std::pair<const_iterator, graphMap> > >
		sortByTopo() const;

		/**
		 * prints a list of all topologies together with all the corresponding
		 * numerators of this graph set mapped onte the topology
		 */
		void printSortByTopo(const std::string& head_, std::ostream& ostream_ = std::cout) const;

		void printColorFactors(std::ostream& ostream_ = std::cout) const;

#ifdef GGRAPH_USE_MMA		
		char* printColorFactorsMMA() const {
			return printCallable(
				std::bind(&labelledGraphs::printColorFactors,*this,std::placeholders::_1));
		}
#endif

		/**
		 * puts explicit external m_numbers to exts
		 * in all permutations, then canonicalizes again and obtains a new graph set
		 * (assumes that graphs are canonicalized with externals first)
		 */
		void diffExts();
	};
}

#endif /*GKA_GRAPHLABELLING_HPP*/
