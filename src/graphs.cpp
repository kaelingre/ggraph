#include "graphs.hpp"

#include <map>
#include <cassert>
#include <omp.h>

#include "couplingsRunner.hpp"
#include "utility/permItRunner.hpp"
#include "canonGraphRunner.hpp"

namespace ggraph {
	std::ostream& operator<<(std::ostream& ostream_, const ext_t& type_) {
		switch(type_) {
		case ext_t::GLUON:
			return ostream_ << "\"gluon\"";
		case ext_t::QUARK:
			return ostream_ << "\"quark\"";
		case ext_t::ANTIQUARK:
			return ostream_ << "\"antiquark\"";
		case ext_t::SCALAR:
			return ostream_ << "\"scalar\"";
		}
		return ostream_;
	}

	ext_t stringToExtT(const std::string& str_) {
		if (str_ == "gluon") {
			return ext_t::GLUON;
		}
		if (str_ == "quark") {
			return ext_t::QUARK;
		}
		if (str_ == "antiquark") {
			return ext_t::ANTIQUARK;
		}
		if (str_ == "scalar") {
			return ext_t::SCALAR;
		}
		assert(false);
		return ext_t::GLUON;
	}

	line_t extToLine(const ext_t& t_) {
		switch (t_) {
		case ext_t::GLUON:
			return line_t::GLUON;
		case ext_t::QUARK:
		case ext_t::ANTIQUARK:
			return line_t::QUARK;
		case ext_t::SCALAR:
			return line_t::SCALAR;
		}
		assert(false);
		return line_t::GLUON;
	}

	std::pair<line_t, bool> extToLineDir(const ext_t& p_) {
		switch (p_) {
		case ext_t::GLUON:
			return std::make_pair(line_t::GLUON, true);
		case ext_t::QUARK:
			return std::make_pair(line_t::QUARK, true);
		case ext_t::ANTIQUARK:
			return std::make_pair(line_t::QUARK, false);
		case ext_t::SCALAR:
			return std::make_pair(line_t::SCALAR, true);
		}
		assert(false);
		return std::make_pair(line_t::GLUON, true);
	}

	ext_t lineDirToExt(const line_t& t_, const bool& dir_) {
		switch (t_) {
		case line_t::GLUON:
			return ext_t::GLUON;
		case line_t::QUARK:
			if (dir_) {
				return ext_t::QUARK;
			}
			return ext_t::ANTIQUARK;
		case line_t::SCALAR:
			return ext_t::SCALAR;
		}
		assert(false);
		return ext_t::GLUON;
	}

	ext_t antiParticle(const ext_t& ext_) {
		switch (ext_) {
		case ext_t::QUARK:
			return ext_t::ANTIQUARK;
		case ext_t::ANTIQUARK:
			return ext_t::QUARK;
		default:
			return ext_;
		}
		assert(false);
		return ext_t::GLUON;
	}

	coupling::coupling(const std::vector<flExt>& exts_, const int& number_)
		: m_number(number_) {
		for (auto& e : exts_) {
			auto it = m_particles.find(e);
			if (it == m_particles.end()) {
				m_particles.insert(std::make_pair(e, 1));
			} else {
				++(it->second);
			}
		}
	}

	std::size_t coupling::size() const {
		std::size_t ret = 0;
		for (auto& p : m_particles) {
			ret += p.second;
		}
		return ret;
	}

	int compare(const coupling& left_, const coupling& right_) {
		//we need to first compare absolute parton numbers
		//(needed for ordering in graphs constructor)
		auto lSize = left_.size();
		auto rSize = right_.size();
		if (lSize < rSize) {
			return -1;
		} else if (rSize < lSize) {
			return 1;
		}
		
		if (left_.m_particles.size() < right_.m_particles.size()) {
			return -1;
		} else if (right_.m_particles.size() < left_.m_particles.size()) {
			return 1;
		}

		for (auto itL = left_.m_particles.begin(), itR = right_.m_particles.begin(),
				 endL = left_.m_particles.end(); itL != endL; ++itL, ++itR) {
			if (*itL < *itR) {
				return -1;
			} else if (*itR < *itL) {
				return 1;
			}
		}

		//ordering of m_number
		if (left_.m_number < right_.m_number) {
			return -1;
		} else if (left_.m_number > right_.m_number) {
			return 1;
		}

		return 0;
	}

	bool operator==(const coupling& left_, const coupling& right_) {
		if (left_.m_particles.size() != right_.m_particles.size()) {
			return false;
		}

		for (auto itL = left_.m_particles.begin(), itR = right_.m_particles.begin(),
				 endL = left_.m_particles.end(); itL != endL; ++itL, ++itR) {
			
			if (*itL != *itR) {
				return false;
			}
		}

		return true;
	}

	const coupling coupling::THREEGLUON(std::map<flExt, unsigned int>({
				{std::make_pair(std::make_pair(ext_t::GLUON, -1),3)}}));

	const coupling coupling::QUARKGLUON(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::GLUON, -1),1),
					std::make_pair(std::make_pair(ext_t::QUARK,0),1),
					std::make_pair(std::make_pair(ext_t::ANTIQUARK,0),1)
				}}));

	const coupling coupling::QUARKGLUON1(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::GLUON, -1),1),
					std::make_pair(std::make_pair(ext_t::QUARK,1),1),
					std::make_pair(std::make_pair(ext_t::ANTIQUARK,1),1)
				}}));

	const coupling coupling::QUARKGLUON2(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::GLUON, -1),1),
					std::make_pair(std::make_pair(ext_t::QUARK,2),1),
					std::make_pair(std::make_pair(ext_t::ANTIQUARK,2),1)
				}}));

	const coupling coupling::SCALARGLUON(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::GLUON, -1),1),
					std::make_pair(std::make_pair(ext_t::SCALAR,0),2)
				}}));

	const coupling coupling::SCALARGLUON1(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::GLUON, -1),1),
					std::make_pair(std::make_pair(ext_t::SCALAR,1),2)
				}}));

	const coupling coupling::SCALARGLUON2(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::GLUON, -1),1),
					std::make_pair(std::make_pair(ext_t::SCALAR,2),2)
				}}));

	const coupling coupling::THREEQUARK(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::QUARK, 0),1),
					std::make_pair(std::make_pair(ext_t::QUARK, 1),1),
					std::make_pair(std::make_pair(ext_t::QUARK, 2),1)
				}}));

	const coupling coupling::THREEAQUARK(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::ANTIQUARK, 0),1),
					std::make_pair(std::make_pair(ext_t::ANTIQUARK, 1),1),
					std::make_pair(std::make_pair(ext_t::ANTIQUARK, 2),1)
				}}));

	const coupling coupling::THREESCALAR(std::map<flExt, unsigned int>({{
					std::make_pair(std::make_pair(ext_t::SCALAR, 0),1),
					std::make_pair(std::make_pair(ext_t::SCALAR, 1),1),
					std::make_pair(std::make_pair(ext_t::SCALAR, 2),1)
				}}));
					
	graphs::graphs(const std::vector<flExt>& exts_, const unsigned int& nLoops_,
				   std::vector<coupling> couplings_, const std::size_t& exactNV_) {
		//sort the couplings by size (important for couplings runner!!!)
		std::sort(couplings_.begin(), couplings_.end());
		//number of externals
		const std::size_t nExts = exts_.size();
		
		couplingsRunner cR(couplings_, nLoops_, nExts, exactNV_);
		if (!cR.isRunning()) {
			return;
		}

		#pragma omp parallel
		{
			#pragma omp single
			do {
				auto tuplePtr = new std::vector<std::size_t>(cR.getCurrent());
				#pragma omp task
				{
					//make all starting unfinishedGraphs
					std::set<unfinishedGraph> uGs = unfinishedGraph::makeUnfinishedGraphs(
						exts_, couplings_, *tuplePtr);

					//connect one after the other until there are no free couplings left
					//(int+ext) nEdges = L + V - 1
					std::size_t nEdges = nLoops_;
					for (auto& v : *tuplePtr){
						nEdges += v;
					}
					assert(nEdges > 0);
					--nEdges;//-1 needs to be here because of conversions and overflow
					for (std::size_t i = 0; i != nEdges; ++i) {
						std::set<unfinishedGraph> newUGs;
						for (const auto& g : uGs) {
							auto tmp = g.glueNext();
							for (auto& newG : tmp) {
								if (!newG.hasDisconnectedComponent()) {
									newUGs.insert(std::move(newG));
								}
							}
						}
						uGs = std::move(newUGs);
					}
					#pragma omp critical
					for (auto& g : uGs) {
						assert(g.m_partGraphs.size() == 1 && g.m_partGraphs.begin()->second == 1);
						insert(std::move(g.m_partGraphs.begin()->first.m_graph));
					}
					//delete the tuplePtr
					delete tuplePtr;
				}
			} while (cR.next());
		}
	}

	graphs graphs::makePMEFTGraphs(std::size_t order_, const std::size_t& nBH_, const bool& higherWLCoupling_) {
		//make starting graphs, i.e. just a vertex for all BHs
		graphs gs;
		for (std::size_t i = 1; i <= nBH_; ++i) {
			graph g;
			g.addVertex(graph::vert_t(i));
			gs.insert(g);
		}

		if (order_ == 0) {
			return gs;
		}

		//idea: first construct all tree graphs with gluon lines
		//      in a second step, replace the gluon lines by directed lines starting from the "lowest" BH
		while (order_-- > 0) {
			std::set<graph> gsOld = std::move(gs);
			gs.clear();

			for (const auto& g : gsOld) {
				for (std::size_t i = 1; i <= nBH_; ++i) {
					auto gsTmp = g.addPMEFTBH(i, higherWLCoupling_);
					gs.insert(gsTmp.begin(), gsTmp.end());
				}
			}
		}

		return gs;
	}

	graphs graphs::makePMEFTGraphsInIn(std::size_t order_, const std::size_t& nBH_, const bool& higherWLCoupling_) {
		//make starting graphs, i.e. just a vertex for BH1
		graphs gs;
		graph g;
		g.addVertex(graph::vert_t(1));
		gs.insert(g);
		
		if (order_ == 0) {
			return gs;
		}

		//add line by line
		while (order_-- > 0) {
			std::set<graph> gsOld = std::move(gs);
			gs.clear();

			for (const auto& g : gsOld) {
				for (std::size_t i = 1; i <= nBH_; ++i) {
					auto gsTmp = g.addPMEFTGrav(i, higherWLCoupling_);
					gs.insert(gsTmp.begin(), gsTmp.end());
				}
			}
		}

		return gs;
	}

	graphs graphs::makePMEFTRadGraphs(std::size_t order_, const std::size_t& nBH_,
									  const bool& higherWLCoupling_) {
		assert(order_ > 0);
		auto gs = makePMEFTGraphsInIn(order_ - 1, nBH_, higherWLCoupling_);
		//add a single external graviton
		std::set<graph> gsOld = std::move(gs);
		gs.clear();

		for (const auto& g : gsOld) {
			auto gsTmp = g.addPMEFTGrav(nBH_+1, higherWLCoupling_);
			gs.insert(gsTmp.begin(), gsTmp.end());
		}

		return gs;
	}
	
	graphs graphs::makeGRWLImpulseGraphs(std::size_t orderG_) {
		graphs gs;
		if (orderG_ == 0) {
			return gs;
		}
		//make starting graph
		graph g;

	    auto vOut = g.addVertex(graph::vert_t(3));
		auto v1 = g.addVertex(graph::vert_t(1));
		auto v2 = g.addVertex(graph::vert_t(2));
		auto e1 = g.addEdge(graph::edge_t(flavor_t(0), line_t::QUARK), v1, vOut);
		auto e2 = g.addEdge(graph::edge_t(flavor_t(-1), line_t::GLUON), v2, v1);

		g.canonicalize();

		gs.insert(std::move(g));

		while (--orderG_ > 0) {
			std::set<graph> gsOld = std::move(gs);
			gs.clear();

			for (const auto& g : gsOld) {
				auto gsTmp = g.addGRWLGraviton();
				gs.insert(gsTmp.begin(), gsTmp.end());

				gsTmp = g.addGRWLTree();
				gs.insert(gsTmp.begin(), gsTmp.end());
			}
		}

		return gs;
	}

	graphs graphs::makeGRWLSGraphs(std::size_t orderG_) {
		graphs gs;
		if (orderG_ == 0) {
			return gs;
		}
		//make starting graph
		graph g;

		auto v1 = g.addVertex(graph::vert_t(1));
		auto v2 = g.addVertex(graph::vert_t(2));
		auto e2 = g.addEdge(graph::edge_t(flavor_t(-1), line_t::GLUON), v2, v1);

		g.canonicalize();

		gs.insert(std::move(g));

		while (--orderG_ > 0) {
			std::set<graph> gsOld = std::move(gs);
			gs.clear();

			for (const auto& g : gsOld) {
				auto gsTmp = g.addGRWLGraviton();
				gs.insert(gsTmp.begin(), gsTmp.end());

				gsTmp = g.addGRWLTreeUndir();
				gs.insert(gsTmp.begin(), gsTmp.end());
			}
		}

		return gs;
	}

	const graph& graphs::operator[](const std::size_t& n_) const {
		assert(n_ <= size());
		auto it = begin();
		std::advance(it, n_);
		return *it;
	}

	std::ostream& operator<<(std::ostream& ostream_, const graphs& graphs_) {
		ostream_ << '{';
		bool first = true;
		for (const auto& g : graphs_) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			ostream_ << g;
		}
		ostream_ << '}';
		return ostream_;
	}

	void graphs::printVerbose(std::ostream& ostream_) const {
		ostream_ << '{';
		bool first = true;
		for (const auto& g : *this) {
			if (first) {
				first = false;
			} else {
				ostream_ << ',';
			}
			g.printVerbose(ostream_);
		}
		ostream_ << '}';
	}

	void graphs::diffExts() {
		std::set<graph> old = std::move(*this);
		clear();

		for (auto g : old) {
			permItRunner r(std::make_pair(g.vBegin(), g.extEnd()));
			do {
				//set new external numbers (and also reset ID's,
				//since they need to correspond to position in vertex list)
				int counter = 0;
				for (auto it = g.vBegin(), end = g.extEnd(); it != end; ++it) {
					(*it)->m_ID = counter;
					(*it)->m_number = ++counter;
				}
				//copy and canonicalize
				auto gg = g;
				gg.canonicalize();
				insert(std::move(gg));
			} while (r.next());
		}
	}

	graphs::partialGraph::partialGraph(const coupling& coupling_) {
		graph::vert v = m_graph.addVertex(graph::vert_t(coupling_.m_number));
		m_map.insert(std::make_pair(v->m_ID, coupling_));
	}

	bool operator==(const graphs::partialGraph& left_, const graphs::partialGraph& right_) {
		//compare graphs
		if (left_.m_graph != right_.m_graph) {
			return false;
		}

		//compare maps
		if (left_.m_map.size() != right_.m_map.size()) {
			return false;
		}

		for (auto itL = left_.m_map.begin(), itR = right_.m_map.begin(), endL = left_.m_map.end();
			 itL != endL; ++itL, ++itR) {
			if (itL->first != itR->first) {
				return false;
			}

			if (itL->second != itR->second) {
				return false;
			}
		}

		return true;
	}

	bool operator<(const graphs::partialGraph& left_, const graphs::partialGraph& right_) {
		//compare graphs
		auto comp = compare(left_.m_graph, right_.m_graph);
		if (comp < 0) {
			return true;
		} else if (comp > 0) {
			return false;
		}

		//compare maps
		if (left_.m_map.size() < right_.m_map.size()) {
			return true;
		} else if (right_.m_map.size() < left_.m_map.size()) {
			return false;
		}

		for (auto itL = left_.m_map.begin(), itR = right_.m_map.begin(), endL = left_.m_map.end();
			 itL != endL; ++itL, ++itR) {
			if (itL->first < itR->first) {
				return true;
			} else if (itR->first < itL->first) {
				return false;
			}

			auto comp = compare(itL->second, itR->second);
			if (comp < 0) {
				return true;
			} else if (comp > 0) {
				return false;
			}
		}

		return false;
	}

	int compare(const graphs::partialGraph& left_, const graphs::partialGraph& right_) {
		//compare graphs
		auto comp = compare(left_.m_graph, right_.m_graph);
		if (comp < 0) {
			return -1;
		} else if (comp > 0) {
			return 1;
		}

		//compare maps
		if (left_.m_map.size() < right_.m_map.size()) {
			return -1;
		} else if (right_.m_map.size() < left_.m_map.size()) {
			return 1;
		}

		for (auto itL = left_.m_map.begin(), itR = right_.m_map.begin(), endL = left_.m_map.end();
			 itL != endL; ++itL, ++itR) {
			if (itL->first < itR->first) {
				return -1;
			} else if (itR->first < itL->first) {
				return 1;
			}

			auto comp = compare(itL->second, itR->second);
			if (comp < 0) {
				return -1;
			} else if (comp > 0) {
				return 1;
			}
		}

		return 0;
	}

	void graphs::partialGraph::join(partialGraph&& other_) {
		auto vShift = m_graph.m_verts.size();
		m_graph.join(std::move(other_.m_graph));
		for (auto& e : other_.m_map) {
			m_map.insert(std::make_pair(e.first + vShift, std::move(e.second)));
		}
	}

	void graphs::partialGraph::canonicalize() {
		auto perm = m_graph.canonicalize();

		std::map<std::size_t, coupling> newMap;
		for (auto& e: m_map) {
			newMap.insert(std::make_pair(perm[e.first], std::move(e.second)));
		}
		m_map = std::move(newMap);
	}

	graphs::unfinishedGraph::unfinishedGraph(const std::vector<coupling>& couplings_,
											 const std::vector<std::size_t>& multiplicities_) {
		assert(couplings_.size() == multiplicities_.size());
		auto it2 = multiplicities_.begin();
		for (auto it1 = couplings_.begin(), end = couplings_.end();
			 it1 != end; ++it1, ++it2) {
			if (*it2 > 0) {
				m_partGraphs.insert(std::make_pair(partialGraph(*it1), *it2));
			}
		}
	}

	std::set<graphs::unfinishedGraph> graphs::unfinishedGraph::makeUnfinishedGraphs(
		const std::vector<flExt>& exts_, const std::vector<coupling>& couplings_,
		const std::vector<std::size_t>& multiplicities_) {
		//idea: add one ext after the other as a coupling and use glueNext() which should exactly
		//glue this external (it should be canonically ordered first)
		std::set<unfinishedGraph> ret;
		ret.insert(unfinishedGraph(couplings_, multiplicities_));

		for (auto e : exts_) {
			//make the "external" coupling (1pt vertex)
			//we need to swap the particle to antiparticle
			e.first = antiParticle(e.first);
			coupling c(1);
			c.m_particles.insert(std::make_pair(e, 1));
			
			//connect it
			partialGraph pG(c);
			std::set<unfinishedGraph> newRet;
			for (auto g : ret) {
				g.m_partGraphs.insert(std::make_pair(pG,1));
				auto tmp = g.glueNext();
				for (auto& newG : tmp) {
					if (!newG.hasDisconnectedComponent()) {
						newRet.insert(std::move(newG));
					}
				}
			}
			ret = std::move(newRet);
		}

		return ret;
	}

	std::vector<graphs::unfinishedGraph> graphs::unfinishedGraph::glueNext() const {
		assert(!m_partGraphs.empty());

		//get first unconnected line (iterators)
		auto partialGraph1 = m_partGraphs.begin();//first partial graph
		auto coupling1 = partialGraph1->first.m_map.begin();//first (vertex,coupling) pair
		auto particle1 = coupling1->second.m_particles.begin();//first particle
		auto aParticle = antiParticle(particle1->first.first);

		std::vector<unfinishedGraph> ret;
		//find all anti particles
		std::size_t partialGraphCounter = 0;
		for (auto partialGraph2 = m_partGraphs.begin(); partialGraph2 != m_partGraphs.end();
			 ++partialGraph2, ++partialGraphCounter) {
			std::size_t couplingCounter = 0;
			for (auto coupling2 = partialGraph2->first.m_map.begin();
				 coupling2 != partialGraph2->first.m_map.end(); ++coupling2, ++couplingCounter) {
				std::size_t particleCounter = 0;
				for (auto particle2 = coupling2->second.m_particles.begin();
					 particle2 != coupling2->second.m_particles.end();
					 ++particle2, ++particleCounter) {
					//check if antiparticle (and same flavor)
					if (particle1->first.second == particle2->first.second
						&& aParticle == particle2->first.first) {
						//found an antiparticle
						if (partialGraph1 == partialGraph2) {
							if (partialGraph1->second > 1) {
								ret.push_back(glueThis(partialGraphCounter, couplingCounter,
													   particleCounter));
							}

							if (coupling1 == coupling2) {
								if (particle1 == particle2) {
									if (particle1->second > 1) {
										ret.push_back(glueThis(particleCounter));
									}
								} else {
									ret.push_back(glueThis(particleCounter));
								}
							} else {
								ret.push_back(glueThis(couplingCounter, particleCounter));
							}
						} else {
							ret.push_back(glueThis(partialGraphCounter, couplingCounter,
												   particleCounter));
						}
					}
				}
			}
		}
		return ret;
	}

	graphs::unfinishedGraph graphs::unfinishedGraph::glueThis(
		const std::size_t& partialGraphCounter2_, const std::size_t& couplingCounter2_,
		const std::size_t& particleCounter2_) const {
		//make a copy
		auto uG = *this;
		//make a copy of the partial graphs and remove them from uG
		auto partialGraphIt = uG.m_partGraphs.begin();
		std::advance(partialGraphIt,  partialGraphCounter2_);
		auto partialGraph2 = partialGraphIt->first;
		if (--(partialGraphIt->second) == 0) {
			uG.m_partGraphs.erase(partialGraphIt);
		}
		partialGraphIt = uG.m_partGraphs.begin();
		auto partialGraph1 = partialGraphIt->first;
		if (--(partialGraphIt->second) == 0) {
			uG.m_partGraphs.erase(partialGraphIt);
		}

		//get vertex IDs and particle type&flavor and remove them from maps
		auto couplingIt = partialGraph2.m_map.begin();
		std::advance(couplingIt, couplingCounter2_);
		auto v2ID = couplingIt->first + partialGraph1.m_graph.m_verts.size();;
		auto particleIt = couplingIt->second.m_particles.begin();
		std::advance(particleIt, particleCounter2_);
		//remove the coupling if no more particles inside
		if (--(particleIt->second) == 0) {
			couplingIt->second.m_particles.erase(particleIt);
		}
		//remove the vertex if the coupling is empty
		if (couplingIt->second.m_particles.empty()) {
			partialGraph2.m_map.erase(couplingIt);
		}

		couplingIt = partialGraph1.m_map.begin();
		auto v1ID = couplingIt->first;
		particleIt = couplingIt->second.m_particles.begin();
		auto particle = particleIt->first;
		//remove the coupling if no more particles inside
			if (--(particleIt->second) == 0) {
			couplingIt->second.m_particles.erase(particleIt);
		}
		//remove the vertex if the coupling is empty
		if (couplingIt->second.m_particles.empty()) {
			partialGraph1.m_map.erase(couplingIt);
		}

		//join the partial graphs
		partialGraph1.join(std::move(partialGraph2));
		graph::vert v1 = partialGraph1.m_graph.m_verts[v1ID].get();
		graph::vert v2 = partialGraph1.m_graph.m_verts[v2ID].get();

		//make the new edge
		auto line = extToLineDir(particle.first);
		if (line.second) {
			partialGraph1.m_graph.addEdge(graph::edge_t(particle.second, line.first), v1, v2);
		} else {
			partialGraph1.m_graph.addEdge(graph::edge_t(particle.second, line.first), v2, v1);
		}

		//canonicalize the new partial graph
		partialGraph1.canonicalize();

		//put it back into the unfinished graph
		auto itBool = uG.m_partGraphs.insert(std::make_pair(partialGraph1,1));
		if (!itBool.second) {
			++itBool.first->second;
		}

		return uG;
	}

	graphs::unfinishedGraph graphs::unfinishedGraph::glueThis(
		const std::size_t& couplingCounter2_, const std::size_t& particleCounter2_) const {
		//make a copy
		auto uG = *this;

		//make a copy of the partial graph and remove it from uG
		auto partialGraphIt = uG.m_partGraphs.begin();
		auto partialGraph1 = partialGraphIt->first;
		if (--(partialGraphIt->second) == 0) {
			uG.m_partGraphs.erase(partialGraphIt);
		}

		//get vertex IDs and particle type&flavor and remove them from maps
		auto couplingIt = partialGraph1.m_map.begin();
		std::advance(couplingIt, couplingCounter2_);
		auto v2ID = couplingIt->first;
		auto particleIt = couplingIt->second.m_particles.begin();
		std::advance(particleIt, particleCounter2_);
		//remove the coupling if no more particles inside
		if (--(particleIt->second) == 0) {
			couplingIt->second.m_particles.erase(particleIt);
		}
		//remove the vertex if the coupling is empty
		if (couplingIt->second.m_particles.empty()) {
			partialGraph1.m_map.erase(couplingIt);
		}

		couplingIt = partialGraph1.m_map.begin();
		auto v1ID = couplingIt->first;
		particleIt = couplingIt->second.m_particles.begin();
		auto particle = particleIt->first;
		//remove the coupling if no more particles inside
			if (--(particleIt->second) == 0) {
			couplingIt->second.m_particles.erase(particleIt);
		}
		//remove the vertex if the coupling is empty
		if (couplingIt->second.m_particles.empty()) {
			partialGraph1.m_map.erase(couplingIt);
		}

		//get the vertices
		graph::vert v1 = partialGraph1.m_graph.m_verts[v1ID].get();
		graph::vert v2 = partialGraph1.m_graph.m_verts[v2ID].get();

		//make the new edge
		auto line = extToLineDir(particle.first);
		if (line.second) {
			partialGraph1.m_graph.addEdge(graph::edge_t(particle.second, line.first), v1, v2);
		} else {
			partialGraph1.m_graph.addEdge(graph::edge_t(particle.second, line.first), v2, v1);
		}

		//canonicalize the new partial graph
		partialGraph1.canonicalize();

		//put it back into the unfinished graph
		auto itBool = uG.m_partGraphs.insert(std::make_pair(partialGraph1,1));
		if (!itBool.second) {
			++itBool.first->second;
		}

		return uG;
	}

	graphs::unfinishedGraph graphs::unfinishedGraph::glueThis(
		const std::size_t& particleCounter2_) const {
		//make a copy
		auto uG = *this;

		//make a copy of the partial graph and remove it from uG
		auto partialGraphIt = uG.m_partGraphs.begin();
		auto partialGraph1 = partialGraphIt->first;
		if (--(partialGraphIt->second) == 0) {
			uG.m_partGraphs.erase(partialGraphIt);
		}

		//get vertex ID and particle type&flavor and remove them from maps
		auto couplingIt = partialGraph1.m_map.begin();
		auto vID = couplingIt->first;
		auto particleIt = couplingIt->second.m_particles.begin();
		std::advance(particleIt, particleCounter2_);
		//remove the coupling if no more particles inside
		if (--(particleIt->second) == 0) {
			couplingIt->second.m_particles.erase(particleIt);
		}

		particleIt = couplingIt->second.m_particles.begin();
		auto particle = particleIt->first;
		//remove the coupling if no more particles inside
			if (--(particleIt->second) == 0) {
			couplingIt->second.m_particles.erase(particleIt);
		}
		//remove the vertex if the coupling is empty
		if (couplingIt->second.m_particles.empty()) {
			partialGraph1.m_map.erase(couplingIt);
		}

		//get the vertex
		graph::vert v = partialGraph1.m_graph.m_verts[vID].get();

		//make the new edge
		auto line = extToLineDir(particle.first);
		partialGraph1.m_graph.addEdge(graph::edge_t(particle.second, line.first), v, v);

		//canonicalize the new partial graph
		partialGraph1.canonicalize();

		//put it back into the unfinished graph
		auto itBool = uG.m_partGraphs.insert(std::make_pair(partialGraph1,1));
		if (!itBool.second) {
			++itBool.first->second;
		}

		return uG;
	}

	bool graphs::unfinishedGraph::hasDisconnectedComponent() const {
		if (m_partGraphs.size() == 1 && m_partGraphs.begin()->second == 1) {
			return false;
		}

		for (const auto& partGraph : m_partGraphs) {
			if (partGraph.first.m_map.size() == 0) {
				return true;
			}
		}

		return false;
	}

	void graphs::unfinishedGraph::printVerbose(std::ostream& ostream_) const {
		for (const auto& pG : m_partGraphs) {
			ostream_ << "  " << pG.second << "x: ";
			pG.first.m_graph.printVerbose(ostream_);
			ostream_ << std::endl;
		}
	}

	bool operator==(const graphs::unfinishedGraph& left_,
					const graphs::unfinishedGraph& right_) {
		if (left_.m_partGraphs.size() != right_.m_partGraphs.size()) {
			return false;
		}

		for (auto itL = left_.m_partGraphs.begin(), itR = right_.m_partGraphs.begin(),
				 end = left_.m_partGraphs.end(); itL != end; ++itL, ++itR) {
			if (*itL != *itR) {
				return false;
			}
		}
		return true;
	}

	bool operator<(const graphs::unfinishedGraph& left_,
				   const graphs::unfinishedGraph& right_) {
		if (left_.m_partGraphs.size() < right_.m_partGraphs.size()) {
			return true;
		} else if (right_.m_partGraphs.size() < left_.m_partGraphs.size()) {
			return false;
		}

		for (auto itL = left_.m_partGraphs.begin(), itR = right_.m_partGraphs.begin(),
				 end = left_.m_partGraphs.end(); itL != end; ++itL, ++itR) {
			auto comp = compare(itL->first, itR->first);
			if (comp < 0) {
				return true;
			} else if (comp > 0) {
				return false;
			}

			if (itL->second < itR->second) {
				return true;
			} else if (itR->second < itL->second) {
				return false;
			}
		}

		return false;
	}
}	
