# Cloning the repo

Since there is a submodule inside the repo you have to 
* either, after you have have already cloned the repo, call
```bash
git submodule update --init --recursive
```
* or clone it recursively from the start with
```bash
git clone --recursive git@gitlab.com:kaelingre/bcjgraphs.git
```

# Prerequisites
* C++-17 compatible compiler (e.g. gcc-6 and higher)
* meson & ninja [https://mesonbuild.com/] >= 0.54.0
* boost >= 1.64
  * On OSX when using homebrew, checkout [https://formulae.brew.sh/formula/boost-python] to properly build it (i.e. use `-DBoost_NO_BOOST_CMAKE=ON`)
* graph_canon: download and install it using the instructions on [https://github.com/jakobandersen/graph_canon]
* Optionally: Mathematica 10+ (maybe even 9, not checked though)
* Optionally: openmp

# Compilation and installation

The commands below will call Mathematica in the background (if Mathematica is activated). Make sure that a license is available while this command is running. Also, check the output if Mathematica is actually found. If the math/MathKernel command is not working, it will compile without the Mathematica interface.

If graph_canon is install in a non-default directory, one has to add its install location via
```bash
export CMAKE_PREFIX_PATH=</path/to/graph_canon/install/dir>
```

The command
```bash
meson setup <build-dir> <options>
meson compile -C <build-dir>
```
will configure the build system and compile all the code inside the chosen `<build-dir>` directory.
For meson versions < 0.54.0 the 2nd command doesn't work and one needs to do the following:
```bash
cd <build-dir> && ninja
```
Supported options are:
* `-Dprefix` sets a custom installation directory (default: `-Dprefix=/usr/local/`)
* `-Dmma` allows to (de-)activate the Mathematica interface (default: `-Dmma=true`)

To install the library (in the folder specified by the `-Dprefix` option) use
```bash
cd build && (sudo) meson install
```
where sudo needs to be used in case administrator privileges are needed to write to the specified directory.

If you want to use the Mathematica interface you will also need to copy or symlink the gGraph folder to `$UserBaseDir<>"/Applications` (just run this command in Mathematica), e.g. something like
```bash
ln -s /path/to/gGraph/ ~/.Mathematica/Applications/gGraph
```

# Compilation with Mathematica notebook (OLD)

Simply open the file gGraph/compilation.m (using the GUI) and run the stuff inside to compile and install the package. This file has not been updated for a while, so I can't guarantee that it works. (Also, the library will only be install for the use in Mathematica and not systemwide)
