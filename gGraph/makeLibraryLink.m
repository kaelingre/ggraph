(* ::Package:: *)

Get[NotebookDirectory[]<>"cppLibraryLink.m"];
Get[NotebookDirectory[]<>"source.m"];


Export[NotebookDirectory[]<>"../src/libraryLink.hpp",generateHpp[classes,functions,includes],"String"];
Export[NotebookDirectory[]<>"../src/libraryLink.cpp",generateCpp[classes,functions,includes],"String"];


(* ::Section:: *)
(*Test*)


Quit[]


Needs["gGraph`"]


gluon=constructFlExt["GLUON",-1];
gs=constructLabelledGraphs[{gluon,gluon,gluon,gluon},2,{THREEGLUON,QUARKGLUON(*,QUARKGLUON1,QUARKGLUON2,THREEQUARK,THREEQUARK,THREEAQUARK*)}];//AbsoluteTiming
