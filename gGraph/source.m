(* ::Package:: *)

(* Wolfram Language package *)

enums = {
	cppEnum["ext_t",
		{"GLUON", "QUARK", "ANTIQUARK", "SCALAR"}
	]
};

classes = {
	cppClass["flExt",
		{
			cppConstructor[{"ext_t", optional[Integer,-1]}]
		},
		{}
	],
	cppClass["coupling",
		{
			cppConstructor[{{Symbol["flExt"]}, optional[Integer,0]}]
		},			
		{
			cppStaticMember["THREEGLUON",Symbol["coupling"],{"MMAFunName"->"THREEGLUON"}],
			cppStaticMember["QUARKGLUON",Symbol["coupling"],{"MMAFunName"->"QUARKGLUON"}],
			cppStaticMember["QUARKGLUON1",Symbol["coupling"],{"MMAFunName"->"QUARKGLUON1"}],
			cppStaticMember["QUARKGLUON2",Symbol["coupling"],{"MMAFunName"->"QUARKGLUON2"}],
			cppStaticMember["SCALARGLUON",Symbol["coupling"],{"MMAFunName"->"SCALARGLUON"}],
			cppStaticMember["SCALARGLUON1",Symbol["coupling"],{"MMAFunName"->"SCALARGLUON1"}],
			cppStaticMember["SCALARGLUON2",Symbol["coupling"],{"MMAFunName"->"SCALARGLUON2"}],
			cppStaticMember["THREEQUARK",Symbol["coupling"],{"MMAFunName"->"THREEQUARK"}],
			cppStaticMember["THREEAQUARK",Symbol["coupling"],{"MMAFunName"->"THREEAQUARK"}],
			cppStaticMember["THREESCALAR",Symbol["coupling"],{"MMAFunName"->"THREESCALAR"}]
		}
	],
	cppClass["graph",
		{},
		{
			cppFunction["getTopo",{},Symbol["graph"],{"MMAFunName"->"stripLineTypes"}],
			cppFunction["printSymmetryGroupMMA",{},"UTF8String",{"MMAFunName"->"printSymmetryGroup", "postFunction"->ToExpression}],
			cppFunction["countMultiEdgeTadpoleSyms",{},Integer,{"MMAFunName"->"countMultiEdgeTadpoleSyms"}],
			cppFunction["symmetryFactor",{},Integer,{"MMAFunName"->"symmetryFactor"}],
			cppFunction["containsSimpleTadpole",{},"Boolean",{"MMAFunName"->"containsSimpleTadpole"}],
			cppFunction["containsBubble",{},"Boolean",{"MMAFunName"->"containsBubble"}],
			cppFunction["containsNull",{},"Boolean",{"MMAFunName"->"containsNull"}]
		}
	],
	cppClass["graphs",
		{
			cppConstructor[{{Symbol["flExt"]}, Integer, {Symbol["coupling"]}, optional[Integer, 0]}]
		},
		{
			cppFunction["operator[]", {Integer}, Symbol["graph"], {"MMAFunName"->"get"}],
			cppFunction["size", {}, Integer, {"MMAFunName"->"size"}],
			cppFunction["diffExts", {}, "Void", {"MMAFunName"->"diffExts"}]
		}
	],
	cppClass["labelledGraph",
		{
		},
		{
			cppFunction["getGraph",{},Symbol["graph"],{"MMAFunName"->"stripMomenta"}],
			cppFunction["printPropsMMA",{},"UTF8String",{"MMAFunName"->"printProps", "postFunction"->ToExpression}],
			cppFunction["printColorFactorMMA",{},"UTF8String",{"MMAFunName"->"printColorFactor", "postFunction"->ToExpression}],
			cppFunction["printSymmetryGroupMMA",{},"UTF8String",{"MMAFunName"->"printSymmetryGroup", "postFunction"->ToExpression}],
			cppFunction["countMultiEdgeTadpoleSyms",{},Integer,{"MMAFunName"->"countMultiEdgeTadpoleSyms"}],
			cppFunction["countQuarkLoops",{},Integer,{"MMAFunName"->"countQuarkLoops"}],
			cppFunction["symmetryFactor",{},Integer,{"MMAFunName"->"symmetryFactor"}],
			cppFunction["containsSimpleTadpole",{},"Boolean",{"MMAFunName"->"containsSimpleTadpole"}],
			cppFunction["containsBubble",{},"Boolean",{"MMAFunName"->"containsBubble"}],
			cppFunction["containsNull",{},"Boolean",{"MMAFunName"->"containsNull"}]
		}
	],
	cppClass["labelledGraphs",
		{
			cppConstructor[{Symbol["graphs"], optional["Boolean", False]}],
			cppConstructor[{{Symbol["flExt"]}, Integer, {Symbol["coupling"]}, optional["Boolean", False], optional[Integer, 0]}]
		},
		{
			cppFunction["printSymsMMA",{optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printSyms", "postFunction"->ToExpression}],
			cppFunction["printJacobiesMMA",{optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printJacobies", "postFunction"->ToExpression}],
			cppFunction["printTwoTermMMA",{optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printTwoTerm", "postFunction"->ToExpression}],
			cppFunction["printTwoTermFlMMA",{optional["UTF8String","n"],optional[{Integer},{0,1,2}]},"UTF8String",{"MMAFunName"->"printTwoTermFl", "postFunction"->ToExpression}],
			cppFunction["printScalarTwoTermMMA",{optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printScalarTwoTerm", "postFunction"->ToExpression}],
			cppFunction["printScalarTwoTermFlMMA",{optional["UTF8String","n"],optional[{Integer},{0,1,2}]},"UTF8String",{"MMAFunName"->"printScalarTwoTermFl", "postFunction"->ToExpression}],
			cppFunction["printFlavorSymsMMA",{optional["UTF8String","n"],optional[{Integer},{0,1,2}],optional["Boolean",True]},"UTF8String",{"MMAFunName"->"printFlavorSyms", "postFunction"->ToExpression}],
			cppFunction["operator[]", {Integer}, Symbol["labelledGraph"], {"MMAFunName"->"get"}],
			cppFunction["size", {}, Integer, {"MMAFunName"->"size"}],
			cppFunction["printNeq4DecompIDMMA",{Symbol["labelledGraph"],optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printNeq4DecompID", "postFunction"->ToExpression}],
			cppFunction["printNeq2DecompIDMMA",{Symbol["labelledGraph"],optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printNeq2DecompID", "postFunction"->ToExpression}],
			cppFunction["printNeq4DecompIDMMAS",{Symbol["labelledGraph"],optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printNeq4DecompIDS", "postFunction"->ToExpression}],
			cppFunction["printNeq2DecompIDMMAS",{Symbol["labelledGraph"],optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printNeq2DecompIDS", "postFunction"->ToExpression}],
			cppFunction["printReversalSymsMMA",{optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printReversalSyms", "postFunction"->ToExpression}],
			cppFunction["printRevAllQuarksMMA",{optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printRevAllQuarks", "postFunction"->ToExpression}],
			cppFunction["printUndirToDirMMA",{Symbol["labelledGraphs"],{Symbol["coupling"]},optional["UTF8String","n"]},"UTF8String",{"MMAFunName"->"printUndirToDir", "postFunction"->ToExpression}],
			cppFunction["printColorFactorsMMA",{},"UTF8String",{"MMAFunName"->"printColorFactors", "postFunction"->ToExpression}],
			cppStaticFunction["makePMEFTGraphs",{Integer, optional[Integer,2], optional["Boolean",False]},Symbol["labelledGraphs"],{"MMAFunName"->"makePMEFTGraphs"}],
			cppStaticFunction["makePMEFTGraphsInIn",{Integer, optional[Integer,2], optional["Boolean",False]},Symbol["labelledGraphs"],{"MMAFunName"->"makePMEFTGraphsInIn"}],
			cppStaticFunction["makePMEFTRadGraphs",{Integer, optional[Integer,2], optional["Boolean",False]},Symbol["labelledGraphs"],{"MMAFunName"->"makePMEFTRadGraphs"}],
			cppStaticFunction["makeGRWLImpulseGraphs",{Integer},Symbol["labelledGraphs"],{"MMAFunName"->"makeGRWLImpulseGraphs"}],
			cppStaticFunction["makeGRWLSGraphs",{Integer},Symbol["labelledGraphs"],{"MMAFunName"->"makeGRWLSGraphs"}],
			cppFunction["diffExts", {}, "Void", {"MMAFunName"->"diffExts"}]
		}
	],
	cppClass["cut",
		{},
		{
			cppFunction["printNumsMMA",{Symbol["labelledGraphs"],optional["UTF8String","n"]}, "UTF8String",{"MMAFunName"->"printNums","postFunction"->ToExpression}],
			cppFunction["printNumsPropMMA",{Symbol["labelledGraphs"],optional["UTF8String","n"]}, "UTF8String",{"MMAFunName"->"printNumsProp","postFunction"->ToExpression}],
			cppFunction["printTreesMMA",{}, "UTF8String",{"MMAFunName"->"printTrees","postFunction"->ToExpression}],
			cppFunction["printPropsMMA",{},"UTF8String",{"MMAFunName"->"printProps", "postFunction"->ToExpression}],
			cppFunction["decomposeToDirected",{Symbol["spanningCuts"]},Symbol["cuts"],{"MMAFunName"->"decomposeToDirected"}],
			cppFunction["swapEdges",{Integer,Integer,"Boolean",Integer,"Boolean"},"Void",{"MMAFunName"->"swapEdges"}]
		}
	],
	cppClass["cuts",
		{},
		{
			cppFunction["operator[]", {Integer}, Symbol["cut"], {"MMAFunName"->"get"}],
			cppFunction["size", {}, Integer, {"MMAFunName"->"size"}]
		}
	],
	cppClass["spanningCuts",
		{
			cppConstructor[{{Symbol["flExt"]}, Integer, {Symbol["coupling"]}}]
		},
		{
			cppFunction["operator[]", {Integer}, Symbol["cut"], {"MMAFunName"->"get"}],
			cppFunction["size", {}, Integer, {"MMAFunName"->"size"}]
		}
	]
};

functions = {
	cppFunction["print", {Symbol["graph"]}, "UTF8String", {"postFunction"->ToExpression}],
	cppFunction["print", {Symbol["labelledGraph"]}, "UTF8String", {"postFunction"->ToExpression}],
	cppFunction["print", {Symbol["graphs"]}, "UTF8String", {"postFunction"->ToExpression}],
	cppFunction["print", {Symbol["labelledGraphs"]}, "UTF8String", {"postFunction"->ToExpression}],
	cppFunction["print", {Symbol["spanningCuts"]}, "UTF8String", {"postFunction"->ToExpression}],
	cppFunction["print", {Symbol["cut"]}, "UTF8String", {"postFunction"->ToExpression}]
};

includes = {
	"util.hpp",
	"graphLabelling.hpp",
	"cut.hpp"
};
