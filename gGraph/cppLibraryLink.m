(* ::Package:: *)

(* Wolfram Language Package *)

BeginPackage["gGraph`cppLibraryLink`"]
Unprotect@@Names["gGraph`cppLibraryLink`*"];

(* Exported symbols added here with SymbolName::usage *)  
optional::usage = "Head for an optional argument"
optional::author = "Gregor Kaelin"

(*cppClass["classname",{cppConstructor[...], ...},{cppFunction[...], cppFunction[...], ...}]*)
cppClass::usage = "Head for a c++ class";
cppClass::author = "Gregor Kaelin";

(*cppFunction["functionName",{argType:defaultArg, ...}, returnType,{Options: "MMAFunName"->"", "postFunction"->ID}]*)
cppFunction::usage = "Head for a c++ function (standalone or member)";
cppFunction::author = "Gregor Kaelin";

(*cppFunction["functionName",{argType:defaultArg, ...}, returnType,{Options: "MMAFunName"->"", "postFunction"->ID}]*)
cppStaticFunction::usage = "Head for a static c++ function";
cppStaticFunction::author = "Gregor Kaelin";

(*cppStaticMember["memberName", memberType, {Options: "mathematicaMemberName"->""}]*)
cppStaticMember::usage = "Head for a c++ class member";
cppStaticMember::author = "Gregor Kaelin";

(*cppConstructor[{argType:defaultArg, ...},{Options: "MMAFunName"->"", "postFunction"->ID}]*)
cppConstructor::usage = "Head for a c++ constructor function";
cppConstructor::author = "Gregor Kaelin";

(*cppEnum["name", {"value1", "value2", ...}]*)
cppEnum::usage = "Head for c++ enum class";
cppEnum::author = "Gregor Kaelin";

generateCpp::usage = "Generates the c++ boilerplate code for the given c++ classes and functions";
generateCpp::author = "Gregor Kaelin";

generateHpp::usage = "Generates the c++ boilerplate code for the given c++ classes and functions";
generateHpp::author = "Gregor Kaelin";

loadCpp::usage = "Defines the mathematica function loads for the the given c++ classes and functions";
loadCpp::author = "Gregor Kaelin";

defineCpp::usage = "Defines user-accessible cpp functions";
defineCpp::author = "Gregor Kaelin";

buildCppMathInterface::usage = "Defines all mathematica functions for given c++ classes and functions";
buildCppMathInterface::author = "Gregor Kaelin";



(* ::Section:: *)
(*Private*)


Begin["`Private`"] (* Begin Private Context *)

(* Define Capitalize for older versions*)
If[TrueQ[$VersionNumber < 10.1],
	Capitalize[string_String] := ToUpperCase[StringTake[string, 1]] <> StringTake[string,{2,StringLength[string]}] 
]

$stdArgs = "WolframLibraryData libData_, mint argc_, MArgument* args_, MArgument res_";


$enums::usage = "Container to store c++ enum classes";
$enums::author = "Gregor Kaelin";
$enums = {};


(*helper function*)
scanIndexed::usage = "Analogue to MapIndexed, but scans instead of maps";
scanIndexed::author = "Gregor Kaelin";
scanIndexed[function_, list_List] := Scan[function[#[[1]], {#[[2]]}]&, Transpose[{list, Range[Length[list]]}]] 


(* ::Subsection:: *)
(*Type handling*)


toCppType::usage = "Gives the corresponding type as would be used in cpp (String)"
toCppType::author = "Gregor Kaelin"
toCppType[opt_optional] := toCppType[First[opt]]
toCppType[{type_}] := "std::vector<"
toCppType[Integer] := "mint"
toCppType[Real] := "double"
toCppType["Boolean"] := "bool"
toCppType["UTF8String"] := "char*"
toCppType[arg_String] := arg (*enum*)
toCppType[arg_] := ToString[arg] (*reference to other c++ object*)

toCppMType::usage = "Gives the corresponding 'm-type' as it would be used in cpp (i.e. mint, MTensor, ...)"
toCppMType::author = "Gregor Kaelin"
toCppMType[opt_optional] := toCppMType[First[opt]]
toCppMType[{type_}] := "MTensor"
toCppMType[Integer] := "mint"
toCppMType[Real] := "double"
toCppMType["Boolean"] := "bool"
toCppMType["UTF8String"] := "char*"
toCppMType[_String] := "mint" (*enum*)
toCppMType[_] := "mint" (*reference to other c++ object*)

getType::usage = "c++ MArgument_get type function"
getType::author = "Gregor Kaelin"
getType[opt_optional] := getType[First[opt]]
getType[{type_}] := "MArgument_getMTensor"
getType[Integer] := "MArgument_getInteger"
getType[Real] := "MArgument_getReal"
getType["Boolean"] := "MArgument_getBoolean"
getType["UTF8String"] := "MArgument_getUTF8String"
getType[_String] := "MArgument_getInteger" (*enum*)
getType[_] := "MArgument_getInteger" (*reference to other c++ object*)  

setType::usage = "c++ MArgument_set type function"
setType::author = "Gregor Kaelin"
setType[opt_optional] := setType[First[opt]] 
setType[{type_}] := "MArgument_setMTensor" (*???*)
setType[Integer] := "MArgument_setInteger"
setType[Real] := "MArgument_setReal"
setType["Boolean"] := "MArgument_setBoolean"
setType["UTF8String"] := "MArgument_setUTF8String"
setType[_String] := "MArgument_setInteger" (*enum*)
setType[_] := "MArgument_setInteger" (*reference to other c++ object*)  

toMathType::usage = "Gives the corresponding type as would be used in mathematica"
toMathType::author = "Gregor Kaelin"
toMathType[opt_optional] := toMathType[First[opt]]
toMathType[{type_}] := {toMathType[type], 1}
toMathType[Integer] := Integer
toMathType[Real] := Real
toMathType["Boolean"] := "Boolean"
toMathType["UTF8String"] := "UTF8String"
toMathType[_String] := Integer (* enum *)
toMathType[_] := Integer (*reference to other c++ object*)

isRefQ::usage = "Checks if the given type is a reference to a c++ object"
isRefQ::author = "Gregor Kaelin"
isRefQ[opt_optional] := isRefQ[First[opt]]
isRefQ[{_}] := False
isRefQ[Integer] := False
isRefQ[Real] := False
isRefQ[_String] := False
isRefQ[_] := True


(* ::Subsection:: *)
(*Validate*)


validateCppClass::usage = "Check if the c++ class is in a proper format"
validateCppClass::author = "Gregor Kaelin"
validateCppClass[class_cppClass] := True(*TODO*)


validateCppFunction::usage = "Check if the c++ function is in a proper format"
validateCppFunction::author = "Gregor Kaelin"
validateCppFunction[fun_cppFunction] := True(*TODO*)


validateCpp::usage = "Check if a c++ object is in a proper format"
validateCpp::author = "Gregor Kaelin"
validateCpp[obj_cppClass] := validateCppClass[obj]
validateCpp[obj_cppFunction] := validateCppClass[obj]
validateCpp[___] := False


(* ::Subsection:: *)
(*Generate C++ code*)


cppLLfunctionName::usage = "Generates a function name to be used inside c++"
cppLLfunctionName::author = "Gregor Kaelin"
cppLLfunctionName[classname_String, functionname_String, number_Integer] := "LL_" <> classname <> Capitalize[StringReplace[functionname, {"["->"", "]"->"", "="->""}]] <> ToString[number]
cppLLfunctionName[functionname_String, number_Integer] := "LL_" <> functionname <> ToString[number]


cppMapName::usage = "Generates name of the map in c++ source code"
cppMapName::author = "Gregor Kaelin"
cppMapName[classname_String] := "LL_" <> classname <> "Map"


generateGetArg::usage = "Generate c++ code to retrieve an argument of given type and given index"
generateGetArg::author = "Gregor Kaelin"
(*generateGetArg[optional_Pattern, idx_Integer] := generateGetArg[optional[[1]], idx]*)
generateGetArg[type_, idx_Integer] := 
	"\t\t" <> toCppMType[type] <> If[Head[type] === List || If[Head[type] === optional, Head[type[[1]]] === List, False], " t", " arg"] <> ToString[idx] <> " = " <> getType[type] <> "(args_[" <> ToString[idx] <> "]);\n"


generateDisownString::usage = "Generate c++ code to disown a string argument"
generateDisownString::author = "Gregor Kaelin"
generateDisownString[opt_optional, idx_Integer] := generateDisownString[opt[[1]], idx]
generateDisownString["UTF8String", idx_Integer] := 
	"\t\tlibData_->UTF8String_disown(" <> "arg" <> ToString[idx] <> ");\n"
generateDisownString[__] := ""


generateMap::usage = "Generates c++ code for the static map variables"
generateMap::author = "Gregor Kaelin"
generateMap[classname_String] := "\tstatic std::map<mint, " <> classname <> "*> " <> cppMapName[classname] <> ";\n\n"


generateArg::usage = "Generates c++ code to produce the proper argument, including static_cast for enums and other c++ objects"
generateArg::author = "Gregor Kaelin"
generateArg[opt_optional, arg_String] := generateArg[First[opt], arg]
generateArg[{type_}, arg_String] := "std::move(" <> arg <> ")"
generateArg[type_List, arg_String] := arg
generateArg[Integer, arg_String] := arg
generateArg[Real, arg_String] := arg
generateArg[type_String, arg_String] /;(type != "Boolean" && type != "UTF8String") := "static_cast<" <> type <> ">(" <> arg <> ")"
generateArg[type_String, arg_String] := arg
generateArg[type_, arg_String] :=  "*" <> cppMapName[ToString[type]] <> "[" <> arg <> "]"


generateTensorToVector::usage = "Generates the c++ code to convers a MTensor argument to a std::vector"
generateTensorToVector::author = "Gregor Kaelin"
generateTensorToVector[optional[{type_}, _], idx_Integer] := generateTensorToVector[{type}, idx]
generateTensorToVector[{type_}, idx_Integer] := 
	"\t\tconst mint* data" <> ToString[idx] <> " = libData_->MTensor_getIntegerData(t" <> ToString[idx] <> ");\n" <>
	"\t\tconst mint length" <> ToString[idx] <> " = libData_->MTensor_getFlattenedLength(t" <> ToString[idx] <> ");\n" <>
	"\t\tstd::vector<" <> toCppType[type] <> "> arg" <> ToString[idx] <> ";\n" <>
	"\t\tfor (unsigned int i = 0; i != length" <> ToString[idx] <> "; ++i) {\n" <> 
	"\t\t\t" <>  "arg" <> ToString[idx] <> ".push_back(" <> 
	Which[
		(*actual integer*)
		type === Integer,
		"data" <> ToString[idx] <> "[i]",
		(*enum*)
		Head[type] === String,
		"static_cast<" <> type <> ">(data" <> ToString[idx] <> "[i])",
		True,
		(*ref to other ++ obj*)
		"*" <> cppMapName[ToString[type]] <> "[data" <> ToString[idx] <> "[i]]"
	] <> ");\n" <>
	"\t\t}\n\n"
generateTensorToVector[__] := ""


generateRegisterLibraryExpr::usage = " Generate c++ code to register a class in the library expression manager"
generateRegisterLibraryExpr::author = "Gregor Kaelin"
generateRegisterLibraryExpr[classname_String] := 
	StringTemplate["\t\tlibData_->registerLibraryExpressionManager(\"`classname`\", LL_`classname`ManageInstance);\n"][<| "classname" -> classname |>]


generateUnregisterLibraryExpr::usage = " Generate c++ code to unregister a class in the library expression manager"
generateUnregisterLibraryExpr::author = "Gregor Kaelin"
generateUnregisterLibraryExpr[classname_String] := 
	StringTemplate["\t\tlibData_->unregisterLibraryExpressionManager(\"`classname`\");\n"][<| "classname" -> classname |>]
	

generateManageInstanceHpp::usage = "Generates c++ code to manage the instance of a class"
generateManageInstanceHpp::author = "Gregor Kaelin"
generateManageInstanceHpp[classname_String] :=
	"\tDLLEXPORT void LL_" <> classname <> "ManageInstance(WolframLibraryData libData_, mbool mode_, mint id_);\n\n"


generateHeaderHpp::usage = "Generates c++ code for general header stuff"
generateHeaderHpp::author = "Gregor Kaelin"
generateHeaderHpp[includes_List] := "\
/**************************************************************************
 * This file has been automatically generated by the cppLibraryLink package
 * DO NOT EDIT THIS FILE BY HAND
 */
 
#ifndef LIBRARYLINK_HPP
#define LIBRARYLINK_HPP

#include <map>
#include \"WolframLibrary.h\"

" <>
	StringJoin@@Map[("#include \"" <> # <> "\"\n")&,includes] <> "
namespace ggraph {
\tEXTERN_C DLLEXPORT int WolframLibrary_getVersion() {
\t\treturn WolframLibraryVersion;
\t}
 
\tstatic char* LL_lastChar;
\tEXTERN_C DLLEXPORT int LL_deleteLastChar(" <> $stdArgs <> ");\n\n"


generateConstructorHpp::usage = "Generates c++ code for a class constructor"
generateConstructorHpp::author = "Gregor Kaelin"
generateConstructorHpp[classname_String, constructor_cppConstructor, number_Integer] := 
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[classname, "Construct", number] <> "(" <> $stdArgs <> ");\n\n"


generateFunctionHpp::usage = "Generates c++ cod for a free function"
generateFunctionHpp::author = "Gregor Kaelin"
generateFunctionHpp[function_cppFunction, number_Integer] := 
	(* comment *)
	"\t//********** " <> function[[1]] <> " **********\n" <>
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[function[[1]], number] <> "(" <> $stdArgs <> ");\n\n"


generateMemberFunctionHpp::usage = "Generates c++ code for a class member function"
generateMemberFunctionHpp::author = "Gregor Kaelin"
generateMemberFunctionHpp[classname_String, function_, number_Integer] := 
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[classname, function[[1]], number] <> "(" <> $stdArgs <> ");\n\n"


generateClassHpp::usage = "Generates c++ code for a class"
generateClassHpp::author = "Gregor Kaelin"
generateClassHpp[class_cppClass] :=
	(* comment *)
	"\t//********** " <> class[[1]] <> " **********\n" <>
	(* generate the map *) 
	generateMap[class[[1]]] <>
	(* generate the instance managers *)
	generateManageInstanceHpp[class[[1]]] <>
	(* generate the constructors *)
	StringJoin@@MapIndexed[generateConstructorHpp[class[[1]], #1, First[#2]]&, class[[2]]] <>
	(* generate the functions *)
	StringJoin@@MapIndexed[generateMemberFunctionHpp[class[[1]], #1, First[#2]]&, class[[3]]]


generateManageInstanceCpp::usage = "Generates c++ code to manage the instance of a class"
generateManageInstanceCpp::author = "Gregor Kaelin"
generateManageInstanceCpp[classname_String] :=
	StringTemplate["\
\tDLLEXPORT void LL_`classname`ManageInstance(WolframLibraryData libData_, mbool mode_, mint id_) {
\t\tif (mode_ == 0) {
\t\t\t`mapName`[id_] = nullptr;
\t\t} else {
\t\t\t`classname`* obj = `mapName`.at(id_);
\t\t\tif (obj) {
\t\t\t\tdelete obj;
\t\t\t}
\t\t\t`mapName`.erase(id_); 
\t\t}
\t}\n\n"][<|"classname" -> classname, "mapName" -> cppMapName[classname]|>]


generateHeaderCpp::usage = "Generates c++ code for general header stuff"
generateHeaderCpp::author = "Gregor Kaelin"
generateHeaderCpp[includes_List] := "\
/**************************************************************************
 * This file has been automatically generated by the cppLibraryLink package
 * DO NOT EDIT THIS FILE BY HAND
 */
 
#if HAVE_CONFIG_H
#  include <config.h>
#endif

#include \"libraryLink.hpp\"

#include <cstring>
#include <sstream>
#include <vector>

namespace ggraph {
\tEXTERN_C DLLEXPORT int LL_deleteLastChar(" <> $stdArgs <> ") {" <> "
\t\tdelete [] LL_lastChar;
\t\treturn LIBRARY_NO_ERROR;
\t}\n\n"


generateConstructorCpp::usage = "Generates c++ code for a class constructor"
generateConstructorCpp::author = "Gregor Kaelin"
generateConstructorCpp[classname_String, constructor_cppConstructor, number_Integer] := 
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[classname, "Construct", number] <> "(" <> $stdArgs <> ") {\n" <> 
	(* get the arguments *)
	StringJoin@@MapIndexed[(generateGetArg[#1, First[#2]])&, constructor[[1]]] <>
	(* transform MTensors to std::vector *)
	MapIndexed[generateTensorToVector[#1, First[#2]]&, constructor[[1]]] <>
	(* call the constructor*)
	"\t\t" <> cppMapName[classname] <> ".at(MArgument_getInteger(args_[0])) = new " <> classname <> "(" <> 
	(* put arguments *)
	StringRiffle[MapIndexed[generateArg[#1, "arg" <> ToString[First[#2]]]&, constructor[[1]]], ", "] <> ");\n" <>
	(* disown strings *)
	StringJoin@@MapIndexed[(generateDisownString[#1, First[#2]])&, constructor[[1]]] <>
	(* finish the function *)
	"\n\t\treturn LIBRARY_NO_ERROR;\n\t}\n\n"


generateFunctionCpp::usage = "Generates c++ code for a free function"
generateFunctionCpp::author = "Gregor Kaelin"
generateFunctionCpp[function_cppFunction, number_Integer] := 
	(* comment *)
	"\t//********** " <> function[[1]] <> " **********\n" <>
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[function[[1]], number] <> "(" <> $stdArgs <> ") {\n" <>
	If[!isRefQ[function[[3]]],
		(*then : return type is not a reference to other c++ obj*)
		(* get the arguments *)
		StringJoin@@MapIndexed[(generateGetArg[#1, First[#2] - 1])&,function[[2]]] <>
		(* transform MTensors to std::vector *)
		MapIndexed[generateTensorToVector[#1, First[#2] - 1]&, function[[2]]] <>
		(* call the function *)
		"\t\t" <> If[function[[3]] =!= "Void", toCppMType[function[[3]]] <> " ret = ", ""] <> function[[1]] <> "(" <>
		(* put arguments *)
		StringRiffle[MapIndexed[generateArg[#1, "arg" <> ToString[First[#2] - 1]]&, function[[2]]], ", "] <> ");\n" <> 
		(* put into res_ *)
		If[function[[3]] =!= "Void", "\t\t" <> setType[function[[3]]] <> "(res_, ret);\n", ""] <>
		(* disown strings *)
		StringJoin@@MapIndexed[(generateDisownString[#1, First[#2] - 1])&, function[[2]]] <>
		(* remember to delete char* *)
		If[function[[3]] === "UTF8String", "\t\tLL_lastChar = ret;\n", ""],
		
		(*else: return type is a ref to other c++ obj SPECIAL CASE*)
		(* get the ID of return object*)
		"\t\tmint idRet = MArgument_getInteger(args_[0]);\n" <>
		(* get the arguments *)
		StringJoin@@MapIndexed[(generateGetArg[#1, First[#2]])&,function[[2]]] <>
		(* transform MTensors to std::vector *)
		MapIndexed[generateTensorToVector[#1, First[#2]]&, function[[2]]] <>
		(* call the function *)
		"\t\t" <> cppMapName[ToString[function[[3]]]] <> ".at(idRet) = new " <> ToString[function[[3]]] <> "(" <> function[[1]] <> "(" <>
		(* put arguments *)
		StringRiffle[MapIndexed[generateArg[#1, "arg" <> ToString[First[#2]]]&, function[[2]]], ", "] <> "));\n"
	] <>
	(* finish the function *)
	"\n\t\treturn LIBRARY_NO_ERROR;\n\t}\n\n"


generateMemberFunctionCpp::usage = "Generates c++ code for a class member function"
generateMemberFunctionCpp::author = "Gregor Kaelin"
generateMemberFunctionCpp[classname_String, function_cppFunction, number_Integer] := 
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[classname, function[[1]], number] <> "(" <> $stdArgs <> ") {\n" <>
	
	If[!isRefQ[function[[3]]],
		(*then : return type is not a reference to other c++ obj*)
		(* get the ID *)
		"\t\tmint id = MArgument_getInteger(args_[0]);\n" <>
		(* get the class instance *)
		"\t\t" <> classname <> "& obj = *" <> cppMapName[classname] <> ".at(id);\n" <>
		(* get the arguments *)
		StringJoin@@MapIndexed[(generateGetArg[#1, First[#2]])&,function[[2]]] <>
		(* transform MTensors to std::vector *)
		MapIndexed[generateTensorToVector[#1, First[#2]]&, function[[2]]] <>
		(* call the function *)
		"\t\t" <> If[function[[3]] =!= "Void", toCppMType[function[[3]]] <> " ret = ", ""] <> "obj." <> function[[1]] <> "(" <>
		(* put arguments *)
		StringRiffle[MapIndexed[generateArg[#1, "arg" <> ToString[First[#2]]]&, function[[2]]], ", "] <> ");\n" <> 
		(* put into res_ *)
		If[function[[3]] =!= "Void", "\t\t" <> setType[function[[3]]] <> "(res_, ret);\n", ""] <>
		(* disown strings *)
		StringJoin@@MapIndexed[(generateDisownString[#1, First[#2]])&, function[[2]]] <>
		(* remember to delete char* *)
		If[function[[3]] === "UTF8String", "\t\tLL_lastChar = ret;\n", ""],
		
		(*else: return type is a ref to other c++ obj SPECIAL CASE*)		
		(* get the ID of return object*)
		"\t\tmint idRet = MArgument_getInteger(args_[0]);\n" <>
		(* get the ID *)
		"\t\tmint id = MArgument_getInteger(args_[1]);\n" <>
		(* get the class instance *)
		"\t\t" <> classname <> "& obj = *" <> cppMapName[classname] <> ".at(id);\n" <>		
		(* get the arguments *)
		StringJoin@@MapIndexed[(generateGetArg[#1, First[#2] + 1])&,function[[2]]] <>
		(* transform MTensors to std::vector *)
		MapIndexed[generateTensorToVector[#1, First[#2] + 1]&, function[[2]]] <>
		(* call the function *)
		"\t\t" <> cppMapName[ToString[function[[3]]]] <> ".at(idRet) = new " <> ToString[function[[3]]] <> "(obj." <> function[[1]] <> "(" <>
		(* put arguments *)
		StringRiffle[MapIndexed[generateArg[#1, "arg" <> ToString[First[#2] + 1]]&, function[[2]]], ", "] <> "));\n"
	] <> 
	(* finish the function *)
	"\n\t\treturn LIBRARY_NO_ERROR;\n\t}\n\n"
	
generateMemberFunctionCpp[classname_String, function_cppStaticFunction, number_Integer] := 
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[classname, function[[1]], number] <> "(" <> $stdArgs <> ") {\n" <>
	
	If[!isRefQ[function[[3]]],
		(*then : return type is not a reference to other c++ obj*)
		(* get the arguments *)
		StringJoin@@MapIndexed[(generateGetArg[#1, First[#2]-1])&,function[[2]]] <>
		(* transform MTensors to std::vector *)
		MapIndexed[generateTensorToVector[#1, First[#2]-1]&, function[[2]]] <>
		(* call the function *)
		"\t\t" <> If[function[[3]] =!= "Void", toCppMType[function[[3]]] <> " ret = ", ""] <> classname <> "::" <> function[[1]] <> "(" <>
		(* put arguments *)
		StringRiffle[MapIndexed[generateArg[#1, "arg" <> ToString[First[#2]-1]]&, function[[2]]], ", "] <> ");\n" <> 
		(* put into res_ *)
		If[function[[3]] =!= "Void", "\t\t" <> setType[function[[3]]] <> "(res_, ret);\n", ""] <>
		(* disown strings *)
		StringJoin@@MapIndexed[(generateDisownString[#1, First[#2]-1])&, function[[2]]] <>
		(* remember to delete char* *)
		If[function[[3]] === "UTF8String", "\t\tLL_lastChar = ret;\n", ""],
		
		(*else: return type is a ref to other c++ obj SPECIAL CASE*)		
		(* get the ID of return object*)
		"\t\tmint idRet = MArgument_getInteger(args_[0]);\n" <>	
		(* get the arguments *)
		StringJoin@@MapIndexed[(generateGetArg[#1, First[#2]])&,function[[2]]] <>
		(* transform MTensors to std::vector *)
		MapIndexed[generateTensorToVector[#1, First[#2]]&, function[[2]]] <>
		(* call the function *)
		"\t\t" <> cppMapName[ToString[function[[3]]]] <> ".at(idRet) = new " <> ToString[function[[3]]] <> "(" <> classname <> "::" <> function[[1]] <> "(" <>
		(* put arguments *)
		StringRiffle[MapIndexed[generateArg[#1, "arg" <> ToString[First[#2]]]&, function[[2]]], ", "] <> "));\n"
	] <> 
	(* finish the function *)
	"\n\t\treturn LIBRARY_NO_ERROR;\n\t}\n\n"
	
generateMemberFunctionCpp[classname_String, function_cppStaticMember, number_Integer] := 
	(* function header *)
	"\tEXTERN_C DLLEXPORT int " <> cppLLfunctionName[classname, function[[1]], number] <> "(" <> $stdArgs <> ") {\n" <>
	(*return type is a ref to other c++ obj SPECIAL CASE*)		
	(* get the ID of return object*)
	"\t\tmint idRet = MArgument_getInteger(args_[0]);\n" <>	
	(* get the member *)
	"\t\t" <> cppMapName[ToString[function[[2]]]] <> ".at(idRet) = new " <> ToString[function[[2]]] <> "(" <> classname <> "::" <> function[[1]] <> ");\n" <> 
	(* finish the function *)
	"\n\t\treturn LIBRARY_NO_ERROR;\n\t}\n\n"


generateClassCpp::usage = "Generates c++ code for a class"
generateClassCpp::author = "Gregor Kaelin"
generateClassCpp[class_cppClass] :=	
	(* comment *)
	"\t//********** " <> class[[1]] <> " **********\n" <>
	(* generate the instance managers *)
	generateManageInstanceCpp[class[[1]]] <>
	(* generate the constructors *)
	StringJoin@@MapIndexed[generateConstructorCpp[class[[1]], #1, First[#2]]&, class[[2]]] <>
	(* generate the functions *)
	StringJoin@@MapIndexed[generateMemberFunctionCpp[class[[1]], #1, First[#2]]&, class[[3]]]


generateHpp[classes_List, functions_List, includes_List : {}] := 	
	(* header *) 
	generateHeaderHpp[includes] <>
	(* classes *)
	StringJoin@@Map[generateClassHpp, classes] <>
	(* functions *)
	StringJoin@@MapIndexed[generateFunctionHpp[#1, First[#2]]&, functions] <>
	(* register library expressions *)
	"\tEXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_);\n\n" <> 
	(* unregister library expressions *)
	"\tEXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_);\n" <>
	(* finish *)
	"}\n#endif /*LIBRARYLINK_HPP*/"
	

generateCpp[classes_List, functions_List, includes_List : {}] := 	
	(* header *) 
	generateHeaderCpp[includes] <>
	(* classes *)
	StringJoin@@Map[generateClassCpp, classes] <>
	(* functions *)
	StringJoin@@MapIndexed[generateFunctionCpp[#1, First[#2]]&, functions] <>
	(* register library expressions *)
	"\tEXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_) {\n" <>
	StringJoin@@Map[generateRegisterLibraryExpr[#[[1]]]&, classes] <> 
	"\t\treturn LIBRARY_NO_ERROR;\n\t}\n\n" <> 
	(* unregister library expressions *)
	"\tEXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_) {\n" <>
	StringJoin@@Map[generateUnregisterLibraryExpr[#[[1]]]&, classes] <> "\t}\n" <>
	(* finish *)
	"}\n"


(* ::Subsection:: *)
(*Generate LibraryFunction code*)


LLfunctionName::usage = "Generates the libraryLink function name"
LLfunctionName::author = "Gregor Kaelin" 
LLfunctionName[classname_String, functionname_String, number_Integer] := "LL" <> classname <> Capitalize[StringReplace[functionname, {"_"->"", "["->"", "]"->""}]] <> ToString[number]
LLfunctionName[functionname_String, number_Integer] := "LL" <> Capitalize[StringReplace[functionname, {"_"->"", "["->"", "]"->""}]] <> ToString[number]


loadCppFunction::usage = "Loads a c++ function"
loadCppFunction::author = "Gregor Kaelin"
loadCppFunction[function_cppFunction, libName_String, number_Integer] := 
	Set[
		Evaluate[Symbol[LLfunctionName[function[[1]], number]]], 
		LibraryFunctionLoad[libName, cppLLfunctionName[function[[1]], number], If[!isRefQ[function[[3]]], toMathType/@function[[2]], Join[{Integer}, toMathType/@function[[2]]]], If[!isRefQ[function[[3]]], toMathType@function[[3]], "Void"]]
	];
	

loadCppConstructor::usage = "Loads a c++ constructor"
loadCppConstructor::author = "Gregor Kaelin"
loadCppConstructor[classname_String, constructor_cppConstructor, libName_String, number_Integer] :=
	Set[
		Evaluate[Symbol[LLfunctionName[classname, "Construct", number]]],
		LibraryFunctionLoad[libName, cppLLfunctionName[classname, "Construct", number], Join[{Integer},toMathType/@constructor[[1]]], "Void"]
	];
	
	
loadCppMemberFunction::usage = "Loads a c++ class member function"
loadCppMemberFunction::author = "Gregor Kaelin"
loadCppMemberFunction[classname_String, function_cppFunction, libName_String, number_Integer] :=
	Set[
		Evaluate[Symbol[LLfunctionName[classname, function[[1]], number]]],
		LibraryFunctionLoad[libName, cppLLfunctionName[classname,function[[1]], number],If[!isRefQ[function[[3]]], Join[{Integer}, toMathType/@function[[2]]],  Join[{Integer, Integer}, toMathType/@function[[2]]]], If[!isRefQ[function[[3]]], toMathType@function[[3]], "Void"]]
	];
loadCppMemberFunction[classname_String, function_cppStaticFunction, libName_String, number_Integer] :=
	Set[
		Evaluate[Symbol[LLfunctionName[classname, function[[1]], number]]],
		LibraryFunctionLoad[libName, cppLLfunctionName[classname,function[[1]], number],If[!isRefQ[function[[3]]], toMathType/@function[[2]],  Join[{Integer}, toMathType/@function[[2]]]], If[!isRefQ[function[[3]]], toMathType@function[[3]], "Void"]]
	];
loadCppMemberFunction[classname_String, function_cppStaticMember, libName_String, number_Integer] :=
	Set[
		Evaluate[Symbol[LLfunctionName[classname, function[[1]], number]]],
		LibraryFunctionLoad[libName, cppLLfunctionName[classname,function[[1]], number],{{Integer}}, "Void"]
	];
	
	
loadCppClass::usage = "Loads a c++ class"
loadCppClass::author = "Gregor Kaelin"
loadCppClass[class_cppClass, libName_String] :=
	(scanIndexed[loadCppConstructor[class[[1]], #1, libName, First[#2]]&, class[[2]]];
	scanIndexed[loadCppMemberFunction[class[[1]], #1, libName, First[#2]]&, class[[3]]];)


loadCppEnum::usage = "Loads a c++ enum class"
loadCppEnum::author = "Gregor Kaelin"
loadCppEnum[enum_cppEnum] := AppendTo[$enums, enum[[1]]->enum[[2]]];


loadCpp[enums_List, classes_List, functions_List, libName_String] := 
	(Scan[loadCppEnum, enums];
	Scan[loadCppClass[#, libName]&, classes];
	scanIndexed[loadCppFunction[#1, libName, First[#2]]&, functions];) 
	
	


(* ::Subsection:: *)
(*Generate actual user-callable functions*)


prepareArgs::usage = "Transforms a list of argument types to a list of pairs: {argument, argumentpattern }"
prepareArgs::author = "Gregor Kaelin"
prepareArgs[args_List] := Map[prepareArg[#]&, args]
	

prepareArg::usage = "Transforms a c++ argument to a argument pattern"
prepareArg::author = "Gregor Kaelin"
prepareArg[opt_optional] := Block[{temp},
	temp = prepareArg[opt[[1]]];
	{temp[[1]], Optional[temp[[2]], opt[[2]]]}
]
prepareArg[{Integer}] := With[{arg = Unique[Symbol["arg"]]}, {arg, arg_List}]
prepareArg[{enum_String}] := With[{arg = Unique[Symbol["arg"]]}, {Hold[First[FirstPosition[enum/.$enums, arg]]] - 1, arg_List}]
prepareArg[{type_}] := With[{arg = Unique[Symbol["arg"]]}, {Hold[ManagedLibraryExpressionID /@ arg], arg_List}]
prepareArg[Integer] := With[{arg = Unique[Symbol["arg"]]}, {arg, Pattern[arg, _Integer]}]
prepareArg[Real] := With[{arg = Unique[Symbol["arg"]]}, {arg, Pattern[arg, _Real]}]
prepareArg["Boolean"] := With[{arg = Unique[Symbol["arg"]]}, {arg, Pattern[arg, True | False]}]
prepareArg["UTF8String"] := With[{arg = Unique[Symbol["arg"]]}, {arg, Pattern[arg, _String]}]
prepareArg[enum_String] := With[{arg = Unique[Symbol["arg"]]}, {Hold[First[FirstPosition[enum/.$enums, arg]]] - 1, Pattern[arg, _String]}]
prepareArg[type_] := With[{arg = Unique[Symbol["arg"]]}, {Hold[ManagedLibraryExpressionID[arg]], Pattern[arg, _type]}]


mathematicaFunctionName::usage = "Extracts the function name that should be used by mathematica"
mathematicaFunctionName::author = "Gregor Kaelin"
mathematicaFunctionName[function_cppFunction] := 
	If[Length[function] == 3,
		(*then*) 
		function[[1]],
		(*else*)
		If[MemberQ[function[[4]], "MMAFunName"->_],
			(*then*)
			"MMAFunName"/.function[[4]],
			(*else*)
			function[[1]]
		]
	]
mathematicaFunctionName[classname_String, function_cppFunction] := 
	If[Length[function] == 3,
		(*then*) 
		classname <> Capitalize[function[[1]]],
		(*else*)
		If[MemberQ[function[[4]], "MMAFunName"->_],
			(*then*)
			"MMAFunName"/.function[[4]],
			(*else*)
			classname <> Capitalize[function[[1]]]
		]
	]
	
mathematicaFunctionName[classname_String, function_cppStaticFunction] := 
	If[Length[function] == 3,
		(*then*) 
		classname <> Capitalize[function[[1]]],
		(*else*)
		If[MemberQ[function[[4]], "MMAFunName"->_],
			(*then*)
			"MMAFunName"/.function[[4]],
			(*else*)
			classname <> Capitalize[function[[1]]]
		]
	]
mathematicaFunctionName[classname_String, function_cppStaticMember] := 
	If[Length[function] == 2,
		(*then*) 
		classname <> Capitalize[function[[1]]],
		(*else*)
		If[MemberQ[function[[3]], "MMAFunName"->_],
			(*then*)
			"MMAFunName"/.function[[3]],
			(*else*)
			classname <> Capitalize[function[[1]]]
		]
	]
mathematicaFunctionName[classname_String, constructor_cppConstructor] := 
	If[Length[constructor] == 1,
		(*then*) 
		"construct" <> Capitalize[classname],
		(*else*)
		If[MemberQ[constructor[[2]], "MMAFunName"->_],
			(*then*)
			"MMAFunName"/.constructor[[2]],
			(*else*)
			"construct" <> Capitalize[classname]
		]
	]
	
defineCppFunction::usage = "Defines a free c++ function"
defineCppFunction::author = "Gregor Kaelin"
defineCppFunction[function_cppFunction, number_Integer] :=
	With[{functionName = mathematicaFunctionName[function], argPairs = prepareArgs[function[[2]]]},
		With[{fun = Symbol[LLfunctionName[function[[1]], number]], patterns = (#[[2]]&/@argPairs), args = (First/@argPairs)},
			SetDelayed[
				Evaluate[Symbol[functionName]][Sequence@@patterns],
				Module[{ret},
					If[!isRefQ[function[[3]]], 
						(*then*)
						ret = fun[Sequence@@ReleaseHold/@args],
						(*else*)
						ret = CreateManagedLibraryExpression[ToString[function[[3]]], function[[3]]];
						fun[ManagedLibraryExpressionID[ret], Sequence@@ReleaseHold/@args]
					];
					If[function[[3]] === "UTF8String",
						(*then*)
						LLdeleteLastChar[];
					];
					If[Length[function] > 3,
						(*then*)
						If[MemberQ[function[[4]], "postFunction"->_],
							(*then*)
							ret = ("postFunction"/.function[[4]])@ret;
						];
					];
					If[function[[3]] =!= "Void", ret, 0;]
				]
			]
		]
	]
		
	
defineCppConstructor::usage = "Defines a c++ class constructor"
defineCppConstructor::author = "Gregor Kaelin"
defineCppConstructor[classname_String, constructor_cppConstructor, number_Integer] :=
	With[{functionName = mathematicaFunctionName[classname, constructor], argPairs = prepareArgs[constructor[[1]]]},
		With[{fun = Symbol[LLfunctionName[classname, "Construct", number]], patterns = (#[[2]]&/@argPairs), args = (First/@argPairs)},
			SetDelayed[
				Evaluate[Symbol[functionName]][Sequence@@patterns],
				Module[{ret},
					ret = CreateManagedLibraryExpression[classname, Symbol[classname]];
					fun[ManagedLibraryExpressionID[ret],Sequence@@ReleaseHold/@args];
					ret
				]
			]
		]
	]


defineCppMemberFunction::usage = "Defines a c++ class member function"
defineCppMemberFunction::author = "Gregor Kaelin"
defineCppMemberFunction[classname_String, function_cppFunction, number_Integer] :=
	With[{functionName = mathematicaFunctionName[classname, function], argPairs = prepareArgs[function[[2]]], arg0 = Unique[Symbol["arg"]], classsymb = Symbol[classname]},
		With[{fun = Symbol[LLfunctionName[classname, function[[1]], number]], patterns = Join[{PatternTest[Pattern[arg0, _classsymb],ManagedLibraryExpressionQ]},(#[[2]]&/@argPairs)], args = Join[{Hold[ManagedLibraryExpressionID[arg0]]},(First/@argPairs)]},
			SetDelayed[
				Evaluate[Symbol[functionName]][Sequence@@patterns],
				Module[{ret},
					If[!isRefQ[function[[3]]],
						(*then*) 
						ret = fun[Sequence@@ReleaseHold/@args],
						(*else*)
						ret = CreateManagedLibraryExpression[ToString[function[[3]]], function[[3]]];
						fun[ManagedLibraryExpressionID[ret], Sequence@@ReleaseHold/@args]
					];
					If[function[[3]] === "UTF8String",
						(*then*)
						LLdeleteLastChar[];
					];
					If[Length[function] > 3,
						(*then*)
						If[MemberQ[function[[4]], "postFunction"->_],
							(*then*)
							ret = ("postFunction"/.function[[4]])@ret;
						];
					];
					If[function[[3]] =!= "Void", ret, 0;]
				]
			]
		]
	]
	
defineCppMemberFunction[classname_String, function_cppStaticFunction, number_Integer] :=
	With[{functionName = mathematicaFunctionName[classname, function], argPairs = prepareArgs[function[[2]]]},
		With[{fun = Symbol[LLfunctionName[classname, function[[1]], number]], patterns = (#[[2]]&/@argPairs), args = (First/@argPairs)},
			SetDelayed[
				Evaluate[Symbol[functionName]][Sequence@@patterns],
				Module[{ret},
					If[!isRefQ[function[[3]]],
						(*then*) 
						ret = fun[Sequence@@ReleaseHold/@args],
						(*else*)
						ret = CreateManagedLibraryExpression[ToString[function[[3]]], function[[3]]];
						fun[ManagedLibraryExpressionID[ret], Sequence@@ReleaseHold/@args]
					];
					If[function[[3]] === "UTF8String",
						(*then*)
						LLdeleteLastChar[];
					];
					If[Length[function] > 3,
						(*then*)
						If[MemberQ[function[[4]], "postFunction"->_],
							(*then*)
							ret = ("postFunction"/.function[[4]])@ret;
						];
					];
					If[function[[3]] =!= "Void", ret, 0;]
				]
			]
		]
	]
defineCppMemberFunction[classname_String, function_cppStaticMember, number_Integer] :=
	With[{functionName = mathematicaFunctionName[classname, function]},
		With[{fun = Symbol[LLfunctionName[classname, function[[1]], number]]},
			Set[
				Evaluate[Symbol[functionName]],
				Module[{ret},
					ret = CreateManagedLibraryExpression[ToString[function[[2]]], function[[2]]];
					fun[ManagedLibraryExpressionID[ret]];
					If[Length[function] > 2,
						(*then*)
						If[MemberQ[function[[3]], "postFunction"->_],
							(*then*)
							ret = ("postFunction"/.function[[3]])@ret;
						];
					];
					ret
				]
			]
		]
	]


defineCppClass::usage = "Defines a c++ class"
defineCppClass::author = "Gregor Kaelin"
defineCppClass[class_cppClass] :=
	(scanIndexed[defineCppConstructor[class[[1]], #1, First[#2]]&, class[[2]]];
	scanIndexed[defineCppMemberFunction[class[[1]], #, First[#2]]&, class[[3]]];)
	

defineCpp[classes_List, functions_List] := 
	(Scan[defineCppClass, classes];
	scanIndexed[defineCppFunction[#1, First[#2]]&, functions];)
	

buildCppMathInterface[enums_List, classes_List, functions_List, libName_String] :=
	(
	(* load delete last char function *)
	With[{symb = Symbol[Context[] <> "LLDeleteLastChar"]},
		symb::usage = "(LibraryLink) clean up char* memory";
		symb::author = "Gregor Kaelin";symb::usage = "(LibraryLink) clean up char* memory";
		symb::author = "Gregor Kaelin";
		symb = LibraryFunctionLoad[libName, "LL_deleteLastChar", {}, "Void"];
	];
	(* load *)
	loadCpp[enums, classes, functions, libName];
	(* define user functions*)
	defineCpp[classes, functions];
	)
	

End[] (* End Private Context *)

Protect@@Names["gGraph`cppLibraryLink`*"];
EndPackage[]
