(* ::Package:: *)

Quit[]


<<"gGraph`";


(* ::Section::Closed:: *)
(*PM EFT impulse*)


gs=makeEFTPMGraphs[2];
drawGraphsEFT@gs


(* ::Section::Closed:: *)
(*GRWL impulse*)


gs=makeGRWLImpulseGraphs[2];
drawGraphsEFT@gs


(* ::Section::Closed:: *)
(*GRWL eikonal action*)


gs=makeGRWLSGraphs[2];
drawGraphsEFT@gs


(* ::Section:: *)
(*Generic constructor*)


(* ::Subsection::Closed:: *)
(*Mimic GRWL impulse *)


(* ::Text:: *)
(*We first define all the line types, then all the couplings. I added a mechanism to add numbers to each coupling such that the BHs can be numbered and the symmetry factors should come out right.*)
(*Some remarks: *)
(*- this set also includes graviton lines that go between the same (!) WL which you probably don't want for now*)
(*- this set also includes graph where only one of the WLs appears, you might want to remove those as well (for now?)*)
(*- there are no momenta assigned to edges, but you can safely use the ID (gray numbers) of the line as a momentum identifier*)


(*define all line types*)
wl1Line=constructFlExt["SCALAR", 0];(*undirected with flavor 0*)
wl2Line=constructFlExt["SCALAR", 1];(*undirected with flavor 1*)
grLine=constructFlExt["GLUON", 0];(*undirected with flavor -1 = no flavor*)


(*couplings*)
vertWL1=constructCoupling[{grLine},1];(*one outgoing graviton line, numbered 1*)
vertWL2=constructCoupling[{grLine},2];(*one outgoing graviton line, numbered 2*)
vertWL11=constructCoupling[{grLine,wl1Line},1];(*one gr line and one wl1 line, numbered 1*)
vertWL21=constructCoupling[{grLine,wl2Line},2];(*one gr line and one wl2 line, numbered 2*)
vertWL12=constructCoupling[{grLine,wl1Line,wl1Line},1];(*...*)
vertWL22=constructCoupling[{grLine,wl2Line,wl2Line},2];
vertGR3=constructCoupling[{grLine,grLine,grLine}];*(*three-graviton coupling, no number*)
vertGR4=constructCoupling[{grLine,grLine,grLine,grLine}];(*four-graviton coupling, no number*)
couplings={vertWL1,vertWL2,vertWL11,vertWL21,vertWL12,vertWL22,vertGR3,vertGR4};


gs=constructGraphs[
	{}(*no externals*),
	0(*0 loops = tree level*),
	couplings,
	4(*number of vertices*)];
drawGraphsEFT@gs


(* ::Subsection::Closed:: *)
(*Radiation graphs (just a workaround for now)*)


(* ::Text:: *)
(*We now number the two BHs "2" and "3", the external graviton will get the label "1". Note that even numbers are drawn up and odd numbers down if you use "drawGraphEFT", so the external guy will be drawn down together with the first BH.*)


(*define all line types*)
wl1Line=constructFlExt["SCALAR", 0];(*undirected with flavor 0*)
wl2Line=constructFlExt["SCALAR", 1];(*undirected with flavor 1*)
grLine=constructFlExt["GLUON", 0];(*undirected with flavor -1 = no flavor*)


(*couplings*)
vertWL1=constructCoupling[{grLine},2];(*one outgoing graviton line, numbered 2*)
vertWL2=constructCoupling[{grLine},3];(*one outgoing graviton line, numbered 3*)
vertWL11=constructCoupling[{grLine,wl1Line},2];(*one gr line and one wl1 line, numbered 2*)
vertWL21=constructCoupling[{grLine,wl2Line},3];(*one gr line and one wl2 line, numbered 3*)
vertWL12=constructCoupling[{grLine,wl1Line,wl1Line},2];(*...*)
vertWL22=constructCoupling[{grLine,wl2Line,wl2Line},3];
vertGR3=constructCoupling[{grLine,grLine,grLine}];(*three-graviton coupling, no number*)
vertGR4=constructCoupling[{grLine,grLine,grLine,grLine}];(*four-graviton coupling, no number*)
couplings={vertWL1,vertWL2,vertWL11,vertWL21,vertWL12,vertWL22,vertGR3,vertGR4};


gs=constructGraphs[
	{grLine}(*the external gravitons*),
	0(*0 loops = tree level*),
	couplings,
	3(*number of vertices*)];
drawGraphsEFT@gs


(* ::Subsection:: *)
(*Spin*)


(* ::Text:: *)
(*Homework for you! I believe everything should be there that you need.*)
(*Hints: *)
(*- use labels 2,3,4,5,... to distinguish between different types of WL/Spin/Tidal couplings*)
(*- use flavors to distinguish spin vs WL edges*)
